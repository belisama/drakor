// ==UserScript==
// @name         Pet Quote Coloring
// @namespace    http://tampermonkey.net/
// @version      2020-08-25-1757
// @description  Colors new pet quotes
// @author       Persica
// @match        https://*.drakor.com*
// ==/UserScript==

persicasPetQuoteColors();

function persicasPetQuoteColors() {
    "use strict";
    var milliSeconds = 500;

    // run constantly
    setInterval(petQuote, milliSeconds);

    function petQuote() {
        var chat = document.getElementById("dr_chatBody").children;
        var chatPet;
        var chatQuote;
        var finalQuote;

        var i;

        var ownerName;
        var ownerFillIn = "[owner]";

        var newQuoteColor = "Red";
        var oldQuoteColor = "DarkSlateGrey";

        var quoteList = [
            // cotton cloudy
            "Cotton Cloudy] Leaked.",
            "Cotton Cloudy] says \"Can't believe that kid called me a whale! SO RUDE\"",
            "Cotton Cloudy] says \"Did you know that i'm the raining champion?\"",
            "Cotton Cloudy] says \"For crying out cloud!\"",
            "Cotton Cloudy] says \"Have you seen The Mist? Such a good movie! Soooooo Realistic!\"",
            "Cotton Cloudy] says \"I hate it when i have mist calls!\"",
            "Cotton Cloudy] says \"I'm sorry! I couldn't hold it in any longer!\"",
            "Cotton Cloudy] says \"Love me for who I am, not what I Look like!\"",
            "Cotton Cloudy] says \"My minds a bit foggy right.\"",
            // angry eel
            "Angry Eel] says \"The flame of anger, bright and brief, sharpens the barb of love.\"",
            "Angry Eel] says \"The world needs anger. The world often contines to allow evil because it isn't angry enough.\"",
            "Angry Eel] says \"There's passion in anger, it comes from a caring heart.\"",
            "Angry Eel] says \"Usually when people are sad, they don't do anything. They just cry over their condition. But when they get angry, they bring about a change.\"",
            // baby blue dragon
            "Baby Blue Dragon] says \"Every flower must grow through dirt.\"",
            "Baby Blue Dragon] says \"Go for it! After all, bad decisions make great stories!\"",
            "Baby Blue Dragon] says \"Intellectual growth should commence at birth and cease only at death. \"",
            "Baby Blue Dragon] says \"Today is the oldest you've ever been and the youngest you'll ever be again.\"",
            // baby green dragon
            "Baby Green Dragon] says \"Every flower must grow through dirt.\"",
            "Baby Green Dragon] says \"Go for it! After all, bad decisions make great stories!\"",
            "Baby Green Dragon] says \"Intellectual growth should commence at birth and cease only at death. \"",
            "Baby Green Dragon] says \"Today is the oldest you've ever been and the youngest you'll ever be again.\"",
            // baby red dragon
            "Baby Red Dragon] says \"Every flower must grow through dirt.\"",
            "Baby Red Dragon] says \"Go for it! After all, bad decisions make great stories!\"",
            "Baby Red Dragon] says \"Intellectual growth should commence at birth and cease only at death. \"",
            "Baby Red Dragon] says \"Today is the oldest you've ever been and the youngest you'll ever be again.\"",
            // baby chick
            "Baby Chick] says \"A hen is only an eggs way of making another egg.\"",
            "Baby Chick] says \"Chickens are the new dog!\"",
            "Baby Chick] says \"Fine feathers don't mean you can fly.\"",
            "Baby Chick] says \"Guess What?....Chicken Butt!!\"",
            "Baby Chick] says \"I dream of a better world where chickens can cross the road without their motives questioned.\"",
            "Baby Chick] says \"I wonder if chickens do the `people dance` at their weddings?\"",
            "Baby Chick] says \"It is better to be the head of chicken than the rear end of an ox.\"",
            "Baby Chick] says \"Quit your squawking!\"",
            "Baby Chick] says \"The sky is falling!\"",
            "Baby Chick] says \"Which came first the chicken or the egg?\"",
            "Baby Chick] says \"Wicked chickens lay devilled eggs.\"",
            // baby fire drake
            "Baby Fire Drake] enters the room and surveys the situation suspiciously...",
            "Baby Fire Drake] says \"A Dwarf walks into a bar with a parrot on his shoulder, Bartender says, \"Hey, where'd ya get that thing?\". Parrot says, \"The Mountains. There's hundreds of 'em running around over there.\"\"",
            "Baby Fire Drake] says \"A tiger doesn't lose sleep over the opinion of a sheep.\"",
            "Baby Fire Drake] says \"Always remember that you are absolutely unique. Just like everyone else.\"",
            "Baby Fire Drake] says \"Can I cook those food buffs for you?\"",
            "Baby Fire Drake] says \"Do vegetarians eat animal crackers?\"",
            "Baby Fire Drake] says \"Funny how you can walk into a spider web and instantaneously become a karate master.\"",
            "Baby Fire Drake] says \"If it looks like I am going to sneeze, run!\"",
            "Baby Fire Drake] says \"The biggest lie you ever told yourself was 'I don’t need to write that down, I’ll remember it'\"",
            "Baby Fire Drake] says \"The only advice I can give you here is don’t worry, no one else knows what the heck they are doing either.\"",
            "Baby Fire Drake] says \"WHAT?! I'm not weird and crazy. My Mom told me so!\"",
            "Baby Fire Drake] says \"When life gives you lemons, make apple juice, sit back, and let the world wonder how you did it.\"",
            // baby ice drake
            "Baby Ice Drake] says \"Always be yourself. Unless you can be a dragon. Then always be a dragon.\"",
            "Baby Ice Drake] says \"Challenge is a dragon with a gift in its mouth. Tame the dragon, and that gift is yours.\"",
            "Baby Ice Drake] says \"Do you want to build a snowman?\"",
            "Baby Ice Drake] says \"He who cannot put his thoughts on ice should not enter into the heat of dispute.\"",
            "Baby Ice Drake] says \"Ice ice, baby.\"",
            "Baby Ice Drake] says \"Life is like ice cream, enjoy it before it melts.\"",
            "Baby Ice Drake] says \"Never meddle in the affairs of dragons for you are crunchy and good with ketchup.\"",
            "Baby Ice Drake] says \"To do list: Wake up, eat ice cream, take nap, repeat.\"",
            "Baby Ice Drake] says \"Why does everyone look at me funny when I offer them a lemon snow cone?\"",
            // baby rockling
            "Baby Rockling] says \"Every rock they throw, I use as a stepping stone.\"",
            "Baby Rockling] says \"Here I am. Rock you like a hurricane.\"",
            "Baby Rockling] says \"I'm confused. It takes incredible skill to smith something from my ore, yet the items you make are terribly mediocre.\"",
            "Baby Rockling] says \"Rock bottom became the solid foundation on which I rebuilt my life.\"",
            "Baby Rockling] says \"Rock, Smash!\"",
            "Baby Rockling] says \"We will, we will, rock you! Rock you!\"",
            "Baby Rockling] says \"Where I come from in the north, we used to have exquisite gourmet rocks.\"",
            "Baby Rockling] says \"You can move mountains.\"",
            // barn owl
            "Barn Owl] says \"A fool thinks himself to be wise, but a wise man knows himself to be a fool.\"",
            "Barn Owl] says \"Behind every great man is a woman rolling her eyes.\"",
            "Barn Owl] says \"If opportunity doesn't knock, build a door.\"",
            "Barn Owl] says \"People who think they know everything are a great annoyance to those of us who do.\"",
            "Barn Owl] says \"We are made wise not by the recollection of our past, but by the responsibility for our future.\"",
            // barometz
            "Barometz] \"Lambchop\" has been baaad.",
            "Barometz] bleats.",
            "Barometz] blinks.",
            "Barometz] dozes off.",
            "Barometz] has been baaad.",
            "Barometz] hears howling in the distance.",
            "Barometz] looks uncomfortable.",
            "Barometz] sighs.",
            "Barometz] sways in the breeze.",
            // blue winged stork
            "Blue Winged Stork] says \"Adults are just outdated children.\"",
            "Blue Winged Stork] says \"Always be nice to your children because they are the ones who will choose your rest home.\"",
            "Blue Winged Stork] says \"And so the adventure begins...\"",
            "Blue Winged Stork] says \"Children are unpredictable. You never know what inconsistency they’re going to catch you in next.\"",
            "Blue Winged Stork] says \"Children seldom misquote. In fact, they usually repeat word for word what you shouldn't have said.\"",
            "Blue Winged Stork] says \"Dream big little one.\"",
            "Blue Winged Stork] says \"Even the smallest one can change the world.\"",
            "Blue Winged Stork] says \"Having kids is a lot like living in a frat house. Everything’s sticky and you’re not quite sure why.\"",
            "Blue Winged Stork] says \"It is a well-documented fact that guys will not ask for directions. This is a biological thing. This is why it takes several million sperm cells... to locate a female egg, despite the fact that the egg is, relative to them, the size of Wisconsin.\"",
            "Blue Winged Stork] says \"Little boys are just superheroes in disguise.\"",
            "Blue Winged Stork] says \"Silence is Golden… unless you have kids, then silence is just suspicious.\"",
            "Blue Winged Stork] says \"The littlest feet make the biggest footprints in our hearts.\"",
            "Blue Winged Stork] says \"There can be no keener revelation of a society's soul than the way in which it treats its children.\"",
            // candy cane drake
            "Candy Cane Drake] says \"[owner] is permanently on the naughty list… and loving every minute of it.\"",
            "Candy Cane Drake] says \"Don’t get your tinsel in a tangle!\"",
            "Candy Cane Drake] says \"I wish you the gift of love, the gift of peace and the gift of happiness.\"",
            "Candy Cane Drake] says \"Joy to the world!\"",
            "Candy Cane Drake] says \"Keep calm and merry on\"",
            "Candy Cane Drake] says \"No matter how old you are, an empty Christmas wrapping paper tube is still a fun thing to bonk someone over the head with!\"",
            "Candy Cane Drake] says \"Peace on Earth and Joy to all!\"",
            "Candy Cane Drake] says \"The main reason Santa is so jolly is because he knows where all the bad girls live.\"",
            "Candy Cane Drake] says \"What do you get when you cross a snowman with a vampire?? Frostbite!!! \"",
            "Candy Cane Drake] says \"You can tell a lot about a person by the way they handle three things: a rainy day, lost luggage and tangled Christmas tree lights.\"",
            "Candy Cane Drake] says \"You have been naughty, and here's the scoop, all you get is the snowman's poop!\"",
            // capuchin
            "Capuchin] says \"I meant to behave but there was too many options.\"",
            "Capuchin] says \"Just 'cause you have the monkey off your back, it doesn't mean the circus has left town ...\"",
            "Capuchin] says \"Laughter is the best medicine... but if you laugh for no reason, you might need medicine.\"",
            "Capuchin] says \"Relax we are all crazy... There is no competition.\"",
            "Capuchin] says \"Why is it called beauty sleep when you wake up looking like a troll doll?\"",
            // chocolate alpaca
            "Chocolate Alpaca] says \"An idea isn't responsible for the people who believe in it.\"",
            "Chocolate Alpaca] says \"Get your facts first, then you can distort them as you please. \"",
            "Chocolate Alpaca] says \"I'm sooooo fluffyyyy!\"",
            "Chocolate Alpaca] says \"Pretty is as pretty does and I does pretty very well.\"",
            "Chocolate Alpaca] says \"Who ate my burger? *glares at [owner]*\"",
            // dire wolf
            "Dire Wolf] says \"Experience is the sinking feeling you have made this mistake before.\"",
            "Dire Wolf] says \"Oh my!!! I appear to have dropped my elephant! \"",
            "Dire Wolf] says \"Sell a man a fish, and he can eat for a day, teach a man to fish, and you lose a great business opportunity.\"",
            "Dire Wolf] says \"The early bird gets the worm, but the second mouse gets the cheese.\"",
            // evil pumpkin
            "Evil Pumpkin] says \"Forget ghosts . . . beware of me!\"",
            "Evil Pumpkin] says \"I think for Halloween I shall go as karma. Some of you should be worried.\"",
            "Evil Pumpkin] says \"I've seen enough horror movies to know that the weirdo in the mask is never friendly.\"",
            "Evil Pumpkin] says \"If you are reading this then you are blissfully unaware of what is creeping up behind you.\"",
            "Evil Pumpkin] says \"The oldest and strongest emotion of mankind is fear, and the oldest and strongest kind of fear is fear of the unknown.\"",
            "Evil Pumpkin] says \"The only thing we have to fear is FEAR itself... oh... and spiders.\"",
            "Evil Pumpkin] says \"What an excellent day for an exorcism\"",
            // fiamma gatto
            "Fiamma Gatto] says \"Behind every great man is a woman rolling her eyes.\"",
            "Fiamma Gatto] says \"Between two evils, I always pick the one I never tried before.\"",
            "Fiamma Gatto] says \"Do not take life too seriously. You will never get out of it alive.\"",
            "Fiamma Gatto] says \"Everything is funny, as long as it's happening to somebody else.\"",
            "Fiamma Gatto] says \"I always wanted to be somebody, but now I realize I should have been more specific.\"",
            "Fiamma Gatto] says \"I like nonsense; it wakes up the brain cells.\"",
            "Fiamma Gatto] says \"If at first you don't succeed, find out if the loser gets anything.\"",
            "Fiamma Gatto] says \"Only the mediocre are always at their best.\"",
            "Fiamma Gatto] says \"Until you walk a mile in another man's moccasins you can't imagine the smell.\"",
            "Fiamma Gatto] says \"You're in pretty good shape for the shape you are in.\"",
			// fire imp
            "Fire Imp] says \"Fire is never a gentle master.\"",
            "Fire Imp] says \"Fire is the most tolerable third party.\"",
            "Fire Imp] says \"Playing with fire is bad for those who burn themselves. For the rest of us, it is a very great pleasure.\"",
			"Fire Imp] says \"What matters most is how well you walk through the fire.\"",
            // flying pig
            "Flying Pig] says \"I wonder, has anyone actually sat down and thought about how much stuff happened when I learned to fly?\"",
            "Flying Pig] says \"If at first you don't succeed, skydiving is not for you.\"",
            "Flying Pig] says \"If I agree with you, we’d both be wrong.\"",
            "Flying Pig] says \"Let’s be spontaneous… but we can do it tomorrow.\"",
            "Flying Pig] says \"Never wrestle with pigs. You both get dirty but only the pig likes it.\"",
            "Flying Pig] says \"What’s mine is yours and what’s yours is mine… Luckily, you have better stuff than me!\"",
            // flying squirrel
            "Flying Squirrel] says \"A good squirrel never looks down on another squirrel unless up in a tree.\"",
            "Flying Squirrel] says \"A squirrel doesn't know what he knows until he knows what he doesn't know.\"",
            "Flying Squirrel] says \"A wise squirrel once said - If the nut does not fall from the tree, climb up and get it.\"",
            "Flying Squirrel] says \"Before you judge a squirrel, walk a mile with his nuts. After that who cares. He`s a mile away and you've got his nuts.\"",
            "Flying Squirrel] says \"Cracking up tough nuts is supposed to be hard. If it were easy, there would be no nuts left to crack.\"",
            "Flying Squirrel] says \"Don't look at me like I'm just a rat with a cuter outfit!\"",
            "Flying Squirrel] says \"I don’t have a short attention span, I just... Oh look, an acorn!\"",
            "Flying Squirrel] says \"Squirrel who asks, may look like a fool for five minutes. Squirrel who does not ask, may end up as a fur coat.\"",
            "Flying Squirrel] says \"Wait, wait. I'm worried what you just heard was \"Give me a lot of acorns\", but what I said was: \"Give me all the acorns you have.\"\"",
            "Flying Squirrel] says \"What do you mean, you logged an entire forest and didn't find me any nuts?!?\"",
            // guinea pig
            "Guinea Pig] comfortably purrs and stretches.",
            "Guinea Pig] says \"Day 42: The humans still think I'm a baked potato.\"",
            "Guinea Pig] says \"Every rustle you make, every bag you shake, I'll be watching you.\"",
            "Guinea Pig] says \"Heeeheee, you thought that was a raisin!\"",
            "Guinea Pig] says \"I don't always chew on my cage. But when i do it's 3AM.\"",
            "Guinea Pig] says \"I don't have to pee, but i will if you pick me up.\"",
            "Guinea Pig] says \"I had a dream that i actually got somewhere on the wheel. Does this mean something? Is my life complete?\"",
            "Guinea Pig] says \"I SMELL FOOD!!\"",
            "Guinea Pig] says \"Put down the carrot and walk away. I said WALK!\"",
            "Guinea Pig] says \"Wheek wheek wheek.\"",
            "Guinea Pig] wishes you a nice wheek!",
            // gyrfalcon
            "Gyrfalcon] says \"Don't ruffle my feathers! I got claws that need sharpening!\"",
            "Gyrfalcon] says \"Eh, whats going on down there in the bush? Oh, never mind I'll be on my way.\"",
            "Gyrfalcon] says \"Hold fast to dreams, for if dreams die, life is a broken-winged bird that cannot fly.\"",
            "Gyrfalcon] says \"If your going to fly with me, you'll need to leave all that ore behind.\"",
            "Gyrfalcon] says \"The early bird always gets the worm\"",
            "Gyrfalcon] says \"Watching, watching, watching. Always watching, never resting...\"",
            // hammerhead shark
            "Hammerhead Shark] says \"Fish are friends, not food.\"",
            "Hammerhead Shark] says \"Have you seen my friend, Great White, anywhere? He still hasn't returned from playing with that be-speckled fisherman.\"",
            "Hammerhead Shark] says \"It's hammer time!\"",
            "Hammerhead Shark] says \"Swim along, nothing to see here.\"",
            // happy slime
            "Happy Slime] gives you a great big slimy hug.",
            "Happy Slime] says \"At least, we meet for the first time for the last time!\"",
            "Happy Slime] says \"Hello, sunshine!\"",
            "Happy Slime] says \"I'm Batman!\"",
            "Happy Slime] says \"Put that cookie down!\"",
            // holy cow
            "Holy Cow] says \"As a calf follows its mother among a thousand cows, so the deeds of a man follow him.\"",
            "Holy Cow] says \"I’ve never seen a wild almond or a soy galloping about in its natural habitat, but cow milk is the best.\"",
            "Holy Cow] says \"The cow is of the bovine ilk. One end is moo, the other, milk.\"",
            "Holy Cow] says \"You can only milk a cow so long, then you're left holding the pail.\"",
            // ice cream critter
            "Ice Cream Critter] says \"Birthdays are nature's way of telling us to eat more Ice cream.\"",
            "Ice Cream Critter] says \"It's always a good idea to make friends with babies. That's free Ice cream once a year, for a lifetime.\"",
            "Ice Cream Critter] says \"Thank you for celebrating Drakor's Birthday, [owner]. You are my best-est friend!\"",
            "Ice Cream Critter] says \"There are no better people with whom to spend your birthday, than your friends. Thank you for celebrating Drakor, on this day, [owner].\"",
            "Ice Cream Critter] says \"Why are birthday's good for you? Statistics show that the people who have the most live the longest!\"",
            "Ice Cream Critter] says \"You can have too much of a good thing: birthdays.\"",
            // kitten
            "Kitten] enters the room and starts licking her paws",
            "Kitten] gets crazyfaced and starts zooming all over the place",
            "Kitten] meows for treats",
            "Kitten] purrs and rubs against your legs",
            "Kitten] says \"4AM is always the best time to boop your nose to let you know i love you\"",
            "Kitten] says \"As every kitten owner knows, nobody owns a kitten.\"",
            "Kitten] says \"Can I get a belly rub? But only exactly three, anymore and you're in trouble\"",
            "Kitten] says \"I fear the humans don’t understand the severity of the fly intruder. I fight with all my strength, yet they just sit idly by, indifferent to the flying drone’s invasion.\"",
            "Kitten] says \"I'm thinking you pushing me off your keyboard means you want me to try harder.\"",
            "Kitten] says \"It amazes me that the humans still assume I trip them by accident.\"",
            "Kitten] says \"Oh look! Something on a counter. Must...knock...it...off\"",
            // love pug
            "Love Pug] says \"All you need is love. But a little chocolate now and then doesn't hurt.\"",
            "Love Pug] says \"Better to have loved and lost, than to have never loved at all.\"",
            "Love Pug] says \"Do you believe in love at first sight, or should I walk by again?\"",
            "Love Pug] says \"Love is like the wind, you can't see it, but you can feel it.\"",
            "Love Pug] says \"Love is the only force capable of transforming an enemy into a friend.\"",
            "Love Pug] says \"Spread love everywhere you go. Let no one ever come to you without leaving happier.\"",
            "Love Pug] says \"True love comes quietly, without banners or flashing lights. If you hear bells, get your ears checked.\"",
            "Love Pug] says \"True love stories never have endings.\"",
            // lashaunna
            "Lashaunna] curls up and starts to nibble on her nails...",
            "Lashaunna] says \"I have lived with several Zen masters, all of them cats.\"",
            "Lashaunna] says \"\"Meow\" means \"woof\" in cat.\"",
            "Lashaunna] stalks you from the shadows...",
            // luna moth
            "Luna Moth] says \"Heaven and earth and I are of the same root. Ten thousand things and I are of one substance.\"",
            "Luna Moth] says \"Nothing ever happens like you imagine it will\"",
            "Luna Moth] says \"Nothing ever exists entirely alone. Everything is in relation to everything else.\"",
            "Luna Moth] says \"The most common form of despair is not being who you are.\"",
            "Luna Moth] says \"The past has no power over the present moment.\"",
            "Luna Moth] says \"The truth is not always beautiful, nor beautiful words the truth.\"",
            "Luna Moth] says \"Things derive their being and nature by mutual dependence and are nothing in themselves.\"",
            "Luna Moth] says \"We are here to awaken from our illusion of separateness.\"",
            // mimic chest
            "Mimic Chest] says \"Feel free to scream whenever you want.\"",
            "Mimic Chest] says \"Sometimes, the things you see in the shadows are more than just shadows.\"",
            "Mimic Chest] says \"The end of the human race will be that it will eventually die of civilization.\"",
            "Mimic Chest] says \"When you look into an abyss, the abyss also looks into you.\"",
            // pet rock
            "Pet Rock] appears to be taking a nap",
            "Pet Rock] does not enjoy being juggled",
            "Pet Rock] gathers moss",
            "Pet Rock] has an unusual fear of pickaxes",
            "Pet Rock] is a big fan of rock and roll",
            "Pet Rock] ponders the meaning of life",
            "Pet Rock] rolls down a nearby hill",
            "Pet Rock] skips across a puddle",
            "Pet Rock] stares off into the distance",
            "Pet Rock] wants to play 'Rock, Paper, Scissors'",
            // phoenix baby
            "Phoenix Baby] says \"Auto correct can go straight to he’ll!!\"",
            "Phoenix Baby] says \"If you think I'm going to eat that, think again.\"",
            "Phoenix Baby] says \"Never gonna give you up, never gonna let you down, Never gonna run around and desert you! \"",
            "Phoenix Baby] says \"No time to explain, get in the llama!\"",
            "Phoenix Baby] says \"Seriously, you should take better care of your equipment.\"",
            "Phoenix Baby] says \"Some days you’re the pigeon and some days you’re the statue.\"",
            "Phoenix Baby] says \"Would your Mother approve? \"",
            // polar bear cub
            "Polar Bear Cub] dreams of eating a big... juicy... Hawkfin.",
            "Polar Bear Cub] says \"Climate change is a big threat for me. Please protect me and my icy home!\"",
            "Polar Bear Cub] says \"Have you brought any yummy seals or fish for me?\"",
            "Polar Bear Cub] says \"There are more polar bears than human inhabitants on Svalbard. Time for a rebellion!\"",
            // raccoon
            "Raccoon] climbs a tree.",
            "Raccoon] digs in a nearby garbage can.",
            "Raccoon] looks for berries.",
            "Raccoon] says \"Don’t assume the animal digging through your trash at night is a raccoon. It might just be a carny in a bear suit.\"",
            "Raccoon] says \"Progress is man's ability to complicate simplicity.\"",
            // sabretooth tigress
            "Sabretooth Tigress] drags an elephant into the room.. \"Look what I found! Can I keep it?\"",
            "Sabretooth Tigress] says \"*finds a wire to play with* Oops, was that a power cord? *looks innocent and checks if the wolf is around to blame*\"",
            "Sabretooth Tigress] says \"*sharpens nails* Are you sure you want to make me angry?\"",
            "Sabretooth Tigress] says \"A Tigress never loses sleep over the opinion of sheep.\"",
            "Sabretooth Tigress] says \"Curiosity killed the cat, luckily we have 9 lives.\"",
            "Sabretooth Tigress] says \"I am a kind master, just as long as you remember your place.\"",
            "Sabretooth Tigress] says \"I could very well be man's best friend but I would never stoop to admitting it.\"",
            "Sabretooth Tigress] says \"I’ve met many thinkers and many cats, but the wisdom of cats is infinitely superior.\"",
            "Sabretooth Tigress] says \"Some people say that felines are sneaky, evil, and cruel. This is not true. We have many other fine qualities as well.\"",
            "Sabretooth Tigress] says \"Women and cats will do as they please; men and dogs should relax and get used to the idea.\"",
            // shadowy figure
            "Shadowy Figure] says \"It's time to go incognito.\"",
            "Shadowy Figure] says \"Let's play some hide-and-seek, come find me if you can!\"",
            "Shadowy Figure] says \"Now you see me, now you don't.\"",
            "Shadowy Figure] says \"We are but dust and shadow.\"",
            "Shadowy Figure] says \"You don't find light by avoiding the darkness.\"",
            // slender cat
            "Slender Cat] purrs and purrs..",
            "Slender Cat] says \"It amazes me that people still assume I trip them by accident.\"",
            "Slender Cat] says \"I want my tummy rubbed.\"",
            "Slender Cat] says \"The floor is just the floor, but a piece of paper on the floor is a luxurious bed.\"",
            "Slender Cat] says \"Why do people get mad when I wake them up? They could always sleep during the day like I do.\"",
            // slug
            "Slug] says \"How do snails get their shells so shiny? They use snail varnish!\"",
            "Slug] says \"In philosophy if you aren't moving at a snail's pace you aren't moving at all. \"",
            "Slug] says \"What do you do when two snails have a fight? Leave them to slug it out! \"",
            // spiky turtle
            "Spiky Turtle] says \"Can't we just stop getting along? I just want to argue!\"",
            "Spiky Turtle] says \"Do you have any cookies? I really like cookies!\"",
            "Spiky Turtle] says \"Is that Chimera Poop? I need that for my collection!\"",
            "Spiky Turtle] says \"I used to be indecisive. Now I’m not so sure.\"",
            "Spiky Turtle] says \"What's that behind you? Eeek!!!\"",
            // vicious beaver
            "Vicious Beaver] says \"A closed mouth gathers no foot.\"",
            "Vicious Beaver] says \"A day without at least one laugh, is a day without sunshine.\"",
            "Vicious Beaver] says \"An idea is not responsible for who believes it.\"",
            "Vicious Beaver] says \"An intellectual is someone whose mind watches itself.\"",
            "Vicious Beaver] says \"Intellectual property has the shelf life of a banana.\"",
            // wildebeest
            "Wildebeest] says \"I advise you not to anger me or soon you will be seeing stars. \"",
            "Wildebeest] says \"I don't have much of a tail but I am certainly attached to it.\"",
            "Wildebeest] says \"I'm so fast that if you blink your eyes you will not see me passing by.\"",
            "Wildebeest] says \"I've been blue all my life. One learns to live with it.\"",
            "Wildebeest] says \"The reason humans move so slow is that they only have two legs.\"",
            "Wildebeest] says \"We are not fake gnus! We just leave alternative tracks.\"",
            "Wildebeest] says \"What do you call a confused Wildebeest? A Bewilderedbeest.\"",
            // wombat
            "Wombat] says \"An animal's eyes have the power to speak a great language.\"",
            "Wombat] says \"Four legs good, two legs bad.\"",
            "Wombat] says \"The greatness of a nation and its moral progress can be judged by the way its animals are treated.\"",
            "Wombat] says \"Until one has loved an animal, a part of one's soul remains unawakened.\"",
            // woolly mammoth
            "Woolly Mammoth] says \"A banker is a fellow who lends you his umbrella when the sun is shining, but wants it back the minute it begins to rain.\"",
            "Woolly Mammoth] says \"A Lie can travel halfway around the world while Truth is still putting it's shoes on.\"",
            "Woolly Mammoth] says \"Climate is what we expect, weather is what we get.\"",
            "Woolly Mammoth] says \"It usually takes more than three weeks to write a good impromptu speech.\"",
            "Woolly Mammoth] says \"It's better to keep your mouth shut and appear stupid than to open it and remove all doubt.\"",
            "Woolly Mammoth] says \"Man is the only animal that blushes. Or needs to.\"",
            "Woolly Mammoth] says \"Many a small thing has been made large by the right kind of advertising.\"",
            "Woolly Mammoth] says \"Never argue with a fool, onlookers may not be able to tell the difference.\"",
            "Woolly Mammoth] says \"Reality can be beaten with enough imagination.\""
        ];

        if (chat !== null) {
            for (i in chat) {
                //object is a message (excludes broadcasts and interactive notices)
                //and object is a pet quote
                if ((chat[i].className === "cmsg") && (chat[i].children[1].className === "pMsg")) {
                    chatPet = chat[i].children[1].children[0];

                    //don't want to muck up the original
                    chatQuote = chatPet.cloneNode(true);

                    //check if pet is named
                    if (chatQuote.children[0].children.length > 0) {
                        //clear pet type and name
                        chatQuote.children[0].children[1].textContent = "";
                        //stringify and remove italics
                        chatQuote = chatQuote.textContent;
                        //replace triple space with double to match unnamed formatting
                        chatQuote = chatQuote.split("   ");
                        chatQuote = chatQuote.join("  ");
                    } else {
                        //stringify and remove italics
                        chatQuote = chatQuote.textContent;
                    }

                    //replace all double spaces with single
                    chatQuote = chatQuote.split("  ");
                    chatQuote = chatQuote.join(" ");

                    //splits off player handle
                    chatQuote = chatQuote.split("\'s [");
                    ownerName = chatQuote[0];

                    //replace any occurences of the owner's handle and we finally have just the quote
                    finalQuote = chatQuote[1].replace(ownerName, ownerFillIn);

                    if (quoteList.includes(finalQuote)) {
                        chatPet.style.color = oldQuoteColor;
                    } else {
                        chatPet.style.color = newQuoteColor;
                    }
                } // if
            } // for
        } // if
    } // function petQuote
}
