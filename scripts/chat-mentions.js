// ==UserScript==
// @name         Chat Mentions
// @namespace    http://tampermonkey.net/
// @version      2019-02-06-1437
// @description  Colors chat mentions.
// @author       Persica
// @match        https://*.drakor.com*
// ==/UserScript==

persicasChatMentions();

function persicasChatMentions() {
    "use strict";
    //really ought to be part of subfunction, but at top to make customizing easier
    var userNames = ["persica", "persi", "pers"]; //make all lower-case to simplify comparison
    var mentionColor = "MediumOrchid";

    var milliSeconds = 500;

    setInterval(colorChat, milliSeconds);

    // noinspection NestedFunctionJS
    function colorChat() {
        var chat = document.getElementById("dr_chatBody").children;
        var classList;
        var base;
        var i;

        var worldChat = "cMsg";

        if (chat !== null) {
            for (i in chat) {
                //message; excludes broadcasts and interactive notices
                if (chat[i].className === "cmsg") {

                    //staff badges bump dom children down
                    classList = chat[i].children[1].className.split(" ");

                    if (classList.some(checkForGameStaff)) {
                        base = 1;
                    } else {
                        base = 0;
                    }

                    if (chat[i].children.length > (base + 2)) {
                        if (chat[i].children[base + 2].className === worldChat) {
                            colorMentions(chat[i], base);
                        }
                    }
                } //if: cmsg
            } //for
        } //if: chat
    }

    // noinspection NestedFunctionJS
    function checkForGameStaff(value) {
        //TODO get misqueeter's content manager tag class
        var staffBadges = ["chatMod", "chatAdmin"];
        return staffBadges.includes(value);
    }

    // noinspection NestedFunctionJS
    function colorMentions(chatLine, base) {
        var userMentioned;
        var changeColor = false;
        var triggerChar = "@";

        //this is who said it, needed for coloring
        var chatSayer = chatLine.children[base + 1].children[0];

        //this is just what's said, w/o sayer name or guild tags
        var chatText = chatLine.children[base + 2];

        //don't want to muck up the original
        var chatWorking1 = chatText.textContent;

        //separate out any mentions
        var chatWorking2 = chatWorking1.split(triggerChar);

        //was *anybody* mentioned?
        if (chatWorking2.length > 1) {

            //now to isolate name mentioned
            // noinspection ReuseOfLocalVariableJS
            userMentioned = chatWorking2[1].split(" ")[0];

            //make everything lower-case to simplify comparison
            userMentioned = userMentioned.toLowerCase();

            if (userNames.includes(userMentioned)) {
                changeColor = true;
            }
        }

        if (changeColor) {
            chatSayer.style.color = mentionColor;
            chatText.style.color = mentionColor;
        }
    }
}
