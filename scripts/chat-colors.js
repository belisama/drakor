/*global GM_config */

// ==UserScript==
// @name         Chat Coloring
// @namespace    http://tampermonkey.net/
// @version      2018/06/30 07:45
// @description  Configurable colors for Drakor Chat
// @author       Persica
// @match        http://*.drakor.com*
// @match        https://*.drakor.com*
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

var configMenu;
var milliSeconds = 50;
var subChannels = {
    //pretty name, short name, background, border, channel, username, text, guild tag
    "wMsg": ["Whispers", "whisper", "#550055", "#990099", "#aaaaaa", "#cccccc", "#dd00dd", false],
    "tMsg": ["Trade Chat", "trade", "#555511", "#999944", "#aaaaaa", "#cccccc", "#cccc55", true],
    "sMsg": ["Support Chat", "support", "#774422", "#cc7722", "#aaaaaa", "#cccccc", "#dd9933", true],
    "rMsg": ["Recruitment Chat", "recruitment", "#115555", "#22aaaa", "#aaaaaa", "#cccccc", "#55cccc", true],
    //event
    "gMsg": ["Guild Chat", "guild", "#006600", "#33aa33", "#aaaaaa", "#cccccc", "#33dd33", false],
};

function colorChat() {
    "use strict";
    var chat = document.getElementById("dr_chatBody").children;
    var base;
    var channel;
    var i;

    if (chat !== null) {
        for (i = 0; i < chat.length; i++) {

            base = 0;

            //message; excludes broadcasts and interactive notices
            if (chat[i].className === "cmsg") {

                //staff badges bump dom children down
                base = checkForGameStaff(chat[i], base);

                if ((chat[i].children[base + 1].className === "pMsg")) {
                    colorPet(chat[i], base);
                }

                if ((chat[i].children.length > base+2)) {
                     if (chat[i].children[base + 2].className === "cMsg") {
                        //todo chatWorld(chat[i], base);
                        colorMentions(chat[i], base);
                    }
                }

                if (chat[i].children.length > base+3) {
                    channel = chat[i].children[base + 3].className;
                    if (subChannels.channel !== "") {
                        colorSubchannel(chat[i], base, subChannels.channel);
                    }
                }


                //todo guild tag color?
                // if (channel[7] && GM_config.get("guildTagToggle") && (chatLine.children[base + 2].children[0].children[0] !== null)) {
                //     chatLine.children[base + 2].children[0].children[0].style.color = GM_config.get("guildTagColor")
                // }

            } //if: cmsg
        } //for
    } //if: chat
}

function checkForGameStaff(chatLine, base) {
    "use strict";
    //TODO get goz"s gm tag class
    var classList = chatLine.children[1].className.split(" ");
    var baseLocal = base;

    if (classList.includes("chatMod")) {
        baseLocal += 1;
    }

    return baseLocal;
}

function colorPet(chatLine, base) {
    "use strict";

    if (GM_config.get("petToggle")) {
        chatLine.children[base + 1].style.color = GM_config.get("petColor");
    } else {
        chatLine.children[base + 1].style.color = "";
    }
}

function colorMentions(chatLine, base) {
    "use strict";
    var userMentioned;
    var userNames;
    var chatSayer;
    var chatText;
    var chatWorking;
    var changeColor = false;
    var triggerChar = "@";

    //this is who said it, needed for coloring
    chatSayer = chatLine.children[base + 1].children[0];

    //this is just what"s said, w/o sayer name or guild tags
    chatText = chatLine.children[base + 2];

    if (GM_config.get("mentionToggle")) {

        //don"t want to muck up the original
        chatWorking = chatText.textContent;

        //separate out any mentions
        chatWorking = chatWorking.split(triggerChar);

        //what are we comparing it to
        userNames = GM_config.get("mentionNames");
        userNames = userNames.split(",");

        //was *anybody* mentioned?
        if (chatWorking.length > 1) {

            //now to isolate name mentioned
            userMentioned = chatWorking[1].split(" ")[0];

            if (userNames.includes(userMentioned)) {
                changeColor = true;
            }
        }
    }

    if (changeColor) {
        chatSayer.style.color = GM_config.get("mentionColor");
        chatText.style.color = GM_config.get("mentionColor");
    } else {
        chatSayer.style.color = "";
        chatText.style.color = "";
    }
}

function colorSubchannel(chatLine, base, channel) {
    "use strict";

    if (GM_config.get(channel + "Toggle")) { //FIXME
        chatLine.children[base + 1].style.backgroundColor = GM_config.get(channel[1] + "ColorBackground");
        chatLine.children[base + 1].style.borderColor = GM_config.get(channel[1] + "ColorBorder");
        chatLine.children[base + 1].style.color = GM_config.get(channel[1] + "ColorChannel");
        chatLine.children[base + 2].style.backgroundColor = GM_config.get(channel[1] + "ColorBackground");
        chatLine.children[base + 2].style.borderColor = GM_config.get(channel[1] + "ColorBorder");
        chatLine.children[base + 2].children[0].style.color = GM_config.get(channel[1] + "ColorUsername");
        chatLine.children[base + 3].style.color = GM_config.get(channel[1] + "ColorText");
    } else {
        chatLine.children[base + 1].style.backgroundColor = "";
        chatLine.children[base + 1].style.borderColor = "";
        chatLine.children[base + 1].style.color = "";
        chatLine.children[base + 2].style.backgroundColor = "";
        chatLine.children[base + 2].style.borderColor = "";
        chatLine.children[base + 2].children[0].style.color = "";
        chatLine.children[base + 3].style.color = "";
    }
}

function displayConfigLink() {
    "use strict";
    var menuBar = document.getElementById("gs_topmenu");
    var link;
    var text;

    text = document.createTextNode("Chat Colors");

    link = document.createElement("A");
    link.className = "gs_topmenu_item";
    link.appendChild(text);
    link.onclick = function() { GM_config.open(); };

    menuBar.appendChild(link);
}

function buildConfigMenu() {
    "use strict";
    var i;
    var menu;
    var channel;
    var channels = Object.keys(subChannels);

    menu = {
        "id": "ChatColors",
        "css": "#ChatColors_wrapper { column-count: 2; } #ChatColors_header { column-span: all; }",
        "title": buildConfigTitle(),
        "fields": {
            "mentionToggle": {
                "section": "Chat Mentions (@name)",
                "label": "Color Chat Mentions?",
                "type": "checkbox",
                "default": false
            },
            "mentionColor": {
                "label": "Text and Username Color",
                "type": "text",
                "default": "MediumOrchid"
            },
            "mentionNames": {
                "label": "User names to check for (comma separated list, case-sensitive)",
                "type": "text",
                "size": 100
            },
            "petToggle": {
                "section": "Pet Chat",
                "label": "Color Pet Chat?",
                "type": "checkbox",
                "default": false
            },
            "petColor": {
                "label": "Text Color",
                "type": "text",
                "default": "#9f9f9f"
            },
            "guildTagToggle": {
                "section": "Guild Tag",
                "label": "Color Guild Tag?",
                "type": "checkbox",
                "default": false
            },
            "guildTagColor": {
                "label": "Text Color",
                "type": "text",
                "default": "#3d3",
            },
        },
    };

    for (i = 0; i < channels.length; i++) {
        channel = channels[i];
        menu.fields[subChannels[channel][1] + "Toggle"] = {
            "section": subChannels[channel][0],
            "label": "Color " + subChannels[channel][0] + "?",
            "type": "checkbox",
            "default": false
        };
        menu.fields[subChannels[channel][1] + "ColorBackground"] = {
            "label": "Channel & Username Background Color",
            "type": "text",
            "default": subChannels[channel][2]
        };
        menu.fields[subChannels[channel][1] + "ColorBorder"] = {
            "label": "Channel & Username Border Color",
            "type": "text",
            "default": subChannels[channel][3]
        };
        menu.fields[subChannels[channel][1] + "ColorChannel"] = {
            "label": "Channel Text Color",
            "type": "text",
            "default": subChannels[channel][4]
        };
        menu.fields[subChannels[channel][1] + "ColorUsername"] = {
            "label": "Username Text Color",
            "type": "text",
            "default": subChannels[channel][5]
        };
        menu.fields[subChannels[channel][1] + "ColorText"] = {
            "label": "Message Text Color",
            "type": "text",
            "default": subChannels[channel][6]
        };
    }

    return menu;
}

function buildConfigTitle() {
    "use strict";
    var title;
    var titleLine;

    title = document.createElement("DIV");

    titleLine = document.createElement("SPAN");
    titleLine.textContent = "Chat Color Configuration";
    title.appendChild(titleLine);

    titleLine = document.createElement("BR");
    title.appendChild(titleLine);

    titleLine = document.createElement("A");
    titleLine.textContent = "(colors can be any valid CSS color designation)";
    titleLine.href = "https://www.w3schools.com/colors/colors_picker.asp";
    titleLine.target = "_blank";
    title.appendChild(titleLine);

    return title;
}

//main logic

configMenu = buildConfigMenu();
GM_config.init(configMenu);

displayConfigLink();

setInterval(colorChat, milliSeconds);
