// ==UserScript==
// @name         Cheap Pet Food on Market
// @namespace    http://tampermonkey.net/
// @version      2019-02-06-1352
// @description  Highlights pet food on market cheaper than OLW.
// @author       Persica
// @match        https://drakor.com/market
// ==/UserScript==

persicasMarketPrices();

function persicasMarketPrices() {
    "use strict";
    var milliSeconds = 500;

    // run constantly
    setInterval(highlightMarket, milliSeconds);

    // noinspection NestedFunctionJS
    function highlightMarket() {
        var petFoods = {
            "Acorn": 1215,
            "Aged Writings": 270,
            "Amber": 1155,
            "Ancient Book": 3690,
            "Ancient Essence": 7380,
            "Ancientbloom": 3690,
            "Angelfish": 1080,
            "Angelflower": 645,
            //"Animal Fat": -, event mat
            "Apple": 540,
            "Apricot": 1080,
            "Archaic Rune": 3690,
            "Arcaneleaf": 360,
            "Banana": 885,
            "Basic Parchment": 915,
            "Bigtooth Aspen": 780,
            "Black Ash": 3690,
            "Black Spruce": 705,
            "Blackened Stone": 915,
            "Blackfish": 1485,
            "Blood Berry": 705,
            "Boiled Gilgrash": 3690, //uses price for "Gilgrash"
            "Bone Remains": 645,
            "Catfish": 540,
            "Chestnut": 795,
            //"Cocoa Beans": -, event mat
            "Dark Honey": 1455,
            "Delvrak": 1815,
            "Dogweed": 2010,
            "Eagletear": 3435,
            "Eel": 90,
            "Egg": 270,
            "Fire Goby": 855,
            "Flame Pearl": 2220,
            "Flounder": 240,
            "Frostweed": 3285,
            "Gem Fragment": 330,
            "Ghostfish": 1455,
            "Ghostshroom": 1830,
            "Gilgrash": 3690, //extends "Boiled Gilgrash"
            "Goldenbush": 60,
            "Golemite Ore": 3690,
            "Gorethistle": 450,
            "Hawkfin": 5715,
            "Herring": 510,
            "Honey": 855,
            "Horsetail": 1305,
            "Icebloom": 2370,
            "Icefish": 510,
            "Iron Ore": 480,
            "Kingsbloom": 135,
            "Ladyslipper": 735,
            "Lancetail": 1155,
            "Large Egg": 1155,
            "Limestone": 240,
            "Lionfish": 885,
            "Liontail": 1920,
            "Lunar Dust": 420,
            "Mackerel": 405,
            "Metal Fragment": 1080,
            "Midnight Berry": 60,
            "Milkweed": 1200,
            "Mineral Chunk": 435,
            "Moonbeard": 600, //fixme
            "Moonlight Essence": 2310,
            "Nightfall Dust": 810,
            "Orange": 675,
            "Peach": 1485,
            //"Peppermint": -, event mat
            "Perch": 300,
            "Phantomlily": 1245, //fixme
            "Pickerel": 150,
            "Pike": 90,
            "Pixieroot": 135,
            "Red Cedar": 150,
            "Rockfish": 675,
            "Rosemary": 60,
            "Salmon": 555,
            "Salt": 675,
            "Sandstone": 105,
            "Shadow Fish": 705,
            "Shale Rock": 165,
            "Shrimp": 150,
            "Silver Maple": 1455,
            "Simple Dust": 210,
            "Spiderfoot": 420,
            "Spiny Piranha": 2625,
            "Spiritherb": 195,
            "Squid": 855,
            "Stone": 60,
            "Stonesilk Lily": 1485,
            "Sunfire Dust": 60,
            "Sunlight Dust": 1020,
            "Sunray": 375,
            "Tin Ore": 270,
            "Trout": 90,
            "Trunkfish": 405,
            "Walnut": 510,
            "Waterberry": 450,
            "White Pine": 300,
            "Wild Cherry": 2220,
            "Wolfsbane": 330,
        };

        var resultsContainer = document.getElementById("displayResults");
        var resultsTable;

        var matName;
        var matPriceAlpha;
        var matPriceNum;
        var matForm;

        var foods = Object.keys(petFoods);
        var i;

        if (resultsContainer.innerText !== "") {
            resultsTable = resultsContainer.children[3];

            //resultsTable is a dom element, NOT an array
            for (i = 0; i < resultsTable.children.length; i++) {
                matName = resultsTable.children[i].children[0].children[0].children[0].children[0].textContent;
                matName = matName.replace("[", "");
                matName = matName.replace("]", "");

                matPriceAlpha = resultsTable.children[i].children[0].children[1].children[1].textContent;
                matPriceAlpha = matPriceAlpha.replace("g Each", "");
                matPriceAlpha = matPriceAlpha.replace(",", "");
                matPriceNum = Number(matPriceAlpha);

                matForm = resultsTable.children[i].children[0];

                if (foods.includes(matName) && matPriceNum <= petFoods[matName]) {
                    matForm.style.border = "thick solid red";
                }
            }//end-for
        }//end-if
    }
}
