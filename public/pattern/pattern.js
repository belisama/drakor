"use strict";

//Infrastructure
var patternTree = [];

var tsAlc = "Alchemy";
var tsAnc = "Ancient Research";
var tsCom = "Combat";
var tsCon = "Construction";
var tsCoo = "Cooking";
var tsCra = "Crafting";
var tsDis = "Disenchanting";
var tsEnc = "Enchanting";
var tsFis = "Fishing";
var tsGat = "Gathering";
var tsIns = "Inscription";
var tsJew = "Jewelcrafting";
var tsLog = "Logging";
var tsMin = "Mining";
var tsSmi = "Smithing";
var tsTre = "Treasure Hunting";

var dataTradeskill = {};
var dataAlphabet = {};
var dataReverse = {};

var dataForward = {
    // alchemy
    "Amnesia Antidote Potion": {ts: tsAlc, fl: "A", op: 1, display: "Amnesia Antidote", components: [
        {compName: "Fresh Water", ip: 3},
        {compName: "Concoct Midnightgold Toxin", ip: 1},
        {compName: "Memory Powder", ip: 8},
    ]},
    "Ancientbloom Oil": {ts: tsAlc, fl: "A", op: 1, components: [
        {compName: "Ancientbloom", ip: 4},
    ]},
    "Angel Dragon Damage (Epic+)": {ts: tsAlc, fl: "A", op: 1, components: [
        {compName: "Extract Kingsbloom Oil", ip: 1},
        {compName: "Angel Dragon Toxin", ip: 2},
    ]},
    "Angel Dragon Toxin": {ts: tsAlc, fl: "A", op: 1, components: [
        {compName: "Extract Angelflower Oil", ip: 3},
        {compName: "Shadow Essence", ip: 1},
        {compName: "Extract Dragontear Oil", ip: 1},
    ]},
    "Angelpixie Heal Potion (Rare+)": {ts: tsAlc, fl: "A", op: 1, components: [
        {compName: "Extract Pixieroot Oil", ip: 3},
        {compName: "Extract Angelflower Oil", ip: 2},
    ]},
    "Basic Battle Damage": {ts: tsAlc, fl: "B", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Extract Midnight Oil", ip: 3},
    ]},
    "Basic Random Curse (Rare)": {ts: tsAlc, fl: "B", op: 1, components: [
        {compName: "Concoct Midnightgold Toxin", ip: 3},
        {compName: "Mystic Essence", ip: 3},
    ]},
    "Basic Random Curse (Superior+)": {ts: tsAlc, fl: "B", op: 1, components: [
        {compName: "Arcane Powder", ip: 3},
        {compName: "Concoct Midnightgold Toxin", ip: 3},
    ]},
    "Black Lotus Acid": {ts: tsAlc, fl: "B", op: 4, components: [
        {compName: "Frostweed", ip: 1},
        {compName: "Black Lotus", ip: 1},
        {compName: "Golembloom", ip: 1},
    ]},
    "Blacksilk Paste": {ts: tsAlc, fl: "B", op: 1, components: [
        {compName: "Silkweed Paste", ip: 3},
    ]},
    "Blacksilk Surprise": {ts: tsAlc, fl: "B", op: 1, components: [
        {compName: "Tiger Lily Oil", ip: 1},
        {compName: "Blacksilk Paste", ip: 3},
    ]},
    "Blood Berry Paste": {ts: tsAlc, fl: "B", op: 1, components: [
        {compName: "Blood Berry", ip: 8},
    ]},
    "Blood Berry Random": {ts: tsAlc, fl: "B", op: 1, components: [
        {compName: "Blood Berry Paste", ip: 4},
    ]},
    "Cherry Oil": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Cherry", ip: 5},
    ]},
    "Cherrygarlic Curse": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Honey Mushroom Paste", ip: 1},
        {compName: "Cherrygarlic Toxin", ip: 2},
    ]},
    "Cherrygarlic Mystery": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Cherrygarlic Toxin", ip: 3},
    ]},
    "Cherrygarlic Toxin": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Cherry Oil", ip: 2},
        {compName: "Wild Garlic Oil", ip: 1},
    ]},
    "Cinnamon Seaweed Oil": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Cinnamon Seaweed", ip: 5},
    ]},
    "Compost": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Fallen Leaves", ip: 8},
    ]},
    "Concoct Goldenrose Toxin": {ts: tsAlc, fl: "C", op: 1, display: "Goldenrose Toxin", components: [
        {compName: "Egg", ip: 2},
        {compName: "Extract Goldenbush Oil", ip: 4},
        {compName: "Extract Rosemary Oil", ip: 4},
    ]},
    "Concoct Goreberry Toxin": {ts: tsAlc, fl: "C", op: 1, display: "Goreberry Toxin", components: [
        {compName: "Extract Gorethistle Oil", ip: 2},
        {compName: "Extract Waterberry Oil", ip: 2},
        {compName: "Large Egg", ip: 2},
    ]},
    "Concoct Midnightgold Toxin": {ts: tsAlc, fl: "C", op: 1, display: "MidnightGold Toxin", components: [
        {compName: "Extract Goldenbush Oil", ip: 1},
        {compName: "Extract Midnight Oil", ip: 1},
        {compName: "Salt", ip: 1},
    ]},
    "Concoct Mooncreeper Toxin": {ts: tsAlc, fl: "C", op: 1, display: "Mooncreeper Toxin", components: [
        {compName: "Extract Moonbeard Oil", ip: 2},
        {compName: "Crushed Sandcreeper Paste", ip: 3},
    ]},
    "Concoct Pixiebloom Toxin": {ts: tsAlc, fl: "C", op: 1, display: "Pixiebloom Toxin", components: [
        {compName: "Egg", ip: 1},
        {compName: "Extract Kingsbloom Oil", ip: 2},
        {compName: "Extract Pixieroot Oil", ip: 2},
    ]},
    "Corpsepoppy Oil": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Corpsepoppy Oil", ip: 5},
    ]},
    "Crawlingroot Paste": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Crawlingroot", ip: 8},
    ]},
    "Crawlingroot Random": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Crawlingroot Paste", ip: 4},
    ]},
    "Crushed Sandcreeper Paste": {ts: tsAlc, fl: "C", op: 1, display: "Sandcreeper Paste", components: [
        {compName: "Sandcreeper", ip: 6},
    ]},
    "Crushed Spiritherb Paste": {ts: tsAlc, fl: "C", op: 1, display: "Spiritherb Paste", components: [
        {compName: "Spiritherb", ip: 6},
    ]},
    "Crushed Wolfsbane Paste": {ts: tsAlc, fl: "C", op: 1, display: "Wolfsbane Paste", components: [
        {compName: "Wolfsbane", ip: 6},
    ]},
    "Cursed Agate": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Concoct Pixiebloom Toxin", ip: 1},
        {compName: "Polish Large Agate", ip: 1},
    ]},
    "Cursed Amber": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Ladyphantom Toxin", ip: 1},
        {compName: "Polish Large Agate", ip: 1},
    ]},
    "Cursed Amethyst": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Polish Large Amethyst", ip: 1},
        {compName: "Vilecorpse Toxin", ip: 1},
    ]},
    "Cursed Aquamarine": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Polish Large Aquamarine", ip: 1},
        {compName: "Tigerfrost Toxin", ip: 1},
    ]},
    "Cursed Citrine": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Concoct Goreberry Toxin", ip: 1},
        {compName: "Polish Large Citrine", ip: 1},
    ]},
    "Cursed Garnet": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Concoct Mooncreeper Toxin", ip: 1},
        {compName: "Polished Large Garnet", ip: 1},
    ]},
    "Cursed Gem": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Concoct Midnightgold Toxin", ip: 1},
        {compName: "Polish Large Gem", ip: 1},
    ]},
    "Cursed Malachite": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Crushed Spiritherb Paste", ip: 1},
        {compName: "Concoct Goreberry Toxin", ip: 1},
        {compName: "Polish Large Malachite", ip: 1},
    ]},
    "Cursed Melanite": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Icesilk Toxin", ip: 1},
        {compName: "Polish Large Melanite", ip: 1},
    ]},
    "Cursed Mercury": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Cherrygarlic Toxin", ip: 1},
        {compName: "Polish Large Mercury", ip: 1},
    ]},
    "Cursed Peridot": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Nightorchid Toxin", ip: 1},
        {compName: "Polish Large Peridot", ip: 1},
    ]},
    "Cursed Sun Opal": {ts: tsAlc, fl: "C", op: 1, components: [
        {compName: "Ghostmedusa Toxin", ip: 1},
        {compName: "Polish Large Sun Opal", ip: 1},
    ]},
    "Devilpetal Paste": {ts: tsAlc, fl: "D", op: 1, components: [
        {compName: "Devilpetal", ip: 8},
    ]},
    "Devilpetal Random": {ts: tsAlc, fl: "D", op: 1, components: [
        {compName: "Devilpetal Paste", ip: 4},
    ]},
    "Devilweed Acid": {ts: tsAlc, fl: "D", op: 4, components: [
        {compName: "Liontail", ip: 1},
        {compName: "Devilweed", ip: 1},
        {compName: "Ancientbloom", ip: 1},
    ]},
    "Dragontear Acid": {ts: tsAlc, fl: "D", op: 4, components: [
        {compName: "Horsetail", ip: 1},
        {compName: "Dragontear", ip: 1},
        {compName: "Ancientbloom", ip: 1},
    ]},
    "Exploding Potion": {ts: tsAlc, fl: "E", op: 1, components: [
        {compName: "Exploding Berries", ip: 5},
    ]},
    "Extract Angelflower Oil": {ts: tsAlc, fl: "E", op: 1, display: "Angelflower Oil", components: [
        {compName: "Angelflower", ip: 5},
    ]},
    "Extract Dragontear Oil": {ts: tsAlc, fl: "E", op: 1, display: "Dragontear Oil", components: [
        {compName: "Dragontear", ip: 4},
    ]},
    "Extract Goldenbush Oil": {ts: tsAlc, fl: "E", op: 1, display: "Goldenbush Oil", components: [
        {compName: "Goldenbush", ip: 4},
    ]},
    "Extract Gorethistle Oil": {ts: tsAlc, fl: "E", op: 1, display: "Gorethistle Oil", components: [
        {compName: "Gorethistle", ip: 5},
    ]},
    "Extract Kingsbloom Oil": {ts: tsAlc, fl: "E", op: 1, display: "Kingsbeard Oil", components: [
        {compName: "Kingsbloom", ip: 5},
    ]},
    "Extract Ladyslipper Oil": {ts: tsAlc, fl: "E", op: 1, display: "Ladyslipper Oil", components: [
        {compName: "Ladyslipper", ip: 5},
    ]},
    "Extract Midnight Oil": {ts: tsAlc, fl: "E", op: 1, display: "Midnight Oil", components: [
        {compName: "Midnight Berry", ip: 4},
    ]},
    "Extract Moonbeard Oil": {ts: tsAlc, fl: "E", op: 1, display: "Moonbeard Oil", components: [
        {compName: "Moonbeard", ip: 5},
    ]},
    "Extract Phantomlily Oil": {ts: tsAlc, fl: "E", op: 1, display: "Phantomlily Oil", components: [
        {compName: "Phantomlily", ip: 5},
    ]},
    "Extract Pixieroot Oil": {ts: tsAlc, fl: "E", op: 1, display: "Pixieroot Oil", components: [
        {compName: "Pixieroot", ip: 5},
    ]},
    "Extract Rosemary Oil": {ts: tsAlc, fl: "E", op: 1, display: "Rosemary Oil", components: [
        {compName: "Rosemary", ip: 5},
    ]},
    "Extract Waterberry Oil": {ts: tsAlc, fl: "E", op: 1, display: "Waterberry Oil", components: [
        {compName: "Waterberry", ip: 5},
    ]},
    "Eye of Newt": {ts: tsAlc, fl: "E", op: 1, components: []},
    "Fire of Belly": {ts: tsAlc, fl: "F", op: 1, components: []},
    "Flame Lotus Acid": {ts: tsAlc, fl: "F", op: 4, components: [
        {compName: "Wild Garlic", ip: 1},
        {compName: "Flame Lotus", ip: 1},
        {compName: "Nightwolf", ip: 1},
    ]},
    "Flameshroom Paste": {ts: tsAlc, fl: "F", op: 1, components: [
        {compName: "Honey Mushroom Paste", ip: 3},
    ]},
    "Flameshroom Surprise": {ts: tsAlc, fl: "F", op: 1, components: [
        {compName: "Cherry Oil", ip: 1},
        {compName: "Flameshroom Paste", ip: 3},
    ]},
    "Frostweed Oil": {ts: tsAlc, fl: "F", op: 1, components: [
        {compName: "Frostweed", ip: 5},
    ]},
    "Ghostmedusa Curse": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Devilpetal Paste", ip: 1},
        {compName: "Ghostmedusa Toxin", ip: 2},
    ]},
    "Ghostmedusa Mystery": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Ghostmedusa Toxin", ip: 3},
    ]},
    "Ghostmedusa Toxin": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Medusaroot Oil", ip: 2},
        {compName: "Ghostshroom Oil", ip: 1},
    ]},
    "Ghostshroom Oil": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Ghostshroom", ip: 5},
    ]},
    "Ghouldevil Paste": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Devilpetal Paste", ip: 3},
        {compName: "Ghoulweed Oil", ip: 1},
    ]},
    "Ghouldevil Surprise": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Medusaroot Oil", ip: 1},
        {compName: "Ghouldevil Paste", ip: 3},
    ]},
    "Ghoulspider Mystery": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Vilegourd Oil", ip: 1},
        {compName: "Ghoulspider Paste", ip: 3},
    ]},
    "Ghoulspider Paste": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Spiderfoot Paste", ip: 3},
        {compName: "Ghoulweed Oil", ip: 1},
    ]},
    "Ghoulweed Oil": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Ghoulweed", ip: 4},
    ]},
    "Golden Dragon Curse (Epic+)": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Golden Dragon Toxin", ip: 2},
    ]},
    "Golden Dragon Toxin": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Extract Goldenbush Oil", ip: 3},
        {compName: "Mystic Essence", ip: 1},
        {compName: "Extract Dragontear Oil", ip: 1},
    ]},
    "Gore Dragon Cleanse (Epic)": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Extract Gorethistle Oil", ip: 2},
        {compName: "Spirit Dragon Paste", ip: 1},
    ]},
    "Goreberry Mystery (Rare+)": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Concoct Goreberry Toxin", ip: 2},
    ]},
    "Goreherb-berry Surprise (Sup+)": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Extract Gorethistle Oil", ip: 2},
        {compName: "Crushed Spiritherb Paste", ip: 2},
        {compName: "Extract Waterberry Oil", ip: 2},
    ]},
    "Ground Spices": {ts: tsAlc, fl: "G", op: 1, components: [
        {compName: "Holiday Spices", ip: 3},
    ]},
    "Happy Potion": {ts: tsAlc, fl: "H", op: 1, components: [
        {compName: "Essense of Joy", ip: 8},
    ]},
    "Honey Mushroom Paste": {ts: tsAlc, fl: "H", op: 1, components: [
        {compName: "Honey Mushroom", ip: 8},
    ]},
    "Honey Mushroom Random": {ts: tsAlc, fl: "H", op: 1, components: [
        {compName: "Honey Mushroom Paste", ip: 4},
    ]},
    "Icebloom Oil": {ts: tsAlc, fl: "I", op: 1, components: [
        {compName: "Icebloom", ip: 5},
    ]},
    "Icesilk Curse": {ts: tsAlc, fl: "I", op: 1, components: [
        {compName: "Blood Berry Paste", ip: 1},
        {compName: "Icesilk Toxin", ip: 2},
    ]},
    "Icesilk Mystery": {ts: tsAlc, fl: "I", op: 1, components: [
        {compName: "Icesilk Toxin", ip: 3},
    ]},
    "Icesilk Toxin": {ts: tsAlc, fl: "I", op: 1, components: [
        {compName: "Stonesilk Lily Oil", ip: 2},
        {compName: "Icebloom Oil", ip: 1},
    ]},
    "Kingsbloom Curse (Common)": {ts: tsAlc, fl: "K", op: 1, components: [
        {compName: "Pixieroot", ip: 2},
        {compName: "Extract Kingsbloom Oil", ip: 1},
    ]},
    "Kingsbloom Curse (Superior+)": {ts: tsAlc, fl: "K", op: 1, components: [
        {compName: "Egg", ip: 1},
        {compName: "Extract Kingsbloom Oil", ip: 5},
    ]},
    "Kingsflower Buff (Superior+)": {ts: tsAlc, fl: "K", op: 1, components: [
        {compName: "Extract Kingsbloom Oil", ip: 3},
        {compName: "Angelflower", ip: 2},
    ]},
    "Ladyphantom ??? (Rare+)": {ts: tsAlc, fl: "L", op: 1, components: [
        {compName: "Ladyphantom Toxin", ip: 3},
    ]},
    "Ladyphantom Buff (Rare+)": {ts: tsAlc, fl: "L", op: 1, components: [
        {compName: "Orange", ip: 2},
        {compName: "Ladyphantom Toxin", ip: 2},
    ]},
    "Ladyphantom Toxin": {ts: tsAlc, fl: "L", op: 1, components: [
        {compName: "Extract Ladyslipper Oil", ip: 2},
        {compName: "Extract Phantomlily Oil", ip: 2},
    ]},
    "Love Potion no.9": {ts: tsAlc, fl: "L", op: 1, components: [
        {compName: "A Touch of Jasmine", ip: 8},
    ]},
    "Medusaroot Oil": {ts: tsAlc, fl: "M", op: 1, components: [
        {compName: "Medusaroot", ip: 5},
    ]},
    "Mooncreeper Random (Rare+)": {ts: tsAlc, fl: "M", op: 1, components: [
        {compName: "Concoct Mooncreeper Toxin", ip: 2},
    ]},
    "Mooncreeper Surprise (Rare+)": {ts: tsAlc, fl: "M", op: 1, components: [
        {compName: "Concoct Mooncreeper Toxin", ip: 3},
    ]},
    "Moonphantom Heal (Rare+)": {ts: tsAlc, fl: "M", op: 1, components: [
        {compName: "Orange", ip: 1},
        {compName: "Moonphantom Toxin", ip: 1},
    ]},
    "Moonphantom Toxin": {ts: tsAlc, fl: "M", op: 1, components: [
        {compName: "Extract Moonbeard Oil", ip: 2},
        {compName: "Extract Phantomlily Oil", ip: 2},
    ]},
    "Mystical Battle Heal (Rare)": {ts: tsAlc, fl: "M", op: 1, components: [
        {compName: "Egg", ip: 1},
        {compName: "Extract Goldenbush Oil", ip: 2},
        {compName: "Salt", ip: 2},
        {compName: "Mystic Essence", ip: 2},
    ]},
    "Nightorchid Curse": {ts: tsAlc, fl: "N", op: 1, components: [
        {compName: "Crawlingroot Paste", ip: 1},
        {compName: "Nightorchid Toxin", ip: 2},
    ]},
    "Nightorchid Mystery": {ts: tsAlc, fl: "N", op: 1, components: [
        {compName: "Nightorchid Mystery", ip: 3},
    ]},
    "Nightorchid Toxin": {ts: tsAlc, fl: "N", op: 1, components: [
        {compName: "Orchid Oil", ip: 2},
        {compName: "Nightshade Oil", ip: 1},
    ]},
    "Nightshade Oil": {ts: tsAlc, fl: "N", op: 1, components: [
        {compName: "Nightshade", ip: 5},
    ]},
    "Orchid Oil": {ts: tsAlc, fl: "O", op: 1, components: [
        {compName: "Orchid", ip: 5},
    ]},
    "Phantom Dragon ?? (Epic+)": {ts: tsAlc, fl: "P", op: 1, components: [
        {compName: "Extract Moonbeard Oil", ip: 3},
        {compName: "Phantom Dragon Paste", ip: 2},
    ]},
    "Phantom Dragon Paste": {ts: tsAlc, fl: "P", op: 1, components: [
        {compName: "Moonlight Essence", ip: 1},
        {compName: "Extract Phantomlily Oil", ip: 2},
        {compName: "Extract Dragontear Oil", ip: 1},
    ]},
    "Pixie Dragon Mystery (Epic)": {ts: tsAlc, fl: "P", op: 1, components: [
        {compName: "Extract Kingsbloom Oil", ip: 2},
        {compName: "Extract Pixieroot Oil", ip: 2},
        {compName: "Extract Angelflower Oil", ip: 2},
        {compName: "Extract Dragontear Oil", ip: 2},
    ]},
    "Pixiebloom Mystery (Superior+)": {ts: tsAlc, fl: "P", op: 1, components: [
        {compName: "Concoct Pixiebloom Toxin", ip: 2},
    ]},
    "Random Battle (Superior)": {ts: tsAlc, fl: "R", op: 1, components: [
        {compName: "Simple Dust", ip: 2},
        {compName: "Extract Goldenbush Oil", ip: 2},
        {compName: "Extract Midnight Oil", ip: 2},
        {compName: "Extract Rosemary Oil", ip: 2},
    ]},
    "Rare Battle Damage": {ts: tsAlc, fl: "R", op: 1, components: [
        {compName: "Concoct Goldenrose Toxin", ip: 3},
    ]},
    "Rosemary Buff (Superior)": {ts: tsAlc, fl: "R", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Extract Rosemary Oil", ip: 5},
    ]},
    "Sandcreep Curse (Superior+)": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Crushed Sandcreeper Paste", ip: 4},
    ]},
    "Serpent Vine Acid": {ts: tsAlc, fl: "S", op: 4, components: [
        {compName: "Nightshade", ip: 1},
        {compName: "Serpent Vine", ip: 1},
        {compName: "Sunlightbloom", ip: 1},
    ]},
    "Serpentroot Paste": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Crawlingroot Paste", ip: 3},
    ]},
    "Serpentroot Surprise": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Orchid Oil", ip: 1},
        {compName: "Serpentroot Paste", ip: 3},
    ]},
    "Silkweed Paste": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Silkweed", ip: 8},
    ]},
    "Silkweed Random": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Silkweed Paste", ip: 4},
    ]},
    "Spiderfoot Paste": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Spiderfoot", ip: 6},
    ]},
    "Spiderfoot Surprise": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Spiderfoot Paste", ip: 4},
    ]},
    "Spirit Dragon Mystery (Epic+)": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Extract Gorethistle Oil", ip: 1},
        {compName: "Extract Waterberry Oil", ip: 1},
        {compName: "Spirit Dragon Paste", ip: 3},
    ]},
    "Spirit Dragon Paste": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Crushed Spiritherb Paste", ip: 4},
        {compName: "Shadow Essence", ip: 1},
        {compName: "Extract Dragontear Oil", ip: 1},
    ]},
    "Spiritherb Heal (Superior+)": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Crushed Spiritherb Paste", ip: 3},
    ]},
    "Stonesilk Lily Oil": {ts: tsAlc, fl: "S", op: 1, components: [
        {compName: "Stonesilk Lily", ip: 5},
    ]},
    "Tiger Lily Oil": {ts: tsAlc, fl: "T", op: 1, components: [
        {compName: "Tiger Lily", ip: 5},
    ]},
    "Tigerfrost Curse": {ts: tsAlc, fl: "T", op: 1, components: [
        {compName: "Silkweed Paste", ip: 1},
        {compName: "Tigerfrost Toxin", ip: 2},
    ]},
    "Tigerfrost Mystery": {ts: tsAlc, fl: "T", op: 1, components: [
        {compName: "Tigerfrost Toxin", ip: 3},
    ]},
    "Tigerfrost Toxin": {ts: tsAlc, fl: "T", op: 1, components: [
        {compName: "Tiger Lily Oil", ip: 2},
        {compName: "Frostweed Oil", ip: 1},
    ]},
    "Toe of Frog": {ts: tsAlc, fl: "T", op: 1, components: []},
    "Venomblood Paste": {ts: tsAlc, fl: "V", op: 1, components: [
        {compName: "Blood Berry Paste", ip: 3},
    ]},
    "Venomblood Surprise": {ts: tsAlc, fl: "V", op: 1, components: [
        {compName: "Stonesilk Lily Oil", ip: 1},
        {compName: "Venomblood Paste", ip: 3},
    ]},
    "Venomweed Acid": {ts: tsAlc, fl: "V", op: 1, components: [
        {compName: "Icebloom", ip: 1},
        {compName: "Venomweed", ip: 1},
        {compName: "Golembloom", ip: 1},
    ]},
    "Vilecorpse Curse (Rare+)": {ts: tsAlc, fl: "V", op: 1, components: [
        {compName: "Spiderfoot Paste", ip: 1},
        {compName: "Vilecorpse Toxin", ip: 2},
    ]},
    "Vilecorpse Mystery": {ts: tsAlc, fl: "V", op: 1, components: [
        {compName: "Vilecorpse Toxin", ip: 3},
    ]},
    "Vilecorpse Toxin": {ts: tsAlc, fl: "V", op: 1, components: [
        {compName: "Vilegourd Oil", ip: 2},
        {compName: "Corpsepoppy Oil", ip: 1},
    ]},
    "Vilegourd Oil": {ts: tsAlc, fl: "V", op: 1, components: [
        {compName: "Vilegourd", ip: 5},
    ]},
    "Water Dragon Heal (Epic+)": {ts: tsAlc, fl: "W", op: 1, components: [
        {compName: "Extract Waterberry Oil", ip: 2},
        {compName: "Spirit Dragon Paste", ip: 2},
    ]},
    "Wild Garlic Oil": {ts: tsAlc, fl: "W", op: 1, components: [
        {compName: "Wild Garlic", ip: 5},
    ]},
    "Witch's Brew": {ts: tsAlc, fl: "W", op: 1, components: [
        {compName: "Clay Mug", ip: 1},
        {compName: "Eye of Newt", ip: 1},
        {compName: "Water", ip: 6},
        {compName: "Fire of Belly", ip: 1},
        {compName: "Honey", ip: 2},
        {compName: "Toe of Frog", ip: 1},
    ]},
    "Wolf Dragon ??? (Epic+)": {ts: tsAlc, fl: "W", op: 1, components: [
        {compName: "Extract Ladyslipper Oil", ip: 1},
        {compName: "Wolf Dragon Paste", ip: 3},
    ]},
    "Wolf Dragon Fireball (Epic+)": {ts: tsAlc, fl: "W", op: 1, components: [
        {compName: "Extract Phantomlily Oil", ip: 1},
        {compName: "Wolf Dragon Paste", ip: 2},
    ]},
    "Wolf Dragon Paste": {ts: tsAlc, fl: "W", op: 1, components: [
        {compName: "Crushed Wolfsbane Paste", ip: 4},
        {compName: "Extract Dragontear Oil", ip: 1},
    ]},
    "Wolfsbane ??? (Superior+)": {ts: tsAlc, fl: "W", op: 1, components: [
        {compName: "Crushed Wolfsbane Paste", ip: 4},
    ]},
    // ancient research
    "Aged Etching": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Aged Reference": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Aged Writings": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Ancient Book": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Ancient Etching": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Ancient Reference": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Ancient Witik Etching": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Arcane Etching": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Archaic Rune": {ts: tsAnc, fl: "A", op: 1, components: []},
    "Basic Parchment": {ts: tsAnc, fl: "B", op: 1, components: []},
    "Basic Reference": {ts: tsAnc, fl: "B", op: 1, components: []},
    "Basic Rune": {ts: tsAnc, fl: "B", op: 1, components: []},
    "Basic Writings": {ts: tsAnc, fl: "B", op: 1, components: []},
    "Bone Writings": {ts: tsAnc, fl: "B", op: 1, components: []},
    "Book of Building": {ts: tsAnc, fl: "B", op: 1, components: []},
    "Book of Holiday Songs": {ts: tsAnc, fl: "B", op: 1, components: []},
    "Celestial Reference": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Celestial Rune": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Celestial Writings": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Chocolate Heart": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Clean Up Instructions": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Complex Etching": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Cryptic Reference": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Cryptic Rune": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Cryptic Writings": {ts: tsAnc, fl: "C", op: 1, components: []},
    "Dwarven Etching": {ts: tsAnc, fl: "D", op: 1, components: []},
    "Elaborate Etching": {ts: tsAnc, fl: "E", op: 1, components: []},
    "Elder Reference": {ts: tsAnc, fl: "E", op: 1, components: []},
    "Elder Writings": {ts: tsAnc, fl: "E", op: 1, components: []},
    "Elemental Parchment": {ts: tsAnc, fl: "E", op: 1, components: []},
    "Elemental Rune": {ts: tsAnc, fl: "E", op: 1, components: []},
    "Elven Artifact": {ts: tsAnc, fl: "E", op: 1, components: []},
    "Elven Parchment": {ts: tsAnc, fl: "E", op: 1, components: []},
    "Elven Rune": {ts: tsAnc, fl: "E", op: 1, components: []},
    "Firmament Parchment": {ts: tsAnc, fl: "F", op: 1, components: []},
    "Frozen Rune": {ts: tsAnc, fl: "F", op: 1, components: []},
    "Golden Hay": {ts: tsAnc, fl: "G", op: 1, components: []},
    "Grey Spell of Protection": {ts: tsAnc, fl: "G", op: 1, components: []},
    "Heart Candy - XOXO": {ts: tsAnc, fl: "H", op: 1, components: []},
    "Holy Rune": {ts: tsAnc, fl: "H", op: 1, components: []},
    "Immaculate Parchment": {ts: tsAnc, fl: "I", op: 1, components: []},
    "Intricate Etching": {ts: tsAnc, fl: "I", op: 1, components: []},
    "Jail Blueprints": {ts: tsAnc, fl: "J", op: 1, components: []},
    "Lylin Carving": {ts: tsAnc, fl: "L", op: 1, components: []},
    "Minor Arcana: Wand": {ts: tsAnc, fl: "M", op: 1, components: []},
    "Minor Etching": {ts: tsAnc, fl: "M", op: 1, components: []},
    "Mysterious Artifact": {ts: tsAnc, fl: "M", op: 1, components: []},
    "Mysterious Powder": {ts: tsAnc, fl: "M", op: 1, components: []},
    "Nomadic Etching": {ts: tsAnc, fl: "N", op: 1, components: []},
    "Old Rites": {ts: tsAnc, fl: "O", op: 1, components: []},
    "Orange Syrup": {ts: tsAnc, fl: "O", op: 1, components: []},
    "Platinum Egg": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Prayer Reference": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Precious Book": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Precious Reference": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Preserved Parchment": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Primeval Parchment": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Primeval Reference": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Primeval Rune": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Primeval Writings": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Primitive Parchment": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Primitive Reference": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Primitive Rune": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Primitive Writings": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Pristine Reference": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Pristine Rune": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Pristine Writings": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Purple Easter Egg": {ts: tsAnc, fl: "P", op: 1, components: []},
    "Red Striped Easter Egg": {ts: tsAnc, fl: "R", op: 1, components: []},
    "Reindeer Map Parchment": {ts: tsAnc, fl: "R", op: 1, components: []},
    "Rightous Etching": {ts: tsAnc, fl: "R", op: 1, components: []},
    "Romantic Music Sheet": {ts: tsAnc, fl: "R", op: 1, components: []},
    "Sacred Reference": {ts: tsAnc, fl: "S", op: 1, components: []},
    "Sentinel Parchment": {ts: tsAnc, fl: "S", op: 1, components: []},
    "Superb Etching": {ts: tsAnc, fl: "S", op: 1, components: []},
    "Temple Translations": {ts: tsAnc, fl: "T", op: 1, components: []},
    "Treasured Etching": {ts: tsAnc, fl: "T", op: 1, components: []},
    "Treasured Rune": {ts: tsAnc, fl: "T", op: 1, components: []},
    "Unknown Artifact": {ts: tsAnc, fl: "U", op: 1, components: []},
    "Witik Book": {ts: tsAnc, fl: "W", op: 1, components: []},
    "Witik Etching": {ts: tsAnc, fl: "W", op: 1, components: []},
    "Wyvern Book": {ts: tsAnc, fl: "W", op: 1, components: []},
    "Wyvern Etching": {ts: tsAnc, fl: "W", op: 1, components: []},
    "Yathe's Book": {ts: tsAnc, fl: "Y", op: 1, components: []},
    // combat
    "Escaped Convict": {ts: tsCom, fl: "E", op: 1, components: []},
    "Rescued Chick": {ts: tsCom, fl: "R", op: 1, components: []},
    // construction
    "Adamantite Crafting Knife": {ts: tsCon, fl: "A", op: 1, components: [
        {compName: "Adamantite Crafting Blade", ip: 1},
        {compName: "Adamantite Fitting", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Alchemy Lab": {ts: tsCon, fl: "A", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 30},
        {compName: "Form Iron Nails", ip: 90},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Pine Building Wall", ip: 1},
        {compName: "Medium Pine Wall", ip: 2},
        {compName: "Ancientbloom", ip: 20},
    ]},
    "Ballista": {ts: tsCon, fl: "B", op: 1, components: [
        {compName: "Ballista Wood", ip: 5},
    ]},
    "Base Guild Hall (Pine)": {ts: tsCon, fl: "B", op: 1, components: [
        {compName: "Form Iron Nails", ip: 100},
        {compName: "Pine Guild Roof", ip: 1},
        {compName: "Pine Guild Wall", ip: 4},
        {compName: "Black Ash", ip: 4},
    ]},
    "Base Guild Wing (Pine)": {ts: tsCon, fl: "B", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 14},
        {compName: "Form Iron Nails", ip: 80},
        {compName: "Pine Guild Roof", ip: 1},
        {compName: "Pine Guild Wall", ip: 3},
    ]},
    "Birch Container Wall": {ts: tsCon, fl: "B", op: 1, components: [
        {compName: "Mill Light Birch", ip: 60},
        {compName: "Form Copper Nails", ip: 200},
    ]},
    "Birch Guild Container": {ts: tsCon, fl: "B", op: 1, components: [
        {compName: "Form Tin Nails", ip: 30},
        {compName: "Birch Container Wall", ip: 4},
        {compName: "Mill Black Spruce", ip: 16},
        {compName: "Medium Birch Wall", ip: 2},
    ]},
    "Birch Mallet": {ts: tsCon, fl: "B", op: 1, components: [
        {compName: "Birch Mallet Top", ip: 1},
        {compName: "Copper Fitting", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
    ]},
    "Blacksmith": {ts: tsCon, fl: "B", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 100},
        {compName: "Form Iron Nails", ip: 100},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Reinforced Pine Wall", ip: 3},
        {compName: "Golemite Ore", ip: 10},
    ]},
    "Bloodstone Crafting Knife": {ts: tsCon, fl: "B", op: 1, components: [
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Bloodstone Crafting Blade", ip: 1},
        {compName: "Bloodstone Fitting", ip: 1},
    ]},
    "Castle Building Kit": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Mill Oak", ip: 50000},
        {compName: "Pyrestone", ip: 40000},
        {compName: "Mithril Nails", ip: 60000},
        {compName: "Mill Sourknot", ip: 10000},
        {compName: "Blackened Clay", ip: 4000},
        {compName: "Rosewood", ip: 4000},
        {compName: "Black Diamond", ip: 400},
        {compName: "Mahogany", ip: 1500},
    ]},
    "Cedar Building Roof": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Mill Red Cedar", ip: 100},
        {compName: "Cedar Shingles", ip: 280},
        {compName: "Form Iron Nails", ip: 200},
    ]},
    "Cedar Building Wall": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Mill Red Cedar", ip: 140},
        {compName: "Form Tin Nails", ip: 240},
    ]},
    "Cedar Container Wall": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Mill Red Cedar", ip: 100},
        {compName: "Form Tin Nails", ip: 200},
    ]},
    "Cedar Guild Container": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Cedar Container Wall", ip: 2},
        {compName: "Form Iron Nails", ip: 45},
        {compName: "Mill Black Spruce", ip: 24},
        {compName: "Medium Cedar Wall", ip: 4},
    ]},
    "Cedar Mallet": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Cedar Mallet Top", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Tin Fitting", ip: 1},
    ]},
    "Cedar Shingles": {ts: tsCon, fl: "C", op: 5, components: [
        {compName: "Red Cedar", ip: 1},
    ]},
    "Chromium Crafting Knife": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Chromium Crafting Blade", ip: 1},
        {compName: "Chromium Fitting", ip: 1},
    ]},
    "Cobalt Crafting Knife": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Cobalt Crafting Knife", ip: 1},
        {compName: "Cobalt Fitting", ip: 1},
    ]},
    "Cooking Hall": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 30},
        {compName: "Form Iron Nails", ip: 60},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Pine Building Wall", ip: 1},
        {compName: "Medium Pine Wall", ip: 2},
        {compName: "Cast Iron Pot", ip: 4},
    ]},
    "Coop Lumber": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Coop Wood", ip: 8},
    ]},
    "Copper Crafting Knife": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Copper Crafting Blade", ip: 1},
        {compName: "Copper Fitting", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
    ]},
    "Crafting Hut": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 200},
        {compName: "Form Chromium Nails", ip: 100},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Medium Fire Clay", ip: 3},
        {compName: "Reinforced Pine Wall", ip: 3},
    ]},
    "Crystal Jewelry Box": {ts: tsCon, fl: "C", op: 1, components: [
        {compName: "White Crystal", ip: 8},
    ]},
    "Dark Cherry Handle": {ts: tsCon, fl: "D", op: 1, components: [
        {compName: "Dark Cherry", ip: 2},
    ]},
    "Elven Walnut Handle": {ts: tsCon, fl: "E", op: 1, components: [
        {compName: "Elven Walnut", ip: 2},
    ]},
    "Enchanting Study": {ts: tsCon, fl: "E", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 35},
        {compName: "Form Iron Nails", ip: 90},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Pine Building Wall", ip: 1},
        {compName: "Medium Pine Wall", ip: 2},
        {compName: "Ancient Essence", ip: 16},
    ]},
    "Estate Building Kit": {ts: tsCon, fl: "E", op: 1, components: [
        {compName: "Limestone", ip: 2500},
        {compName: "Mill White Pine", ip: 3000},
        {compName: "Pine Shingles", ip: 5000},
        {compName: "Mill Bigtooth Aspen", ip: 4000},
        {compName: "Form Chromium Nails", ip: 6000},
        {compName: "Form Iron Nails", ip: 4000},
        {compName: "Silver Maple", ip: 400},
        {compName: "Wild Cherry", ip: 200},
        {compName: "Black Ash", ip: 100},
    ]},
    "Fern Container": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Mill Fern", ip: 16},
        {compName: "Fern Wall", ip: 6},
        {compName: "Manganese Nails", ip: 50},
    ]},
    "Fern Mallet": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Fern Mallet Top", ip: 1},
        {compName: "Manganese Fitting", ip: 1},
        {compName: "Maidenhair Handle", ip: 1},
    ]},
    "Fern Roof": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Mill Fern", ip: 110},
        {compName: "Fern Shingles", ip: 380},
        {compName: "Mill Blue Spruce", ip: 40},
        {compName: "Manganese Nails", ip: 250},
    ]},
    "Fern Shingles": {ts: tsCon, fl: "F", op: 5, display: "Fern Shingle", components: [
        {compName: "Fern", ip: 1},
    ]},
    "Fern Wall": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Mill Fern", ip: 110},
        {compName: "Mill Blue Spruce", ip: 40},
        {compName: "Manganese Nails", ip: 200},
    ]},
    "Fine Aspen Handle": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Bigtooth Aspen", ip: 4},
        {compName: "Garnet", ip: 1},
    ]},
    "Fine Spruce Handle": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Agate", ip: 1},
        {compName: "Black Spruce", ip: 4},
    ]},
    "Fort Building Kit": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Mill Hemlock", ip: 10000},
        {compName: "Hemlock Shingles", ip: 7000},
        {compName: "Lava Rock", ip: 4000},
        {compName: "Limestone", ip: 3000},
        {compName: "Marble", ip: 6000},
        {compName: "Mill Bigtooth Aspen", ip: 2000},
        {compName: "Iridium Nails", ip: 15000},
    ]},
    "Fortified Castle Kit": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Mill Fern", ip: 60000},
        {compName: "Obsidian", ip: 50000},
        {compName: "Mill Blue Spruce", ip: 15000},
        {compName: "Manganese Nails", ip: 70000},
        {compName: "White Willow", ip: 5000},
        {compName: "Wolf Ash Clay", ip: 4000},
        {compName: "Dragonstone", ip: 500},
        {compName: "Maidenhair", ip: 2000},
    ]},
    "Fortress Building Kit": {ts: tsCon, fl: "F", op: 1, components: [
        {compName: "Crystal", ip: 30000},
        {compName: "Mill Mora", ip: 30000},
        {compName: "Mill Birdseye", ip: 5000},
        {compName: "Titanium Nails", ip: 45000},
        {compName: "Molten Clay", ip: 2000},
        {compName: "Satinwood", ip: 2000},
        {compName: "Dark Cherry", ip: 1000},
    ]},
    "Guild Hall Bank": {ts: tsCon, fl: "G", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 14},
        {compName: "Form Iron Nails", ip: 100},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Medium Cedar Container", ip: 4},
        {compName: "Reinforced Pine Wall", ip: 3},
    ]},
    "Hall Building Kit": {ts: tsCon, fl: "H", op: 1, components: [
        {compName: "Mill Red Cedar", ip: 2000},
        {compName: "Mill White Pine", ip: 5000},
        {compName: "Pine Shingles", ip: 1500},
        {compName: "Shale Rock", ip: 500},
        {compName: "Form Tin Nails", ip: 1000},
        {compName: "Form Iron Nails", ip: 8000},
        {compName: "Chestnut", ip: 50},
        {compName: "Silver Maple", ip: 250},
        {compName: "Black Ash", ip: 50},
    ]},
    "Hemlock Container": {ts: tsCon, fl: "H", op: 1, components: [
        {compName: "Mill Hemlock", ip: 12},
        {compName: "Hemlock Wall", ip: 6},
        {compName: "Iridium Nails", ip: 60},
    ]},
    "Hemlock Mallet": {ts: tsCon, fl: "H", op: 1, components: [
        {compName: "Hemlock Mallet Top", ip: 1},
        {compName: "Iridium Fitting", ip: 1},
        {compName: "Wild Cherry Handle", ip: 1},
    ]},
    "Hemlock Roof": {ts: tsCon, fl: "H", op: 1, components: [
        {compName: "Mill Hemlock", ip: 100},
        {compName: "Hemlock Shingles", ip: 340},
        {compName: "Iridium Nails", ip: 220},
    ]},
    "Hemlock Shingles": {ts: tsCon, fl: "H", op: 5, components: [
        {compName: "Hemlock", ip: 1},
    ]},
    "Hemlock Wall": {ts: tsCon, fl: "H", op: 1, components: [
        {compName: "Mill Hemlock", ip: 100},
        {compName: "Iridium Nails", ip: 180},
    ]},
    "Hickory Slats": {ts: tsCon, fl: "H", op: 25, components: [
        {compName: "Hickory", ip: 1},
    ]},
    "Holiday Sled": {ts: tsCon, fl: "H", op: 1, components: [
        {compName: "Mill Light Birch", ip: 12},
        {compName: "Form Copper Nails", ip: 60},
        {compName: "Copper Fitting", ip: 2},
        {compName: "Copper Fasteners", ip: 2},
    ]},
    "House Building Kit": {ts: tsCon, fl: "H", op: 1, components: [
        {compName: "Mill Red Cedar", ip: 2500},
        {compName: "Cedar Shingles", ip: 750},
        {compName: "Sandstone", ip: 500},
        {compName: "Form Tin Nails", ip: 3500},
        {compName: "Clay", ip: 60},
        {compName: "Wild Cherry", ip: 75},
        {compName: "Black Ash", ip: 25},
    ]},
    "Inscription Library": {ts: tsCon, fl: "I", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 35},
        {compName: "Form Iron Nails", ip: 90},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Pine Building Wall", ip: 1},
        {compName: "Medium Pine Wall", ip: 2},
        {compName: "Ancient Book", ip: 10},
        {compName: "Archaic Rune", ip: 10},
    ]},
    "Iridium Crafting Knife": {ts: tsCon, fl: "I", op: 1, components: [
        {compName: "Iridium Crafting Blade", ip: 1},
        {compName: "Iridium Fitting", ip: 1},
        {compName: "Wild Cherry Handle", ip: 1},
    ]},
    "Iron Crafting Knife": {ts: tsCon, fl: "I", op: 1, components: [
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Iron Crafting Blade", ip: 1},
        {compName: "Iron Fitting", ip: 1},
    ]},
    "Jewel Room": {ts: tsCon, fl: "J", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 10},
        {compName: "Form Iron Nails", ip: 100},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Pine Building Wall", ip: 1},
        {compName: "Process Huge Citrine", ip: 3},
        {compName: "Process Huge Malachite", ip: 5},
        {compName: "Reinforced Pine Wall", ip: 2},
    ]},
    "Keep Building Kit": {ts: tsCon, fl: "K", op: 1, components: [
        {compName: "Mill Hemlock", ip: 15000},
        {compName: "Hemlock Shingles", ip: 10000},
        {compName: "Lava Rock", ip: 5000},
        {compName: "Quartz", ip: 8000},
        {compName: "Bloodstone Ore", ip: 400},
        {compName: "Iridium Nails", ip: 10000},
        {compName: "Dark Cotton", ip: 100},
        {compName: "Kaolinite", ip: 150},
        {compName: "Sharkwood", ip: 100},
        {compName: "Silver Maple", ip: 1000},
        {compName: "Flame Pearl", ip: 150},
    ]},
    "Light Birch Shingles": {ts: tsCon, fl: "L", op: 5, display: "Birch Shingles", components: [
        {compName: "Light Birch", ip: 1},
    ]},
    "Mahogany Handle": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mahogany", ip: 2},
    ]},
    "Maidenhair Handle": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Maidenhair", ip: 2},
    ]},
    "Manganese Crafting Knife": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Manganese Crafting Blade", ip: 1},
        {compName: "Manganese Fitting", ip: 1},
        {compName: "Maidenhair Handle", ip: 1},
    ]},
    "Medium Birch Container": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mill Light Birch", ip: 16},
        {compName: "Form Tin Nails", ip: 30},
        {compName: "Medium Birch Wall", ip: 6},
    ]},
    "Medium Birch Wall": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mill Light Birch", ip: 90},
        {compName: "Form Tin Nails", ip: 200},
        {compName: "Mill Black Spruce", ip: 24},
    ]},
    "Medium Cedar Container": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mill Red Cedar", ip: 24},
        {compName: "Form Iron Nails", ip: 60},
        {compName: "Medium Cedar Wall", ip: 6},
    ]},
    "Medium Cedar Wall": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mill Red Cedar", ip: 130},
        {compName: "Form Iron Nails", ip: 180},
        {compName: "Mill Black Spruce", ip: 24},
    ]},
    "Medium Pine Wall": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mill White Pine", ip: 50},
        {compName: "Mill Bigtooth Aspen", ip: 35},
        {compName: "Form Chromium Nails", ip: 200},
    ]},
    "Mill Bigtooth Aspen": {ts: tsCon, fl: "M", op: 1, display: "Aspen Lumber", components: [
        {compName: "Bigtooth Aspen", ip: 1},
    ]},
    "Mill Birdseye": {ts: tsCon, fl: "M", op: 1, display: "Birdseye Lumber", components: [
        {compName: "Birdseye Maple", ip: 1},
    ]},
    "Mill Black Spruce": {ts: tsCon, fl: "M", op: 2, display: "Black Spruce Lumber", components: [
        {compName: "Black Spruce", ip: 1},
    ]},
    "Mill Blue Spruce": {ts: tsCon, fl: "M", op: 1, display: "Blue Spruce Lumber", components: [
        {compName: "Blue Spruce", ip: 1},
    ]},
    "Mill Fern": {ts: tsCon, fl: "M", op: 1, display: "Fern Lumber", components: [
        {compName: "Fern", ip: 1},
    ]},
    "Mill Hemlock": {ts: tsCon, fl: "M", op: 1, display: "Hemlock Lumber", components: [
        {compName: "Hemlock", ip: 1},
    ]},
    "Mill Light Birch": {ts: tsCon, fl: "M", op: 2, display: "Birch Lumber", components: [
        {compName: "Light Birch", ip: 1},
    ]},
    "Mill Mora": {ts: tsCon, fl: "M", op: 1, display: "Mora Lumber", components: [
        {compName: "Mora", ip: 1},
    ]},
    "Mill Oak": {ts: tsCon, fl: "M", op: 1, display: "Oak Lumber", components: [
        {compName: "Oak", ip: 1},
    ]},
    "Mill Red Cedar": {ts: tsCon, fl: "M", op: 2, display: "Cedar Lumber", components: [
        {compName: "Red Cedar", ip: 1},
    ]},
    "Mill Silver Birch": {ts: tsCon, fl: "M", op: 1, display:"Silver Birch Lumber", components: [
        {compName: "Silver Birch", ip: 1},
    ]},
    "Mill Sourknot": {ts: tsCon, fl: "M", op: 1, display: "Sourknot Lumber", components: [
        {compName: "Sourknot", ip: 1},
    ]},
    "Mill Sugar Maple": {ts: tsCon, fl: "M", op: 1, display:"Sugar Maple Lumber", components: [
        {compName: "Sugar Maple", ip: 1},
    ]},
    "Mill Walnut": {ts: tsCon, fl: "M", op: 1, display: "Walnut Lumber", components: [
        {compName: "Walnut", ip: 1},
    ]},
    "Mill White Pine": {ts: tsCon, fl: "M", op: 1, display: "Pine Lumber", components: [
        {compName: "White Pine", ip: 1},
    ]},
    "Mithril Crafting Knife": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mithril Crafting Blade", ip: 1},
        {compName: "Mithril Fitting", ip: 1},
        {compName: "Mahogany Handle", ip: 1},
    ]},
    "Mora Container": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mill Mora", ip: 16},
        {compName: "Mora Wall", ip: 6},
        {compName: "Titanium Nails", ip: 50},
    ]},
    "Mora Mallet": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mora Mallet Top", ip: 1},
        {compName: "Titanium Fitting", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Mora Roof": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mill Mora", ip: 110},
        {compName: "Mora Shingles", ip: 380},
        {compName: "Mill Birdseye", ip: 40},
        {compName: "Titanium Nails", ip: 250},
    ]},
    "Mora Shingles": {ts: tsCon, fl: "M", op: 5, display: "Mora Shingle", components: [
        {compName: "Mora", ip: 1},
    ]},
    "Mora Wall": {ts: tsCon, fl: "M", op: 1, components: [
        {compName: "Mill Mora", ip: 110},
        {compName: "Mill Birdseye", ip: 40},
        {compName: "Titanium Nails", ip: 200},
    ]},
    "Oak Container": {ts: tsCon, fl: "O", op: 1, components: [
        {compName: "Mill Oak", ip: 16},
        {compName: "Mithril Nails", ip: 50},
        {compName: "Oak Wall", ip: 6},
    ]},
    "Oak Mallet": {ts: tsCon, fl: "O", op: 1, components: [
        {compName: "Oak Mallet Top", ip: 1},
        {compName: "Mithril Fitting", ip: 1},
        {compName: "Mahogany Handle", ip: 1},
    ]},
    "Oak Roof": {ts: tsCon, fl: "O", op: 1, components: [
        {compName: "Mill Oak", ip: 110},
        {compName: "Oak Shingles", ip: 380},
        {compName: "Mithril Nails", ip: 250},
        {compName: "Mill Sourknot", ip: 40},
    ]},
    "Oak Shingles": {ts: tsCon, fl: "O", op: 5, display: "Oak Shingle", components: [
        {compName: "Oak", ip: 1},
    ]},
    "Oak Wall": {ts: tsCon, fl: "O", op: 1, components: [
        {compName: "Mill Oak", ip: 110},
        {compName: "Mithril Nails", ip: 200},
        {compName: "Mill Sourknot", ip: 40},
    ]},
    "Palace Building Kit": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Basalt", ip: 25000},
        {compName: "Mill Walnut", ip: 20000},
        {compName: "Walnut Shingles", ip: 25000},
        {compName: "Adamantite Nails", ip: 30000},
        {compName: "Dark Cotton", ip: 250},
        {compName: "Earthen Clay", ip: 1000},
        {compName: "Sharkwood", ip: 5000},
        {compName: "Dark Cherry", ip: 1000},
    ]},
    "Pine Building Roof": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Mill White Pine", ip: 60},
        {compName: "Pine Shingles", ip: 300},
        {compName: "Form Chromium Nails", ip: 200},
    ]},
    "Pine Building Wall": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Mill White Pine", ip: 75},
        {compName: "Form Chromium Nails", ip: 160},
    ]},
    "Pine Container Wall": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Mill White Pine", ip: 65},
        {compName: "Form Chromium Nails", ip: 160},
    ]},
    "Pine Guild Container": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 16},
        {compName: "Form Chromium Nails", ip: 40},
        {compName: "Pine Container Wall", ip: 4},
        {compName: "Medium Pine Wall", ip: 2},
    ]},
    "Pine Guild Roof": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Limestone", ip: 220},
        {compName: "Pine Shingles", ip: 1250},
        {compName: "Form Iron Nails", ip: 1500},
        {compName: "Black Ash", ip: 10},
    ]},
    "Pine Guild Wall": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Limestone", ip: 120},
        {compName: "Mill White Pine", ip: 250},
        {compName: "Form Iron Nails", ip: 900},
    ]},
    "Pine Mallet": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Pine Mallet Top", ip: 1},
        {compName: "Chromebalt Fitting", ip: 1},
    ]},
    "Pine Pet Container": {ts: tsCon, fl: "P", op: 1, components: [
        {compName: "Mill White Pine", ip: 80},
        {compName: "Form Iron Nails", ip: 325},
        {compName: "Small Iron Gate", ip: 1},
    ]},
    "Pine Shingles": {ts: tsCon, fl: "P", op: 5, components: [
        {compName: "White Pine", ip: 1},
    ]},
    "Reinforced Pine Wall": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Limestone", ip: 160},
        {compName: "Mill White Pine", ip: 160},
        {compName: "Form Iron Nails", ip: 300},
        {compName: "Black Ash", ip: 1},
    ]},
    "Rough Aspen Rod": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Bigtooth Aspen", ip: 4},
    ]},
    "Rough Birch Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Light Birch", ip: 4},
    ]},
    "Rough Birch Rod": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Light Birch", ip: 6},
    ]},
    "Rough Cedar Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Red Cedar", ip: 5},
    ]},
    "Rough Cedar Rod": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Red Cedar", ip: 6},
    ]},
    "Rough Fern Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Fern", ip: 5},
    ]},
    "Rough Hemlock Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Hemlock", ip: 5},
    ]},
    "Rough Mora Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Mora", ip: 5},
    ]},
    "Rough Oak Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Oak", ip: 5},
    ]},
    "Rough Pine Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "White Pine", ip: 5},
    ]},
    "Rough Rosewood Rod": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Rosewood", ip: 4},
    ]},
    "Rough Satinwood Rod": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Satinwood", ip: 4},
    ]},
    "Rough Sharkwood Rod": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Sharkwood", ip: 4},
    ]},
    "Rough Silver Birch Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Silver Birch", ip: 5},
    ]},
    "Rough Tilia Cordata Rod": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Tilia Cordata", ip: 4},
    ]},
    "Rough Walnut Block": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "Walnut", ip: 5},
    ]},
    "Rough White Willow Rod": {ts: tsCon, fl: "R", op: 1, components: [
        {compName: "White Willow", ip: 4},
    ]},
    "Shack Building Kit": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Light Birch", ip: 1000},
        {compName: "Light Birch Shingles", ip: 500},
        {compName: "Form Copper Nails", ip: 1500},
        {compName: "Stone", ip: 120},
        {compName: "Mill Black Spruce", ip: 400},
    ]},
    "Shovel Handle": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Long Branch", ip: 8},
    ]},
    "Silver Birch Container": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Silver Birch", ip: 16},
        {compName: "Silver Birch Wall", ip: 6},
        {compName: "Tourmaline Nails", ip: 50},
    ]},
    "Silver Birch Mallet": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Silver Birch Mallet Top", ip: 1},
        {compName: "Tourmaline Fitting", ip: 1},
        {compName: "Elven Walnut Handle", ip: 1},
    ]},
    "Silver Birch Roof": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Silver Birch", ip: 110},
        {compName: "Silver Birch Shingles", ip: 380},
        {compName: "Mill Sugar Maple", ip: 40},
        {compName: "Tourmaline Nails", ip: 250},
    ]},
    "Silver Birch Shingles": {ts: tsCon, fl: "S", op: 5, display:"Silver Birch Shingle", components: [
        {compName: "Silver Birch", ip: 1},
    ]},
    "Silver Birch Wall": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Silver Birch", ip: 110},
        {compName: "Mill Sugar Maple", ip: 40},
        {compName: "Tourmaline Nails", ip: 200},
    ]},
    "Silver Maple Handle": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Silver Maple", ip: 5},
    ]},
    "Small Birch Container": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Light Birch", ip: 16},
        {compName: "Form Copper Nails", ip: 30},
        {compName: "Birch Container Wall", ip: 6},
    ]},
    "Small Building Roof": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Light Birch", ip: 80},
        {compName: "Light Birch Shingles", ip: 250},
        {compName: "Form Copper Nails", ip: 200},
    ]},
    "Small Building Wall": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Light Birch", ip: 120},
        {compName: "Form Copper Nails", ip: 240},
    ]},
    "Small Cedar Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Cedar Building Roof", ip: 1},
        {compName: "Cedar Building Wall", ip: 4},
        {compName: "Form Iron Nails", ip: 45},
        {compName: "Mill Black Spruce", ip: 32},
    ]},
    "Small Cedar Container": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Form Tin Nails", ip: 45},
        {compName: "Cedar Container Wall", ip: 6},
        {compName: "Mill Black Spruce", ip: 16},
    ]},
    "Small Fern Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Fern", ip: 30},
        {compName: "Mill Blue Spruce", ip: 60},
        {compName: "Fern Roof", ip: 1},
        {compName: "Fern Wall", ip: 4},
        {compName: "Manganese Nails", ip: 90},
    ]},
    "Small Hemlock Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Hemlock", ip: 30},
        {compName: "Hemlock Roof", ip: 1},
        {compName: "Hemlock Wall", ip: 4},
        {compName: "Iridium Nails", ip: 90},
    ]},
    "Small Market Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Form Copper Nails", ip: 50},
        {compName: "Small Building Roof", ip: 1},
        {compName: "Small Building Wall", ip: 4},
        {compName: "Mill Black Spruce", ip: 24},
    ]},
    "Small Mora Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Mora", ip: 30},
        {compName: "Mill Birdseye", ip: 60},
        {compName: "Mora Roof", ip: 1},
        {compName: "Mora Wall", ip: 4},
        {compName: "Titanium Nails", ip: 90},
    ]},
    "Small Oak Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Oak", ip: 30},
        {compName: "Mithril Nails", ip: 90},
        {compName: "Oak Roof", ip: 1},
        {compName: "Oak Wall", ip: 4},
        {compName: "Mill Sourknot", ip: 60},
    ]},
    "Small Pine Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 25},
        {compName: "Form Chromium Nails", ip: 45},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Pine Building Wall", ip: 4},
    ]},
    "Small Pine Container": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 20},
        {compName: "Form Chromium Nails", ip: 40},
        {compName: "Pine Container Wall", ip: 6},
    ]},
    "Small Silver Birch Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Silver Birch", ip: 30},
        {compName: "Silver Birch Roof", ip: 1},
        {compName: "Silver Birch Wall", ip: 4},
        {compName: "Mill Sugar Maple", ip: 60},
        {compName: "Tourmaline Nails", ip: 90},
    ]},
    "Small Walnut Building": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Mill Walnut", ip: 30},
        {compName: "Adamantite Nails", ip: 90},
        {compName: "Walnut Roof", ip: 1},
        {compName: "Walnut Wall", ip: 4},
    ]},
    "Supreme Castle Kit": {ts: tsCon, fl: "S", op: 1, components: [
        {compName: "Dark Silver", ip: 60000},
        {compName: "Mill Silver Birch", ip: 70000},
        {compName: "Mill Sugar Maple", ip: 18000},
        {compName: "Tourmaline Nails", ip: 80000},
        {compName: "Golden Clay", ip: 5000},
        {compName: "Tilia Cordata", ip: 7000},
        {compName: "Elven Walnut", ip: 3000},
        {compName: "Emberstone", ip: 750},
    ]},
    "Tin Crafting Knife": {ts: tsCon, fl: "T", op: 1, components: [
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Tin Crafting Blade", ip: 1},
        {compName: "Tin Fitting", ip: 1},
    ]},
    "Titanium Crafting Knife": {ts: tsCon, fl: "T", op: 1, components: [
        {compName: "Titanium Crafting Blade", ip: 1},
        {compName: "Titanium Fitting", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Tourmaline Crafting Knife": {ts: tsCon, fl: "T", op: 1, components: [
        {compName: "Tourmaline Crafting Blade", ip: 1},
        {compName: "Tourmaline Fitting", ip: 1},
        {compName: "Elven Walnut Handle", ip: 1},
    ]},
    "Vendor Room": {ts: tsCon, fl: "V", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 25},
        {compName: "Form Iron Nails", ip: 100},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Medium Pine Wall", ip: 3},
    ]},
    "Walnut Container": {ts: tsCon, fl: "W", op: 1, components: [
        {compName: "Mill Walnut", ip: 16},
        {compName: "Adamantite Nails", ip: 50},
        {compName: "Walnut Wall", ip: 6},
    ]},
    "Walnut Mallet": {ts: tsCon, fl: "W", op: 1, components: [
        {compName: "Walnut Mallet Top", ip: 1},
        {compName: "Adamantite Fitting", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Walnut Roof": {ts: tsCon, fl: "W", op: 1, components: [
        {compName: "Mill Walnut", ip: 110},
        {compName: "Walnut Shingles", ip: 380},
        {compName: "Adamantite Nails", ip: 250},
    ]},
    "Walnut Shingles": {ts: tsCon, fl: "W", op: 5, components: [
        {compName: "Walnut", ip: 1},
    ]},
    "Walnut Wall": {ts: tsCon, fl: "W", op: 1, components: [
        {compName: "Mill Walnut", ip: 110},
        {compName: "Adamantite Nails", ip: 200},
    ]},
    "Wild Cherry Handle": {ts: tsCon, fl: "W", op: 1, components: [
        {compName: "Wild Cherry", ip: 2},
    ]},
    "Workshop": {ts: tsCon, fl: "W", op: 1, components: [
        {compName: "Mill Bigtooth Aspen", ip: 10},
        {compName: "Form Iron Nails", ip: 100},
        {compName: "Pine Building Roof", ip: 1},
        {compName: "Reinforced Pine Wall", ip: 3},
        {compName: "Black Ash", ip: 10},
    ]},
    "Wreath": {ts: tsCon, fl: "W", op: 1, components: [
        {compName: "Pine Branches", ip: 8},
    ]},
    // cooking
    "Angelfish Stew (CR)": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Cooked Herring", ip: 2},
        {compName: "Cooked Icefish", ip: 2},
        {compName: "Spiced Angelfish", ip: 2},
    ]},
    "Angelherring Soup (DR)": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Cooked Herring", ip: 3},
        {compName: "Angelfish", ip: 4},
        {compName: "Apricot Spice", ip: 2},
    ]},
    "Apple Cider": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Apple", ip: 3},
    ]},
    "Apple Goby Steak (Random)": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Apple Cider", ip: 2},
        {compName: "Grind Kingsbloom Spice", ip: 2},
        {compName: "Cooked Fire Goby", ip: 3},
    ]},
    "Apple Halibut Jerky (Random)": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Smoked Halibut", ip: 3},
        {compName: "Apple Cider", ip: 1},
        {compName: "Dried Apple Spices", ip: 1},
        {compName: "Egg", ip: 1},
        {compName: "Grind Kingsbloom Spice", ip: 2},
    ]},
    "Apple Shrimp (Gold Drop%)": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Grilled Shrimp", ip: 4},
        {compName: "Apple Cider", ip: 1},
    ]},
    "Apricot Eggs (Travel)": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Apricot Spice", ip: 3},
        {compName: "Giant Egg", ip: 3},
    ]},
    "Apricot Spice": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Apricot", ip: 2},
    ]},
    "Arapaima Surprise (random)": {ts: tsCoo, fl: "A", op: 1, components: [
        {compName: "Spiced Cuttlefish", ip: 1},
        {compName: "Cooked Arapaima", ip: 3},
    ]},
    "Banana Eggs (Invis)": {ts: tsCoo, fl: "B", op: 1, components: [
        {compName: "Dried Banana Spice", ip: 1},
        {compName: "Giant Egg", ip: 4},
    ]},
    "Banana Mackerel (Travel)": {ts: tsCoo, fl: "B", op: 1, components: [
        {compName: "Fried Mackerel", ip: 2},
        {compName: "Dried Banana Spice", ip: 2},
    ]},
    "Basic Food Buff": {ts: tsCoo, fl: "B", op: 1, components: [
        {compName: "Cooked Trout", ip: 2},
        {compName: "Salted Eel", ip: 2},
    ]},
    "Birthday Cake": {ts: tsCoo, fl: "B", op: 1, components: [
        {compName: "Apricot", ip: 5},
        {compName: "Banana", ip: 10},
        {compName: "Cacao Bean", ip: 15},
        {compName: "Egg", ip: 25},
        {compName: "Orange", ip: 15},
        {compName: "Dark Honey", ip: 5},
    ]},
    "Blackened Soup (DR) {051}": {ts: tsCoo, fl: "B", op: 1, components: [
        {compName: "Cooked Flathead", ip: 3},
        {compName: "Blackfish", ip: 4},
        {compName: "Peach Spice", ip: 2},
    ]},
    "Blackened Soup (DR) {101}": {ts: tsCoo, fl: "B", op: 1, components: [
        {compName: "Cooked Bass", ip: 3},
        {compName: "Cuttlefish", ip: 4},
        {compName: "Pear Spice", ip: 2},
    ]},
    "Blackened Soup (DR) {131)": {ts: tsCoo, fl: "B", op: 1, components: [
        {compName: "Cooked Barracuda", ip: 3},
        {compName: "Nile Tilapia", ip: 4},
        {compName: "Scallions Spice", ip: 2},
    ]},
    "Blackfish Stew (CR)": {ts: tsCoo, fl: "B", op: 1, components: [
        {compName: "Cooked Flathead", ip: 2},
        {compName: "Cooked Knifefish", ip: 2},
        {compName: "Spiced Blackfish", ip: 2},
    ]},
    "Boiled Gilgrash": {ts: tsCoo, fl: "B", op: 2, components: [
        {compName: "Gilgrash", ip: 2},
    ]},
    "Caffeinated Blood": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Water", ip: 6},
        {compName: "Fish Blood", ip: 12},
        {compName: "Tea Leaves", ip: 20},
    ]},
    "Chicken Feed": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Chick Grain", ip: 8},
    ]},
    "Chocolate Covered Strawberries": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Chocolate Heart", ip: 1},
        {compName: "Pink Strawberry", ip: 7},
    ]},
    "Chowder of Battle": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Pile of Fish", ip: 5},
    ]},
    "Cod Delight (Gold Drop)": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Cooked Cod", ip: 4},
        {compName: "Grape Spice", ip: 1},
    ]},
    "Cooked Arapaima": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Arapaima", ip: 3},
        {compName: "Maple Syrup", ip: 2},
    ]},
    "Cooked Barracuda": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Barracuda", ip: 5},
    ]},
    "Cooked Bass": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Bass", ip: 5},
    ]},
    "Cooked Cod": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Cod", ip: 5},
    ]},
    "Cooked Crawfish": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Crawfish", ip: 3},
        {compName: "Mango", ip: 2},
    ]},
    "Cooked Fangtooth": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Fangtooth", ip: 3},
        {compName: "Giant Egg", ip: 2},
    ]},
    "Cooked Featherback": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Featherback", ip: 3},
        {compName: "Speckled Egg", ip: 2},
    ]},
    "Cooked Fire Goby": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Salt", ip: 1},
        {compName: "Fire Goby", ip: 2},
    ]},
    "Cooked Flathead": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Flathead", ip: 5},
    ]},
    "Cooked Flounder": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Flounder", ip: 5},
    ]},
    "Cooked Ghostfish": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Dark Honey", ip: 2},
        {compName: "Ghostfish", ip: 3},
    ]},
    "Cooked Haddock": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Haddock", ip: 5},
    ]},
    "Cooked Herring": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Herring", ip: 5},
    ]},
    "Cooked Icefish": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Icefish", ip: 5},
    ]},
    "Cooked Knifefish": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Knifefish", ip: 5},
    ]},
    "Cooked Lancetail": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Dried Orange Spice", ip: 2},
        {compName: "Lancetail", ip: 2},
    ]},
    "Cooked Nettlefish": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Iron Egg", ip: 2},
        {compName: "Nettlefish", ip: 3},
    ]},
    "Cooked Pike": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Pike", ip: 2},
    ]},
    "Cooked Sunray": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Sunray", ip: 2},
    ]},
    "Cooked Tigerfish": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Tigerfish", ip: 5},
    ]},
    "Cooked Trout": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Trout", ip: 2},
    ]},
    "Cooked Trunkfish": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Trunkfish", ip: 5},
    ]},
    "Cooked Whitefish": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Whitefish", ip: 5},
    ]},
    "Crawfish Surprise (random)": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Spiced Nile Tilapia", ip: 1},
        {compName: "Cooked Crawfish", ip: 3},
    ]},
    "Cuttlefish Stew (CR)": {ts: tsCoo, fl: "C", op: 1, components: [
        {compName: "Cooked Bass", ip: 2},
        {compName: "Cooked Tigerfish", ip: 2},
        {compName: "Spiced Cuttlefish", ip: 2},
    ]},
    "Dried Apple Spices": {ts: tsCoo, fl: "D", op: 1, components: [
        {compName: "Apple", ip: 2},
    ]},
    "Dried Banana Spice": {ts: tsCoo, fl: "D", op: 1, components: [
        {compName: "Banana", ip: 2},
    ]},
    "Dried Orange Spice": {ts: tsCoo, fl: "D", op: 1, components: [
        {compName: "Orange", ip: 2},
    ]},
    "Egg Nog": {ts: tsCoo, fl: "E", op: 1, components: [
        {compName: "Egg", ip: 3},
        {compName: "Holiday Spices", ip: 5},
    ]},
    "Fangtooth Surprise (random)": {ts: tsCoo, fl: "F", op: 1, components: [
        {compName: "Spiced Angelfish", ip: 1},
        {compName: "Cooked Fangtooth", ip: 3},
    ]},
    "Featherback Surprise (random)": {ts: tsCoo, fl: "F", op: 1, components: [
        {compName: "Spiced Blackfish", ip: 1},
        {compName: "Cooked Featherback", ip: 3},
    ]},
    "Fire Catfish Chili (Random)": {ts: tsCoo, fl: "F", op: 1, components: [
        {compName: "Salt", ip: 1},
        {compName: "Spiced Catfish", ip: 2},
        {compName: "Cooked Fire Goby", ip: 2},
    ]},
    "Fish Chowder (Random)": {ts: tsCoo, fl: "F", op: 1, components: [
        {compName: "Cooked Pike", ip: 1},
        {compName: "Cooked Trout", ip: 1},
        {compName: "Salted Eel", ip: 2},
        {compName: "Salt", ip: 2},
        {compName: "Salted Salmon", ip: 5},
    ]},
    "Flounder Cider Soup (DR)": {ts: tsCoo, fl: "F", op: 1, components: [
        {compName: "Cooked Flounder", ip: 1},
        {compName: "Apple Cider", ip: 1},
        {compName: "Grilled Flounder", ip: 1},
    ]},
    "Fried Eggs & Rings (GD)": {ts: tsCoo, fl: "F", op: 1, components: [
        {compName: "Apple Cider", ip: 1},
        {compName: "Scrambled Eggs", ip: 3},
        {compName: "Squid Rings", ip: 9},
    ]},
    "Fried Mackerel": {ts: tsCoo, fl: "F", op: 1, components: [
        {compName: "Mackerel", ip: 5},
    ]},
    "Fried Trout (Gold Drop%)": {ts: tsCoo, fl: "F", op: 1, components: [
        {compName: "Cooked Trout", ip: 3},
    ]},
    "Ghostfish Omelette (CR)": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Cooked Ghostfish", ip: 3},
        {compName: "Giant Egg", ip: 3},
    ]},
    "Ghostfish Surprise (Random)": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Fried Mackerel", ip: 2},
        {compName: "Spiced Lionfish", ip: 2},
        {compName: "Cooked Ghostfish", ip: 1},
    ]},
    "Ghosttooth Soup (Invis)": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Cooked Fangtooth", ip: 3},
        {compName: "Ghostshroom", ip: 5},
    ]},
    "Golden Goulash (Create Rate)": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Cooked Trout", ip: 2},
        {compName: "Salted Eel", ip: 1},
        {compName: "Grind Goldenbush Spice", ip: 2},
    ]},
    "Grape Eggs (Travel)": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Grape Spice", ip: 3},
        {compName: "Iron Egg", ip: 3},
    ]},
    "Grape Haddock Soup (DR)": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Cooked Haddock", ip: 3},
        {compName: "Grape Spice", ip: 2},
        {compName: "Terrorfish", ip: 4},
    ]},
    "Grape Spice": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Grape", ip: 2},
    ]},
    "Grilled Flounder": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Flounder", ip: 5},
        {compName: "Dried Apple Spices", ip: 1},
    ]},
    "Grilled Perch": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Perch", ip: 5},
    ]},
    "Grilled Shrimp": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Shrimp", ip: 2},
    ]},
    "Grind Goldenbush Spice": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Goldenbush", ip: 4},
        {compName: "Salt", ip: 1},
    ]},
    "Grind Kingsbloom Spice": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Kingsbloom", ip: 6},
    ]},
    "Grind Rosemary Spice": {ts: tsCoo, fl: "G", op: 1, components: [
        {compName: "Rosemary", ip: 5},
    ]},
    "Hot Chocolate": {ts: tsCoo, fl: "H", op: 1, components: [
        {compName: "Cocoa Fish", ip: 8},
    ]},
    "Icefeather Soup (Invis) {051}": {ts: tsCoo, fl: "I", op: 1, components: [
        {compName: "Cooked Featherback", ip: 3},
        {compName: "Icebloom", ip: 5},
    ]},
    "Icefeather Soup (Invis) {071}": {ts: tsCoo, fl: "I", op: 1, components: [
        {compName: "Cooked Nettlefish", ip: 3},
        {compName: "Frostweed", ip: 5},
    ]},
    "Icefeather Soup (Invis) {101}": {ts: tsCoo, fl: "I", op: 1, components: [
        {compName: "Cooked Arapaima", ip: 3},
        {compName: "Nightshade", ip: 5},
    ]},
    "Icefeather Soup (Invis) {131}": {ts: tsCoo, fl: "I", op: 1, components: [
        {compName: "Cooked Crawfish", ip: 3},
        {compName: "Wild Garlic", ip: 5},
    ]},
    "Icefish Delight (Gold Drop)": {ts: tsCoo, fl: "I", op: 1, components: [
        {compName: "Cooked Icefish", ip: 4},
        {compName: "Apricot Spice", ip: 1},
    ]},
    "King Sunray Steak (CR)": {ts: tsCoo, fl: "K", op: 1, components: [
        {compName: "Cooked Sunray", ip: 2},
        {compName: "Grind Kingsbloom Spice", ip: 2},
    ]},
    "Knifefish Delight (Gold Drop)": {ts: tsCoo, fl: "K", op: 1, components: [
        {compName: "Cooked Knifefish", ip: 4},
        {compName: "Peach Spice", ip: 1},
    ]},
    "Lancetail Steak & Eggs (DR)": {ts: tsCoo, fl: "L", op: 1, components: [
        {compName: "Cooked Lancetail", ip: 3},
        {compName: "Scrambled Eggs", ip: 2},
    ]},
    "Mackerel Chowder (DR)": {ts: tsCoo, fl: "M", op: 1, components: [
        {compName: "Fried Mackerel", ip: 4},
        {compName: "Lionfish", ip: 1},
        {compName: "Spiced Lionfish", ip: 1},
        {compName: "Cooked Ghostfish", ip: 1},
    ]},
    "Minced Catfish": {ts: tsCoo, fl: "M", op: 1, components: [
        {compName: "Apple", ip: 3},
        {compName: "Catfish", ip: 9},
        {compName: "Dogweed", ip: 1},
    ]},
    "Minced Pickerel": {ts: tsCoo, fl: "M", op: 1, components: [
        {compName: "Pickerel", ip: 9},
        {compName: "Angelflower", ip: 3},
        {compName: "Honey", ip: 1},
    ]},
    "Nettlefish Surprise (random)": {ts: tsCoo, fl: "N", op: 1, components: [
        {compName: "Spiced Terrorfish", ip: 1},
        {compName: "Cooked Nettlefish", ip: 3},
    ]},
    "Nile Tilapia Stew (CR)": {ts: tsCoo, fl: "N", op: 1, components: [
        {compName: "Cooked Barracuda", ip: 2},
        {compName: "Cooked Whitefish", ip: 2},
        {compName: "Spiced Nile Tilapia", ip: 2},
    ]},
    "Omelette Flambée (CR)": {ts: tsCoo, fl: "O", op: 1, components: [
        {compName: "Cooked Flounder", ip: 2},
        {compName: "Spiced Catfish", ip: 2},
        {compName: "Scrambled Eggs", ip: 2},
    ]},
    "Orange Scrambled Eggs (Inv)": {ts: tsCoo, fl: "O", op: 1, components: [
        {compName: "Dried Orange Spice", ip: 1},
        {compName: "Scrambled Eggs", ip: 3},
    ]},
    "Party Cake": {ts: tsCoo, fl: "P", op: 1, components: [
        {compName: "Rosemary", ip: 50},
        {compName: "Flax", ip: 25},
        {compName: "Salt", ip: 12},
        {compName: "Honey", ip: 15},
    ]},
    "Peach Eggs (Travel)": {ts: tsCoo, fl: "P", op: 1, components: [
        {compName: "Peach Spice", ip: 3},
        {compName: "Speckled Egg", ip: 3},
    ]},
    "Peach Spice": {ts: tsCoo, fl: "P", op: 1, components: [
        {compName: "Peach", ip: 2},
    ]},
    "Pear Eggs (Travel)": {ts: tsCoo, fl: "P", op: 1, components: [
        {compName: "Pear Spice", ip: 3},
        {compName: "Maple Syrup", ip: 3},
    ]},
    "Pear Spice": {ts: tsCoo, fl: "P", op: 1, components: [
        {compName: "Pear", ip: 2},
    ]},
    "Perch Fishsticks (Random)": {ts: tsCoo, fl: "P", op: 1, components: [
        {compName: "Grilled Perch", ip: 3},
        {compName: "Dried Orange Spice", ip: 1},
    ]},
    "Pickerel Kebobs (Invis)": {ts: tsCoo, fl: "P", op: 1, components: [
        {compName: "Salted Pickerel", ip: 3},
        {compName: "Grind Kingsbloom Spice", ip: 2},
    ]},
    "Reindeer Salad": {ts: tsCoo, fl: "R", op: 1, components: [
        {compName: "Midnight Berry", ip: 15},
        {compName: "Rosemary", ip: 2},
        {compName: "Egg", ip: 2},
        {compName: "Salt", ip: 2},
        {compName: "Honey", ip: 3},
        {compName: "Reindeer Grass", ip: 8},
    ]},
    "Rockperch Chowder (Travel)": {ts: tsCoo, fl: "R", op: 1, components: [
        {compName: "Grilled Perch", ip: 2},
        {compName: "Salted Rockfish", ip: 2},
    ]},
    "Salmon Steak (Random)": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Salted Salmon", ip: 3},
    ]},
    "Salted Eel": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Eel", ip: 2},
        {compName: "Salt", ip: 1},
    ]},
    "Salted Eel Jerky (Travel Buff)": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Salted Eel", ip: 2},
        {compName: "Salt", ip: 1},
    ]},
    "Salted Pickerel": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Pickerel", ip: 2},
        {compName: "Salt", ip: 1},
    ]},
    "Salted Rockfish": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Rockfish", ip: 3},
        {compName: "Salt", ip: 1},
    ]},
    "Salted Salmon": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Salt", ip: 1},
        {compName: "Salmon", ip: 2},
    ]},
    "Salty Orange Rockfish (CR)": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Dried Orange Spice", ip: 3},
        {compName: "Salted Rockfish", ip: 3},
    ]},
    "Scallions Eggs (Travel)": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Scallions Spice", ip: 3},
        {compName: "Mango", ip: 3},
    ]},
    "Scallions Spice": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Scallions", ip: 2},
    ]},
    "Scrambled Eggs": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Large Egg", ip: 3},
    ]},
    "Seafood Medley (Drop Rate)": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Cooked Pike", ip: 1},
        {compName: "Cooked Trout", ip: 1},
        {compName: "Salted Eel", ip: 1},
        {compName: "Grind Rosemary Spice", ip: 1},
        {compName: "Salted Salmon", ip: 1},
    ]},
    "Shrimp Omelette (Drop Rate)": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Grilled Shrimp", ip: 3},
        {compName: "Egg", ip: 2},
        {compName: "Grind Kingsbloom Spice", ip: 1},
    ]},
    "Smoked Halibut": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Halibut", ip: 5},
    ]},
    "Spiced Angelfish": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Angelfish", ip: 3},
        {compName: "Apricot Spice", ip: 1},
    ]},
    "Spiced Blackfish": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Blackfish", ip: 3},
        {compName: "Peach Spice", ip: 1},
    ]},
    "Spiced Catfish": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Dried Apple Spices", ip: 2},
        {compName: "Catfish", ip: 3},
    ]},
    "Spiced Cuttlefish": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Cuttlefish", ip: 3},
        {compName: "Pear Spice", ip: 1},
    ]},
    "Spiced Lionfish": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Dried Banana Spice", ip: 1},
        {compName: "Lionfish", ip: 3},
    ]},
    "Spiced Nile Tilapia": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Nile Tilapia", ip: 3},
        {compName: "Scallions Spice", ip: 1},
    ]},
    "Spiced Pike (Invis)": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Cooked Pike", ip: 2},
        {compName: "Egg", ip: 1},
        {compName: "Grind Rosemary Spice", ip: 2},
    ]},
    "Spiced Terrorfish": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Grape Spice", ip: 1},
        {compName: "Terrorfish", ip: 3},
    ]},
    "Spicy Chowder (Travel)": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Grilled Shrimp", ip: 3},
        {compName: "Salted Pickerel", ip: 3},
        {compName: "Cooked Sunray", ip: 2},
        {compName: "Grind Kingsbloom Spice", ip: 2},
    ]},
    "Squid Rings": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Squid", ip: 1},
    ]},
    "Sunlight Treasure Elixir": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Serpent Vine", ip: 3},
        {compName: "Divinity Essence", ip: 3},
        {compName: "Phantomfin", ip: 3},
        {compName: "Sunlightbloom", ip: 3},
    ]},
    "Supreme Treasure Elixir": {ts: tsCoo, fl: "S", op: 1, components: [
        {compName: "Eagletear", ip: 3},
        {compName: "Cosmos Essence", ip: 3},
        {compName: "Golembloom", ip: 3},
        {compName: "Hawkfin", ip: 3},
    ]},
    "Terrorfish Stew (CR)": {ts: tsCoo, fl: "T", op: 1, components: [
        {compName: "Cooked Cod", ip: 2},
        {compName: "Cooked Haddock", ip: 2},
        {compName: "Spiced Terrorfish", ip: 2},
    ]},
    "Tigerfish Delight (Gold Drop)": {ts: tsCoo, fl: "T", op: 1, components: [
        {compName: "Cooked Tigerfish", ip: 4},
        {compName: "Pear Spice", ip: 1},
    ]},
    "Treasure Hunting Elixir": {ts: tsCoo, fl: "T", op: 1, components: [
        {compName: "Dragontear", ip: 3},
        {compName: "Ancient Essence", ip: 3},
        {compName: "Ancientbloom", ip: 3},
        {compName: "Gilgrash", ip: 3},
    ]},
    "Trunkfish Delight (Gold Drop)": {ts: tsCoo, fl: "T", op: 1, components: [
        {compName: "Cooked Trunkfish", ip: 3},
        {compName: "Spiced Lionfish", ip: 1},
    ]},
    "Whitefish Delight (Gold Drop)": {ts: tsCoo, fl: "W", op: 1, components: [
        {compName: "Cooked Whitefish", ip: 4},
        {compName: "Scallions Spice", ip: 1},
    ]},
    // crafting
    "Adamantite Pick": {ts: tsCra, fl: "A", op: 1, components: [
        {compName: "Adamantite Pick Blade", ip: 1},
        {compName: "Adamantite Fitting", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Adamantite Sceptre": {ts: tsCra, fl: "A", op: 1, components: [
        {compName: "Enchanted Sun Opal", ip: 1},
        {compName: "Liontail Sandpaper", ip: 6},
        {compName: "Inscribed Adamantite Staff", ip: 1},
    ]},
    "Adamantite Shovel": {ts: tsCra, fl: "A", op: 1, components: [
        {compName: "Adamantite Fitting", ip: 1},
        {compName: "Adamantite Shovel Blade", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Adamantite Treasure Pick": {ts: tsCra, fl: "A", op: 1, components: [
        {compName: "Adamantite Pick Blade", ip: 1},
        {compName: "Adamantite Fitting", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Adamantite Wand": {ts: tsCra, fl: "A", op: 1, components: [
        {compName: "Enchanted Sun Opal", ip: 1},
        {compName: "Liontail Sandpaper", ip: 6},
        {compName: "Inscribed Adamantite Staff", ip: 1},
    ]},
    "Animal Fat": {ts: tsCra, fl: "A", op: 1, components: []},
    "Aspen Fishing Rod": {ts: tsCra, fl: "A", op: 1, components: [
        {compName: "Smooth Aspen Rod", ip: 1},
        {compName: "Dogweed Fishing Line", ip: 1},
    ]},
    "Birch Fishing Rod": {ts: tsCra, fl: "B", op: 1, components: [
        {compName: "Smooth Birch Rod", ip: 1},
        {compName: "Milkweed Fishing Line", ip: 1},
    ]},
    "Birch Mallet Top": {ts: tsCra, fl: "B", op: 1, components: [
        {compName: "Rough Birch Block", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 2},
    ]},
    "Black Lotus Line": {ts: tsCra, fl: "B", op: 1, components: [
        {compName: "Black Lotus", ip: 4},
    ]},
    "Black Lotus Thread": {ts: tsCra, fl: "B", op: 8, components: [
        {compName: "Black Lotus", ip: 1},
    ]},
    "Bloodstone Mining Pick": {ts: tsCra, fl: "B", op: 1, components: [
        {compName: "Bloodstone Pick Blade", ip: 1},
        {compName: "Bloodstone Fitting", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Bloodstone Shovel": {ts: tsCra, fl: "B", op: 1, components: [
        {compName: "Bloodstone Fitting", ip: 1},
        {compName: "Bloodstone Shovel Blade", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Bloodstone Treasure Pick": {ts: tsCra, fl: "B", op: 1, components: [
        {compName: "Bloodstone Pick Blade", ip: 1},
        {compName: "Bloodstone Fitting", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Bloodstone Wand": {ts: tsCra, fl: "B", op: 1, components: [
        {compName: "Enchanted Amber", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 6},
        {compName: "Inscribed Bloodstone Staff", ip: 1},
    ]},
    "Cedar Fishing Rod": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Smooth Cedar Rod", ip: 1},
        {compName: "Milkweed Fishing Line", ip: 1},
    ]},
    "Cedar Mallet Top": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Rough Cedar Block", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 3},
    ]},
    "Cherry Gloves": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cherry Oil", ip: 1},
        {compName: "Wild Garlic Yarn", ip: 4},
        {compName: "Flame Lotus Thread", ip: 14},
    ]},
    "Chromium Mining Pick": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Chromium Pick Blade", ip: 1},
        {compName: "Chromium Fitting", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Chromium Shovel": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Chromium Fitting", ip: 1},
        {compName: "Chromium Shovel Blade", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Chromium Treasure Pick": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Chromium Pick Blade", ip: 1},
        {compName: "Chromium Fitting", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Chromium Wand": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Enchanted Garnet", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 6},
        {compName: "Inscribed Chromium Staff", ip: 1},
    ]},
    "Clay Mug": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Light Clay Mixture", ip: 2},
    ]},
    "Cobalt Mining Pick": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cobalt Pick Blade", ip: 1},
        {compName: "Cobalt Fitting", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Cobalt Shovel": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Cobalt Fitting", ip: 1},
        {compName: "Cobalt Shovel Blade", ip: 1},
    ]},
    "Cobalt Treasure Pick": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cobalt Pick Blade", ip: 1},
        {compName: "Cobalt Fitting", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Cobalt Wand": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Enchanted Malachite", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 5},
        {compName: "Inscribed Cobalt Staff", ip: 1},
    ]},
    "Copper Mining Pick": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Copper Pick Blade", ip: 1},
        {compName: "Copper Fitting", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
    ]},
    "Copper Shovel": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Copper Fitting", ip: 1},
        {compName: "Copper Shovel Blade", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
    ]},
    "Copper Treasure Pick": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Copper Pick Blade", ip: 1},
        {compName: "Copper Fitting", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
    ]},
    "Copper Wand": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Enchanted Gem", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 2},
        {compName: "Inscribed Copper Staff", ip: 1},
    ]},
    "Cotton Yarn": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cotton", ip: 5},
    ]},
    "Craft Glue": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Fresh Water", ip: 15},
        {compName: "Ground Spices", ip: 10},
    ]},
    "Curing Tank": {ts: tsCra, fl: "C", op: 1, components: []},
    "Cursed Adamantite Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Sun Opal", ip: 1},
        {compName: "Liontail Sandpaper", ip: 6},
        {compName: "Inscribed Adamantite Staff", ip: 1},
    ]},
    "Cursed Bloodstone Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Amber", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 6},
        {compName: "Inscribed Bloodstone Staff", ip: 1},
    ]},
    "Cursed Chromium Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Garnet", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 6},
        {compName: "Inscribed Chromium Staff", ip: 1},
    ]},
    "Cursed Cobalt Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Malachite", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 5},
        {compName: "Inscribed Cobalt Staff", ip: 1},
    ]},
    "Cursed Copper Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Gem", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 2},
        {compName: "Inscribed Copper Staff", ip: 1},
    ]},
    "Cursed Iridium Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Amethyst", ip: 1},
        {compName: "Liontail Sandpaper", ip: 4},
        {compName: "Inscribed Iridium Staff", ip: 1},
    ]},
    "Cursed Iron Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Citrine", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 4},
        {compName: "Inscribed Iron Staff", ip: 1},
    ]},
    "Cursed Manganese Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Peridot", ip: 1},
        {compName: "Dragon's Tongue Sandpaper", ip: 6},
        {compName: "Inscribed Manganese Staff", ip: 1},
    ]},
    "Cursed Mithril Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Aquamarine", ip: 1},
        {compName: "Foxflower Sandpaper", ip: 6},
        {compName: "Inscribed Mithril Staff", ip: 1},
    ]},
    "Cursed Tin Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Agate", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 3},
        {compName: "Inscribed Tin Staff", ip: 1},
    ]},
    "Cursed Titanium Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Melanite", ip: 1},
        {compName: "Eagletear Sandpaper", ip: 6},
        {compName: "Inscribed Titanium Staff", ip: 1},
    ]},
    "Cursed Tourmaline Staff": {ts: tsCra, fl: "C", op: 1, components: [
        {compName: "Cursed Mercury", ip: 1},
        {compName: "Inscribed Tourmaline Staff", ip: 1},
        {compName: "Wraith Thorns Sandpaper", ip: 6},
    ]},
    "Dark Cotton Yarn": {ts: tsCra, fl: "D", op: 1, components: [
        {compName: "Dark Cotton", ip: 5},
    ]},
    "Devilweed Line": {ts: tsCra, fl: "D", op: 1, components: [
        {compName: "Devilweed", ip: 4},
    ]},
    "Devilweed Thread": {ts: tsCra, fl: "D", op: 8, components: [
        {compName: "Devilweed", ip: 1},
    ]},
    "Dogweed Fishing Line": {ts: tsCra, fl: "D", op: 1, display: "Dogweed Line", components: [
        {compName: "Dogweed", ip: 3},
    ]},
    "Dogweed Thread": {ts: tsCra, fl: "D", op: 8, components: [
        {compName: "Dogweed", ip: 1},
    ]},
    "Dragon's Tongue Sandpaper": {ts: tsCra, fl: "D", op: 6, components: [
        {compName: "Dragon's Tongue", ip: 2},
    ]},
    "Eagletear Sandpaper": {ts: tsCra, fl: "E", op: 6, components: [
        {compName: "Eagletear", ip: 2},
    ]},
    "Earthern Bowl": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Light Earthen Clay", ip: 3},
    ]},
    "Easter Basket": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Form Copper Nails", ip: 15},
        {compName: "Easter Grass", ip: 5},
        {compName: "Essence of Easter", ip: 1},
        {compName: "Hickory Slats", ip: 50},
    ]},
    "Enchanted Bloodstone Sceptre": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Enchanted Amber", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 6},
        {compName: "Inscribed Bloodstone Staff", ip: 1},
    ]},
    "Enchanted Chromium Sceptre": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Enchanted Garnet", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 6},
        {compName: "Inscribed Chromium Staff", ip: 1},
    ]},
    "Enchanted Cobalt Sceptre": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Enchanted Malachite", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 5},
        {compName: "Inscribed Cobalt Staff", ip: 1},
    ]},
    "Enchanted Copper Sceptre": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Enchanted Gem", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 2},
        {compName: "Inscribed Copper Staff", ip: 1},
    ]},
    "Enchanted Iridium Sceptre": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Enchanted Amethyst", ip: 1},
        {compName: "Liontail Sandpaper", ip: 4},
        {compName: "Inscribed Iridium Staff", ip: 1},
    ]},
    "Enchanted Iron Sceptre": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Enchanted Citrine", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 4},
        {compName: "Inscribed Iron Staff", ip: 1},
    ]},
    "Enchanted Tin Sceptre": {ts: tsCra, fl: "E", op: 1, components: [
        {compName: "Enchanted Agate", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 3},
        {compName: "Inscribed Tin Staff", ip: 1},
    ]},
    "Fern Mallet Top": {ts: tsCra, fl: "F", op: 1, components: [
        {compName: "Rough Fern Block", ip: 1},
        {compName: "Dragon's Tongue Sandpaper", ip: 6},
    ]},
    "Flame Lotus Line": {ts: tsCra, fl: "F", op: 1, components: [
        {compName: "Flame Lotus", ip: 4},
    ]},
    "Flame Lotus Thread": {ts: tsCra, fl: "F", op: 8, components: [
        {compName: "Flame Lotus", ip: 1},
    ]},
    "Foxflower Sandpaper": {ts: tsCra, fl: "F", op: 6, components: [
        {compName: "Foxflower", ip: 2},
    ]},
    "Frostweed Yarn": {ts: tsCra, fl: "F", op: 1, components: [
        {compName: "Frostweed", ip: 5},
    ]},
    "Goldenbush Cotton Gloves": {ts: tsCra, fl: "G", op: 1, components: [
        {compName: "Extract Goldenbush Oil", ip: 1},
        {compName: "Cotton Yarn", ip: 3},
        {compName: "Milkweed Thread", ip: 4},
    ]},
    "Gorethistle Cotton Gloves": {ts: tsCra, fl: "G", op: 1, components: [
        {compName: "Extract Gorethistle Oil", ip: 1},
        {compName: "Cotton Yarn", ip: 3},
        {compName: "Milkweed Thread", ip: 8},
    ]},
    "Hard Blackened Clay": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Pyrestone", ip: 6},
        {compName: "Blackened Clay", ip: 6},
    ]},
    "Hard Blackened Clay Bowl": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Hard Blackened Clay", ip: 4},
    ]},
    "Hard Clay Bowl": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Hard Clay Mixture", ip: 5},
    ]},
    "Hard Clay Mixture": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Shale Rock", ip: 10},
        {compName: "Clay", ip: 6},
    ]},
    "Hard Fire Clay": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Slate", ip: 12},
        {compName: "Fire Clay", ip: 6},
    ]},
    "Hard Fire Clay Bowl": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Hard Fire Clay", ip: 5},
    ]},
    "Hard Golden Clay": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Dark Silver", ip: 6},
        {compName: "Golden Clay", ip: 6},
    ]},
    "Hard Golden Clay Bowl": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Hard Golden Clay", ip: 4},
    ]},
    "Hard Kaolinite": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Basalt", ip: 6},
        {compName: "Kaolinite", ip: 6},
    ]},
    "Hard Kaolinite Bowl": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Hard Kaolinite", ip: 4},
    ]},
    "Hard Molten Clay": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Crystal", ip: 6},
        {compName: "Molten Clay", ip: 6},
    ]},
    "Hard Molten Clay Bowl": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Hard Molten Clay", ip: 4},
    ]},
    "Hard Wolf Ash Clay": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Obsidian", ip: 6},
        {compName: "Wolf Ash Clay", ip: 6},
    ]},
    "Hard Wolf Ash Clay Bowl": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Hard Wolf Ash Clay", ip: 4},
    ]},
    "Hemlock Mallet Top": {ts: tsCra, fl: "H", op: 1, components: [
        {compName: "Rough Hemlock Block", ip: 1},
        {compName: "Liontail Sandpaper", ip: 4},
    ]},
    "Horsetail Sandpaper": {ts: tsCra, fl: "H", op: 6, components: [
        {compName: "Horsetail", ip: 2},
    ]},
    "Icebloom Yarn": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Icebloom", ip: 5},
    ]},
    "Iridium Mining Pick": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Iridium Pick Blade", ip: 1},
        {compName: "Iridium Fitting", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Iridium Shovel": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Iridium Fitting", ip: 1},
        {compName: "Iridium Shovel Blade", ip: 1},
        {compName: "Wild Cherry Handle", ip: 1},
    ]},
    "Iridium Treasure Pick": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Iridium Pick Blade", ip: 1},
        {compName: "Iridium Fitting", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
    ]},
    "Iridium Wand": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Enchanted Amethyst", ip: 1},
        {compName: "Liontail Sandpaper", ip: 4},
        {compName: "Inscribed Iridium Staff", ip: 1},
    ]},
    "Iron Mining Pick": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Iron Pick Blade", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Iron Fitting", ip: 1},
    ]},
    "Iron Shovel": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Iron Fitting", ip: 1},
        {compName: "Iron Shovel Blade", ip: 1},
    ]},
    "Iron Treasure Pick": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Iron Pick Blade", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Iron Fitting", ip: 1},
    ]},
    "Iron Wand": {ts: tsCra, fl: "I", op: 1, components: [
        {compName: "Enchanted Citrine", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 4},
        {compName: "Inscribed Iron Staff", ip: 1},
    ]},
    "Ladyslipper Dark Gloves": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Extract Ladyslipper Oil", ip: 1},
        {compName: "Dark Cotton Yarn", ip: 3},
        {compName: "Dogweed Thread", ip: 10},
    ]},
    "Large Arrow Shaft": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Diamond Arrow Shaft", ip: 5},
    ]},
    "Light Clay Bowl": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Light Clay Mixture", ip: 4},
    ]},
    "Light Clay Mixture": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Stone", ip: 3},
        {compName: "Clay", ip: 2},
    ]},
    "Light Earthen Clay": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Basalt", ip: 4},
        {compName: "Earthen Clay", ip: 3},
    ]},
    "Light Fire Clay": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Slate", ip: 3},
        {compName: "Fire Clay", ip: 2},
    ]},
    "Light Fire Clay Bowl": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Light Fire Clay", ip: 4},
    ]},
    "Light Kaolinite": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Quartz", ip: 3},
        {compName: "Kaolinite", ip: 2},
    ]},
    "Light Kaolinite Bowl": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Light Kaolinite", ip: 4},
    ]},
    "Linen Fibres": {ts: tsCra, fl: "L", op: 1, components: [
        {compName: "Curing Tank", ip: 1},
        {compName: "Flax", ip: 6},
        {compName: "Rollers", ip: 2},
        {compName: "Water", ip: 6},
    ]},
    "Liontail Sandpaper": {ts: tsCra, fl: "L", op: 6, components: [
        {compName: "Liontail", ip: 2},
    ]},
    "Manganese Pick": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Manganese Pick Blade", ip: 1},
        {compName: "Manganese Fitting", ip: 1},
        {compName: "Maidenhair Handle", ip: 1},
    ]},
    "Manganese Sceptre": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Enchanted Peridot", ip: 1},
        {compName: "Dragon's Tongue", ip: 6},
        {compName: "Inscribed Manganese Staff", ip: 1},
    ]},
    "Manganese Shovel": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Manganese Fitting", ip: 1},
        {compName: "Manganese Shovel Blade", ip: 1},
        {compName: "Maidenhair Handle", ip: 1},
    ]},
    "Manganese Treasure Pick": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Manganese Pick Blade", ip: 1},
        {compName: "Manganese Fitting", ip: 1},
        {compName: "Maidenhair Handle", ip: 1},
    ]},
    "Manganese Wand": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Enchanted Peridot", ip: 1},
        {compName: "Dragon's Tongue", ip: 6},
        {compName: "Inscribed Manganese Staff", ip: 1},
    ]},
    "Medium Clay Bowl": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Medium Clay Mixture", ip: 4},
    ]},
    "Medium Clay Mixture": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Sandstone", ip: 4},
        {compName: "Clay", ip: 4},
    ]},
    "Medium Fire Clay": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Slate", ip: 6},
        {compName: "Fire Clay", ip: 4},
    ]},
    "Medium Fire Clay Bowl": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Medium Fire Clay", ip: 4},
    ]},
    "Medium Kaolinite": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Quartz", ip: 6},
        {compName: "Kaolinite", ip: 4},
    ]},
    "Medium Kaolinite Bowl": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Medium Kaolinite", ip: 4},
    ]},
    "Medusaroot Gloves": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Medusaroot Oil", ip: 1},
        {compName: "Dark Cotton Yarn", ip: 4},
        {compName: "Devilweed Thread", ip: 14},
    ]},
    "Milkweed Fishing Line": {ts: tsCra, fl: "M", op: 1, display: "Milkweed Line", components: [
        {compName: "Milkweed", ip: 3},
    ]},
    "Milkweed Thread": {ts: tsCra, fl: "M", op: 8, components: [
        {compName: "Milkweed", ip: 1},
    ]},
    "Mithril Pick": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Mithril Pick Blade", ip: 1},
        {compName: "Mithril Fitting", ip: 1},
        {compName: "Mahogany Handle", ip: 1},
    ]},
    "Mithril Sceptre": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Enchanted Aquamarine", ip: 1},
        {compName: "Foxflower Sandpaper", ip: 6},
        {compName: "Inscribed Mithril Staff", ip: 1},
    ]},
    "Mithril Shovel": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Mithril Fitting", ip: 1},
        {compName: "Mithril Shovel Blade", ip: 1},
        {compName: "Mahogany Handle", ip: 1},
    ]},
    "Mithril Treasure Pick": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Mithril Pick Blade", ip: 1},
        {compName: "Mithril Fitting", ip: 1},
        {compName: "Mahogany Handle", ip: 1},
    ]},
    "Mithril Wand": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Enchanted Aquamarine", ip: 1},
        {compName: "Foxflower Sandpaper", ip: 6},
        {compName: "Inscribed Mithril Staff", ip: 1},
    ]},
    "Moonbeard Cotton Gloves": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Extract Moonbeard Oil", ip: 1},
        {compName: "Cotton Yarn", ip: 3},
        {compName: "Dogweed Thread", ip: 8},
    ]},
    "Mora Mallet Top": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Rough Mora Block", ip: 1},
        {compName: "Eagletear Sandpaper", ip: 6},
    ]},
    "Mummy Wraps": {ts: tsCra, fl: "M", op: 1, components: [
        {compName: "Copper Fasteners", ip: 4},
        {compName: "Linen Fibres", ip: 4},
    ]},
    "Nest Box": {ts: tsCra, fl: "N", op: 1, components: [
        {compName: "Coop Wood", ip: 8},
    ]},
    "Nightshade Yarn": {ts: tsCra, fl: "N", op: 1, components: [
        {compName: "Nightshade", ip: 5},
    ]},
    "Oak Mallet Top": {ts: tsCra, fl: "O", op: 1, components: [
        {compName: "Rough Oak Block", ip: 1},
        {compName: "Foxflower Sandpaper", ip: 6},
    ]},
    "Orchid Gloves": {ts: tsCra, fl: "O", op: 1, components: [
        {compName: "Orchid Oil", ip: 1},
        {compName: "Nightshade Yarn", ip: 4},
        {compName: "Serpent Vine Thread", ip: 1},
    ]},
    "Pine Mallet Top": {ts: tsCra, fl: "P", op: 1, components: [
        {compName: "Rough Pine Block", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 4},
    ]},
    "Pixieroot Cotton Gloves": {ts: tsCra, fl: "P", op: 1, components: [
        {compName: "Extract Pixieroot Oil", ip: 1},
        {compName: "Cotton Yarn", ip: 3},
        {compName: "Milkweed Thread", ip: 6},
    ]},
    "Pruning Shears": {ts: tsCra, fl: "P", op: 1, components: [
        {compName: "Scrap Metal", ip: 8},
    ]},
    "Reindeer Net": {ts: tsCra, fl: "R", op: 1, components: [
        {compName: "Tough Rope", ip: 3},
    ]},
    "Rollers": {ts: tsCra, fl: "R", op: 1, components: []},
    "Rosewood Fishing Rod": {ts: tsCra, fl: "R", op: 1, components: [
        {compName: "Black Lotus Line", ip: 1},
        {compName: "Smooth Rosewood Rod", ip: 1},
    ]},
    "Satinwood Fishing Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Smooth Satinwood Rod", ip: 1},
        {compName: "Venomweed Line", ip: 1},
    ]},
    "Serpent Vine Line": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Serpent Vine", ip: 4},
    ]},
    "Serpent Vine Thread": {ts: tsCra, fl: "S", op: 8, components: [
        {compName: "Serpent Vine", ip: 1},
    ]},
    "Sharkwood Fishing Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Devilweed Line", ip: 1},
        {compName: "Smooth Sharkwood Rod", ip: 1},
    ]},
    "Shaving Cream": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Animal Fat", ip: 1},
        {compName: "Alkali", ip: 2},
    ]},
    "Shaving Products": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Shaving Cream", ip: 1},
        {compName: "Sponge", ip: 1},
        {compName: "Steel Razor Blade", ip: 1},
    ]},
    "Silver Birch Mallet Top": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Rough Silver Birch Block", ip: 1},
        {compName: "Wraith Thorns Sandpaper", ip: 6},
    ]},
    "Smooth Aspen Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Horsetail Sandpaper", ip: 3},
        {compName: "Rough Aspen Rod", ip: 2},
    ]},
    "Smooth Birch Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Horsetail Sandpaper", ip: 1},
        {compName: "Rough Birch Rod", ip: 2},
    ]},
    "Smooth Cedar Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Horsetail Sandpaper", ip: 2},
        {compName: "Rough Cedar Rod", ip: 2},
    ]},
    "Smooth Rosewood Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Foxflower Sandpaper", ip: 4},
        {compName: "Rough Rosewood Rod", ip: 2},
    ]},
    "Smooth Satinwood Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Eagletear Sandpaper", ip: 4},
        {compName: "Rough Satinwood Rod", ip: 5},
    ]},
    "Smooth Sharkwood Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Liontail Sandpaper", ip: 4},
        {compName: "Smooth Sharkwood Rod", ip: 2},
    ]},
    "Smooth Tilia Cordata Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Rough Tilia Cordata Rod", ip: 2},
        {compName: "Wraith Thorns Sandpaper", ip: 4},
    ]},
    "Smooth White Willow Rod": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Dragon's Tongue Sandpaper", ip: 4},
        {compName: "Rough White Willow Rod", ip: 2},
    ]},
    "Snow Globe": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Fresh Water", ip: 2},
        {compName: "Gold Glitter", ip: 10},
        {compName: "Spawn Mystic Orb", ip: 1},
    ]},
    "Stonesilk Lily Gloves": {ts: tsCra, fl: "S", op: 1, components: [
        {compName: "Stonesilk Lily Oil", ip: 1},
        {compName: "Icebloom Yarn", ip: 4},
        {compName: "Venomweed Thread", ip: 14},
    ]},
    "Tiger Lily Gloves": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Tiger Lily Oil", ip: 1},
        {compName: "Frostweed Yarn", ip: 4},
        {compName: "Black Lotus Thread", ip: 14},
    ]},
    "Tilia Cordata Fishing Rod": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Flame Lotus Line", ip: 1},
        {compName: "Smooth Tilia Cordata Rod", ip: 1},
    ]},
    "Tin Mining Pick": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Tin Pick Blade", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Tin Fitting", ip: 1},
    ]},
    "Tin Shovel": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Tin Fitting", ip: 1},
        {compName: "Tin Shovel Blade", ip: 1},
    ]},
    "Tin Treasure Pick": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Tin Pick Blade", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Tin Fitting", ip: 1},
    ]},
    "Tin Wand": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Enchanted Agate", ip: 1},
        {compName: "Horsetail Sandpaper", ip: 3},
        {compName: "Inscribed Tin Staff", ip: 1},
    ]},
    "Titanium Pick": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Titanium Pick Blade", ip: 1},
        {compName: "Titanium Fitting", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Titanium Sceptre": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Enchanted Melanite", ip: 1},
        {compName: "Eagletear Sandpaper", ip: 6},
        {compName: "Inscribed Titanium Staff", ip: 1},
    ]},
    "Titanium Shovel": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Titanium Fitting", ip: 1},
        {compName: "Titanium Shovel Blade", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Titanium Treasure Pick": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Titanium Pick Blade", ip: 1},
        {compName: "Titanium Fitting", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Titanium Wand": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Enchanted Melanite", ip: 1},
        {compName: "Eagletear Sandpaper", ip: 6},
        {compName: "Inscribed Titanium Staff", ip: 1},
    ]},
    "Tough Rope": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Cotton", ip: 3},
        {compName: "Horsetail", ip: 3},
    ]},
    "Tourmaline Pick": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Tourmaline Pick Blade", ip: 1},
        {compName: "Tourmaline Fitting", ip: 1},
        {compName: "Elven Walnut Handle", ip: 1},
    ]},
    "Tourmaline Sceptre": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Enchanted Mercury", ip: 1},
        {compName: "Inscribed Tourmaline Staff", ip: 1},
        {compName: "Wraith Thorns Sandpaper", ip: 6},
    ]},
    "Tourmaline Shovel": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Tourmaline Fitting", ip: 1},
        {compName: "Tourmaline Shovel Blade", ip: 1},
        {compName: "Elven Walnut Handle", ip: 1},
    ]},
    "Tourmaline Treasure Pick": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Tourmaline Pick Blade", ip: 1},
        {compName: "Tourmaline Fitting", ip: 1},
        {compName: "Elven Walnut Handle", ip: 1},
    ]},
    "Tourmaline Wand": {ts: tsCra, fl: "T", op: 1, components: [
        {compName: "Enchanted Mercury", ip: 1},
        {compName: "Inscribed Tourmaline Staff", ip: 1},
        {compName: "Wraith Thorns Sandpaper", ip: 6},
    ]},
    "Venomweed Line": {ts: tsCra, fl: "V", op: 1, components: [
        {compName: "Venomweed", ip: 4},
    ]},
    "Venomweed Thread": {ts: tsCra, fl: "V", op: 8, components: [
        {compName: "Venomweed", ip: 1},
    ]},
    "Vilegourd Dark Gloves": {ts: tsCra, fl: "V", op: 1, components: [
        {compName: "Vilegourd Oil", ip: 1},
        {compName: "Dark Cotton Yarn", ip: 3},
        {compName: "Devilweed Thread", ip: 12},
    ]},
    "Walnut Mallet Top": {ts: tsCra, fl: "W", op: 1, components: [
        {compName: "Rough Walnut Block", ip: 1},
        {compName: "Liontail Sandpaper", ip: 6},
    ]},
    "White Willow Fishing Rod": {ts: tsCra, fl: "W", op: 1, components: [
        {compName: "Serpent Vine Line", ip: 1},
        {compName: "Smooth White Willow Rod", ip: 1},
    ]},
    "Wild Garlic Yarn": {ts: tsCra, fl: "W", op: 1, components: [
        {compName: "Wild Garlic", ip: 5},
    ]},
    "Wraith Thorns Sandpaper": {ts: tsCra, fl: "W", op: 6, components: [
        {compName: "Wraith Thorns", ip: 2},
    ]},
    "Wreath of Roses": {ts: tsCra, fl: "W", op: 1, components: [
        {compName: "Red Rose", ip: 6},
    ]},
    // disenchanting
    "Anarchy Essence": {ts: tsDis, fl: "A", op: 1, components: []},
    "Ancient Essence": {ts: tsDis, fl: "A", op: 1, components: []},
    "Arcane Powder": {ts: tsDis, fl: "A", op: 1, components: []},
    "Barrier Powder": {ts: tsDis, fl: "B", op: 1, components: []},
    "Chaos Essence": {ts: tsDis, fl: "C", op: 1, components: []},
    "Chimera Powder": {ts: tsDis, fl: "C", op: 1, components: []},
    "Cloud Wisp": {ts: tsDis, fl: "C", op: 1, components: []},
    "Cosmos Essence": {ts: tsDis, fl: "C", op: 1, components: []},
    "Demonic Essence": {ts: tsDis, fl: "D", op: 1, components: []},
    "Divinity Essence": {ts: tsDis, fl: "D", op: 1, components: []},
    "Elemental Powder": {ts: tsDis, fl: "E", op: 1, components: []},
    "Essence of Easter": {ts: tsDis, fl: "E", op: 1, components: []},
    "Ethereal Essence": {ts: tsDis, fl: "E", op: 1, components: []},
    "Ethereal Glow": {ts: tsDis, fl: "E", op: 1, components: []},
    "Fallen Dust": {ts: tsDis, fl: "F", op: 1, components: []},
    "Gold Glitter": {ts: tsDis, fl: "G", op: 1, components: []},
    "Grey Shielding Dust": {ts: tsDis, fl: "G", op: 1, components: []},
    "Heart Candy - I Love You": {ts: tsDis, fl: "H", op: 1, components: []},
    "Insanity Essence": {ts: tsDis, fl: "I", op: 1, components: []},
    "Invisible Powder": {ts: tsDis, fl: "I", op: 1, components: []},
    "Love Essence": {ts: tsDis, fl: "L", op: 1, components: []},
    "Lunar Dust": {ts: tsDis, fl: "L", op: 1, components: []},
    "Magic Glue": {ts: tsDis, fl: "M", op: 1, components: []},
    "Magma Powder": {ts: tsDis, fl: "M", op: 1, components: []},
    "Moonlight Essence": {ts: tsDis, fl: "M", op: 1, components: []},
    "Mystic Essence": {ts: tsDis, fl: "M", op: 1, components: []},
    "Mystic Salt": {ts: tsDis, fl: "M", op: 1, components: []},
    "Mystical Flower": {ts: tsDis, fl: "M", op: 1, components: []},
    "Nightfall Dust": {ts: tsDis, fl: "N", op: 1, components: []},
    "Party Essence": {ts: tsDis, fl: "P", op: 1, components: []},
    "Permafrost Powder": {ts: tsDis, fl: "P", op: 1, components: []},
    "Phantom Dust": {ts: tsDis, fl: "P", op: 1, components: []},
    "Powder of Protect": {ts: tsDis, fl: "P", op: 1, components: []},
    "Purity Powder": {ts: tsDis, fl: "P", op: 1, components: []},
    "Radiant Powder": {ts: tsDis, fl: "R", op: 1, components: []},
    "Rainbow Glitter": {ts: tsDis, fl: "R", op: 1, components: []},
    "Shadow Essence": {ts: tsDis, fl: "S", op: 1, components: []},
    "Silver Egg": {ts: tsDis, fl: "S", op: 1, components: []},
    "Silver Glitter": {ts: tsDis, fl: "S", op: 1, components: []},
    "Simple Dust": {ts: tsDis, fl: "S", op: 1, components: []},
    "Spectral Powder": {ts: tsDis, fl: "S", op: 1, components: []},
    "Stardust": {ts: tsDis, fl: "S", op: 1, components: []},
    "Strange Dust": {ts: tsDis, fl: "S", op: 1, components: []},
    "Sunfire Dust": {ts: tsDis, fl: "S", op: 1, components: []},
    "Sunlight Dust": {ts: tsDis, fl: "S", op: 1, components: []},
    "Void Powder": {ts: tsDis, fl: "V", op: 1, components: []},
    // enchanting
    "Arms Enchant (Common)": {ts: tsEnc, fl: "A", op: 1, components: [
        {compName: "Lunar Dust", ip: 3},
    ]},
    "Arms Enchant (Rare+) {003}": {ts: tsEnc, fl: "A", op: 1, components: [
        {compName: "Simple Dust", ip: 4},
        {compName: "Arcane Powder", ip: 2},
        {compName: "Mystic Essence", ip: 2},
    ]},
    "Arms Enchant (Rare+) {022}": {ts: tsEnc, fl: "A", op: 1, components: [
        {compName: "Sunfire Dust", ip: 1},
        {compName: "Spectral Powder", ip: 1},
        {compName: "Spawn Moonlight Orb", ip: 1},
    ]},
    "Boots Enchant (Rare+)": {ts: tsEnc, fl: "B", op: 1, components: [
        {compName: "Sunfire Dust", ip: 3},
        {compName: "Spawn Moonlight Orb", ip: 1},
    ]},
    "Chest Enchant (Rare+)": {ts: tsEnc, fl: "C", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Arcane Powder", ip: 2},
        {compName: "Mystic Essence", ip: 2},
    ]},
    "Chest Enchant (Superior)": {ts: tsEnc, fl: "C", op: 1, components: [
        {compName: "Lunar Dust", ip: 3},
        {compName: "Magma Powder", ip: 2},
    ]},
    "Common Leg Enchant": {ts: tsEnc, fl: "C", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
    ]},
    "Common Random Enchant {001}": {ts: tsEnc, fl: "C", op: 1, components: [
        {compName: "Simple Dust", ip: 4},
    ]},
    "Common Random Enchant {006}": {ts: tsEnc, fl: "C", op: 1, components: [
        {compName: "Simple Dust", ip: 4},
    ]},
    "Common Shield Enchant": {ts: tsEnc, fl: "C", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
    ]},
    "Common Weapon Enchant": {ts: tsEnc, fl: "C", op: 1, components: [
        {compName: "Simple Dust", ip: 4},
    ]},
    "De Icer": {ts: tsEnc, fl: "D", op: 1, components: [
        {compName: "Mystic Salt", ip: 8},
    ]},
    "Enchant Arms (Superior+) {030}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Nightfall Dust", ip: 5},
        {compName: "Void Powder", ip: 2},
    ]},
    "Enchant Arms (Superior+) {040}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunlight Dust", ip: 5},
        {compName: "Elemental Powder", ip: 2},
    ]},
    "Enchant Arms (Superior+) {051}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Phantom Dust", ip: 5},
        {compName: "Permafrost Powder", ip: 2},
    ]},
    "Enchant Arms (Superior+) {071}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Strange Dust", ip: 5},
        {compName: "Radiant Powder", ip: 2},
    ]},
    "Enchant Arms (Superior+) {101}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Stardust", ip: 5},
        {compName: "Purity Powder", ip: 2},
    ]},
    "Enchant Arms (Superior+) {131}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Cloud Wisp", ip: 5},
        {compName: "Chimera Powder", ip: 2},
    ]},
    "Enchant Chest (Common+)": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunfire Dust", ip: 5},
        {compName: "Spectral Powder", ip: 1},
    ]},
    "Enchant Chest (Rare+)": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Void Powder", ip: 2},
        {compName: "Spawn Chaos Orb", ip: 1},
    ]},
    "Enchant Chest (Superior+) {040}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunlight Dust", ip: 8},
    ]},
    "Enchant Chest (Superior+) {051}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Phantom Dust", ip: 8},
    ]},
    "Enchant Chest (Superior+) {071}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Strange Dust", ip: 8},
    ]},
    "Enchant Chest (Superior+) {101}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Stardust", ip: 8},
    ]},
    "Enchant Chest (Superior+) {131}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Cloud Wisp", ip: 8},
    ]},
    "Enchant Feet (Common+) {030}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Nightfall Dust", ip: 4},
        {compName: "Void Powder", ip: 1},
    ]},
    "Enchant Feet (Common+) {040}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunlight Dust", ip: 4},
        {compName: "Elemental Powder", ip: 1},
    ]},
    "Enchant Feet (Rare+) {051}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Phantom Dust", ip: 3},
        {compName: "Spawn Anarchy Orb", ip: 1},
    ]},
    "Enchant Feet (Rare+) {071}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Strange Dust", ip: 3},
        {compName: "Spawn Ethereal Orb", ip: 1},
    ]},
    "Enchant Feet (Rare+) {101}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Stardust", ip: 3},
        {compName: "Spawn Pure Orb", ip: 1},
    ]},
    "Enchant Feet (Rare+) {131}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Cloud Wisp", ip: 3},
        {compName: "Spawn Cloud Orb", ip: 1},
    ]},
    "Enchant Hands (Common+) {030}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Nightfall Dust", ip: 4},
        {compName: "Void Powder", ip: 1},
    ]},
    "Enchant Hands (Common+) {051}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Phantom Dust", ip: 4},
        {compName: "Permafrost Powder", ip: 1},
    ]},
    "Enchant Hands (Common+) {071}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Strange Dust", ip: 4},
        {compName: "Radiant Powder", ip: 1},
    ]},
    "Enchant Hands (Common+) {101}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Stardust", ip: 4},
        {compName: "Purity Powder", ip: 1},
    ]},
    "Enchant Hands (Common+) {131}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Cloud Wisp", ip: 1},
        {compName: "Chimera Powder", ip: 1},
    ]},
    "Enchant Hands (Rare+)": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunlight Dust", ip: 3},
        {compName: "Spawn Insanity Orb", ip: 1},
    ]},
    "Enchant Hands (Superior+)": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunfire Dust", ip: 3},
        {compName: "Spectral Powder", ip: 3},
    ]},
    "Enchant Head (Rare+) {030}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Chaos Essence", ip: 1},
        {compName: "Spawn Chaos Orb", ip: 1},
    ]},
    "Enchant Head (Rare+) {040}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Insanity Essence", ip: 1},
        {compName: "Spawn Insanity Orb", ip: 1},
    ]},
    "Enchant Head (Rare+) {051}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Anarchy Essence", ip: 1},
        {compName: "Spawn Anarchy Orb", ip: 1},
    ]},
    "Enchant Head (Rare+) {071}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Ethereal Essence", ip: 1},
        {compName: "Spawn Ethereal Orb", ip: 1},
    ]},
    "Enchant Head (Rare+) {101}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Ethereal Glow", ip: 1},
        {compName: "Spawn Pure Orb", ip: 1},
    ]},
    "Enchant Head (Rare+) {131}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Fallen Dust", ip: 1},
        {compName: "Spawn Cloud Orb", ip: 1},
    ]},
    "Enchant Legs (Common+) {051}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Phantom Dust", ip: 4},
        {compName: "Permafrost Powder", ip: 1},
    ]},
    "Enchant Legs (Common+) {071}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Strange Dust", ip: 4},
        {compName: "Radiant Powder", ip: 1},
    ]},
    "Enchant Legs (Common+) {101}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Stardust", ip: 4},
        {compName: "Purity Powder", ip: 1},
    ]},
    "Enchant Legs (Common+) {131}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Cloud Wisp", ip: 4},
        {compName: "Chimera Powder", ip: 1},
    ]},
    "Enchant Legs (Rare+)": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Elemental Powder", ip: 2},
        {compName: "Spawn Insanity Orb", ip: 1},
    ]},
    "Enchant Legs (Superior+)": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Nightfall Dust", ip: 8},
    ]},
    "Enchant Shield (Common+) {020}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunfire Dust", ip: 3},
        {compName: "Spectral Powder", ip: 2},
    ]},
    "Enchant Shield (Common+) {040}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunlight Dust", ip: 4},
        {compName: "Elemental Powder", ip: 1},
    ]},
    "Enchant Shield (Rare+) {030}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Nightfall Dust", ip: 3},
        {compName: "Spawn Chaos Orb", ip: 1},
    ]},
    "Enchant Shield (Rare+) {051}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Permafrost Powder", ip: 2},
        {compName: "Spawn Anarchy Orb", ip: 1},
    ]},
    "Enchant Shield (Rare+) {071}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Radiant Powder", ip: 2},
        {compName: "Spawn Ethereal Orb", ip: 1},
    ]},
    "Enchant Shield (Rare+) {101}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Purity Powder", ip: 2},
        {compName: "Spawn Pure Orb", ip: 1},
    ]},
    "Enchant Shield (Rare+) {131}": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Chimera Powder", ip: 2},
        {compName: "Spawn Cloud Orb", ip: 1},
    ]},
    "Enchant of Protection": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Powder of Protect", ip: 5},
    ]},
    "Enchanted Agate": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Arcane Powder", ip: 3},
        {compName: "Polish Large Agate", ip: 1},
    ]},
    "Enchanted Amber": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunfire Dust", ip: 4},
        {compName: "Spectral Powder", ip: 4},
        {compName: "Polish Large Amber", ip: 1},
    ]},
    "Enchanted Amethyst": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Nightfall Dust", ip: 4},
        {compName: "Void Powder", ip: 3},
        {compName: "Polish Large Amethyst", ip: 1},
    ]},
    "Enchanted Aquamarine": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Strange Dust", ip: 4},
        {compName: "Radiant Powder", ip: 3},
        {compName: "Polish Large Aquamarine", ip: 1},
    ]},
    "Enchanted Bloodstone Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Bloodstone Key", ip: 3},
        {compName: "Spectral Powder", ip: 4},
        {compName: "Moonlight Essence", ip: 2},
    ]},
    "Enchanted Candle": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Candle", ip: 1},
        {compName: "Mystic Essence", ip: 1},
    ]},
    "Enchanted Citrine": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Lunar Dust", ip: 2},
        {compName: "Magma Powder", ip: 2},
        {compName: "Polish Large Citrine", ip: 1},
    ]},
    "Enchanted Crystal Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Crystal Key", ip: 3},
        {compName: "Permafrost Powder", ip: 4},
        {compName: "Anarchy Essence", ip: 2},
    ]},
    "Enchanted Dark Golemite Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Radiant Powder", ip: 4},
        {compName: "Ethereal Essence", ip: 2},
        {compName: "Cosmos Essence", ip: 1},
        {compName: "Dark Golemite Key", ip: 3},
    ]},
    "Enchanted Flowers": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Mystical Flower", ip: 8},
    ]},
    "Enchanted Garnet": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunfire Dust", ip: 2},
        {compName: "Spectral Powder", ip: 2},
        {compName: "Polished Large Garnet", ip: 1},
    ]},
    "Enchanted Gem": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Simple Dust", ip: 2},
        {compName: "Arcane Powder", ip: 2},
        {compName: "Polish Large Gem", ip: 1},
    ]},
    "Enchanted Golemite Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Golemite Key", ip: 3},
        {compName: "Ancient Essence", ip: 2},
        {compName: "Ancient Reference", ip: 2},
    ]},
    "Enchanted Malachite": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Lunar Dust", ip: 4},
        {compName: "Magma Powder", ip: 4},
        {compName: "Polish Large Malachite", ip: 1},
    ]},
    "Enchanted Manganese Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Manganese Key", ip: 3},
        {compName: "Purity Powder", ip: 4},
        {compName: "Ethereal Glow", ip: 2},
        {compName: "Divinity Essence", ip: 1},
    ]},
    "Enchanted Melanite": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Phantom Dust", ip: 4},
        {compName: "Permafrost Powder", ip: 3},
        {compName: "Polish Large Melanite", ip: 1},
    ]},
    "Enchanted Mercury": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Cloud Wisp", ip: 4},
        {compName: "Chimera Powder", ip: 3},
        {compName: "Polish Large Mercury", ip: 1},
    ]},
    "Enchanted Mithril Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Mithril Key", ip: 3},
        {compName: "Radiant Powder", ip: 4},
        {compName: "Ethereal Essence", ip: 2},
        {compName: "Cosmos Essence", ip: 1},
    ]},
    "Enchanted Obsidian Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Obsidian Key", ip: 3},
        {compName: "Purity Powder", ip: 4},
        {compName: "Ethereal Glow", ip: 2},
    ]},
    "Enchanted Peridot": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Stardust", ip: 4},
        {compName: "Purity Powder", ip: 3},
        {compName: "Polish Large Peridot", ip: 1},
    ]},
    "Enchanted Pyrestone Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Pyrestone Key", ip: 3},
        {compName: "Radiant Powder", ip: 4},
        {compName: "Ethereal Essence", ip: 2},
    ]},
    "Enchanted Silver Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Silver Key", ip: 3},
        {compName: "Chimera Powder", ip: 4},
        {compName: "Fallen Dust", ip: 2},
    ]},
    "Enchanted Sun Opal": {ts: tsEnc, fl: "E", op: 1, components: [
        {compName: "Sunlight Dust", ip: 4},
        {compName: "Elemental Powder", ip: 3},
        {compName: "Polish Large Sun Opal", ip: 1},
    ]},
    "Enchanted Titanium Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Permafrost Powder", ip: 4},
        {compName: "Anarchy Essence", ip: 2},
        {compName: "Titanium Key", ip: 3},
        {compName: "Cosmos Essence", ip: 1},
    ]},
    "Enchanted Tourmaline Key": {ts: tsEnc, fl: "E", op: 3, components: [
        {compName: "Chimera Powder", ip: 4},
        {compName: "Fallen Dust", ip: 2},
        {compName: "Tourmaline Key", ip: 3},
        {compName: "Demonic Essence", ip: 1},
    ]},
    "Feet Enchant (Superior+)": {ts: tsEnc, fl: "F", op: 1, components: [
        {compName: "Lunar Dust", ip: 3},
        {compName: "Magma Powder", ip: 1},
        {compName: "Shadow Essence", ip: 3},
    ]},
    "Hands Enchant (Rare+)": {ts: tsEnc, fl: "H", op: 1, components: [
        {compName: "Simple Dust", ip: 4},
        {compName: "Arcane Powder", ip: 2},
        {compName: "Mystic Essence", ip: 2},
    ]},
    "Hands Enchant (Superior+)": {ts: tsEnc, fl: "H", op: 1, components: [
        {compName: "Lunar Dust", ip: 2},
        {compName: "Magma Powder", ip: 4},
        {compName: "Shadow Essence", ip: 1},
    ]},
    "Head Enchant (Common)": {ts: tsEnc, fl: "H", op: 1, components: [
        {compName: "Lunar Dust", ip: 3},
    ]},
    "Head Enchant (Rare+)": {ts: tsEnc, fl: "H", op: 1, components: [
        {compName: "Spectral Powder", ip: 2},
        {compName: "Spawn Moonlight Orb", ip: 1},
    ]},
    "Helm Enchant (Epic+)": {ts: tsEnc, fl: "H", op: 1, components: [
        {compName: "Spawn Mystic Orb", ip: 2},
    ]},
    "Leg Enchant (Rare+)": {ts: tsEnc, fl: "L", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Arcane Powder", ip: 1},
        {compName: "Mystic Essence", ip: 2},
    ]},
    "Leg Enchant (Superior)": {ts: tsEnc, fl: "L", op: 1, components: [
        {compName: "Lunar Dust", ip: 3},
        {compName: "Magma Powder", ip: 2},
    ]},
    "Protection Barrier": {ts: tsEnc, fl: "P", op: 1, components: [
        {compName: "Barrier Powder", ip: 8},
    ]},
    "Random Enchant (Common+)": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Lunar Dust", ip: 2},
        {compName: "Magma Powder", ip: 1},
        {compName: "Shadow Essence", ip: 1},
    ]},
    "Random Enchant (Epic+) {005}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Mystic Orb", ip: 4},
    ]},
    "Random Enchant (Epic+) {012}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Shadow Orb", ip: 4},
    ]},
    "Random Enchant (Epic+) {022}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Moonlight Orb", ip: 4},
    ]},
    "Random Enchant (Epic+) {030}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Chaos Orb", ip: 4},
    ]},
    "Random Enchant (Epic+) {040}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Insanity Orb", ip: 4},
    ]},
    "Random Enchant (Epic+) {051}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Anarchy Orb", ip: 4},
    ]},
    "Random Enchant (Epic+) {071}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Ethereal Orb", ip: 4},
    ]},
    "Random Enchant (Epic+) {101}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Pure Orb", ip: 4},
    ]},
    "Random Enchant (Epic+) {131}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Cloud Orb", ip: 4},
    ]},
    "Random Enchant (Rare+) {001}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Mystic Orb", ip: 1},
    ]},
    "Random Enchant (Rare+) {010}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Spawn Shadow Orb", ip: 2},
    ]},
    "Random Enchant (Superior+) {020}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Sunfire Dust", ip: 2},
        {compName: "Spectral Powder", ip: 2},
        {compName: "Moonlight Essence", ip: 1},
    ]},
    "Random Enchant (Superior+) {030}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Nightfall Dust", ip: 2},
        {compName: "Void Powder", ip: 2},
        {compName: "Chaos Essence", ip: 2},
    ]},
    "Random Enchant (Superior+) {040}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Sunlight Dust", ip: 2},
        {compName: "Elemental Powder", ip: 2},
        {compName: "Insanity Essence", ip: 2},
    ]},
    "Random Enchant (Superior+) {051}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Phantom Dust", ip: 2},
        {compName: "Permafrost Powder", ip: 2},
        {compName: "Anarchy Essence", ip: 2},
    ]},
    "Random Enchant (Superior+) {071}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Strange Dust", ip: 2},
        {compName: "Radiant Powder", ip: 2},
        {compName: "Ethereal Essence", ip: 2},
    ]},
    "Random Enchant (Superior+) {101}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Stardust", ip: 2},
        {compName: "Purity Powder", ip: 2},
        {compName: "Ethereal Glow", ip: 2},
    ]},
    "Random Enchant (Superior+) {131}": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Cloud Wisp", ip: 2},
        {compName: "Chimera Powder", ip: 2},
        {compName: "Fallen Dust", ip: 2},
    ]},
    "Random Superior Enchant": {ts: tsEnc, fl: "R", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Arcane Powder", ip: 3},
    ]},
    "Shield Enchant (Rare+)": {ts: tsEnc, fl: "S", op: 1, components: [
        {compName: "Lunar Dust", ip: 2},
        {compName: "Magma Powder", ip: 1},
        {compName: "Spawn Shadow Orb", ip: 1},
    ]},
    "Spawn Anarchy Orb": {ts: tsEnc, fl: "S", op: 1, display: "Anarchy Orb", components: [
        {compName: "Phantom Dust", ip: 5},
        {compName: "Permafrost Powder", ip: 4},
        {compName: "Anarchy Essence", ip: 3},
        {compName: "Melanite", ip: 1},
    ]},
    "Spawn Chaos Orb": {ts: tsEnc, fl: "S", op: 1, display: "Chaos Orb", components: [
        {compName: "Nightfall Dust", ip: 5},
        {compName: "Void Powder", ip: 3},
        {compName: "Amethyst", ip: 1},
        {compName: "Chaos Essence", ip: 3},
    ]},
    "Spawn Cloud Orb": {ts: tsEnc, fl: "S", op: 1, display: "Cloud Orb", components: [
        {compName: "Cloud Wisp", ip: 5},
        {compName: "Chimera Powder", ip: 4},
        {compName: "Fallen Dust", ip: 3},
        {compName: "Mercury", ip: 1},
    ]},
    "Spawn Ethereal Orb": {ts: tsEnc, fl: "S", op: 1, display: "Ethereal Orb", components: [
        {compName: "Strange Dust", ip: 5},
        {compName: "Radiant Powder", ip: 4},
        {compName: "Aquamarine", ip: 1},
        {compName: "Ethereal Essence", ip: 3},
    ]},
    "Spawn Insanity Orb": {ts: tsEnc, fl: "S", op: 1, display: "Insanity Orb", components: [
        {compName: "Sunlight Dust", ip: 5},
        {compName: "Elemental Powder", ip: 4},
        {compName: "Insanity Essence", ip: 3},
        {compName: "Sun Opal", ip: 1},
    ]},
    "Spawn Moonlight Orb": {ts: tsEnc, fl: "S", op: 1, display: "Moonlight Orb", components: [
        {compName: "Sunfire Dust", ip: 5},
        {compName: "Spectral Powder", ip: 3},
        {compName: "Amber", ip: 1},
        {compName: "Moonlight Essence", ip: 3},
    ]},
    "Spawn Mystic Orb": {ts: tsEnc, fl: "S", op: 1, display: "Mystic Orb", components: [
        {compName: "Simple Dust", ip: 5},
        {compName: "Arcane Powder", ip: 3},
        {compName: "Agate", ip: 1},
        {compName: "Mystic Essence", ip: 3},
    ]},
    "Spawn Pure Orb": {ts: tsEnc, fl: "S", op: 1, display: "Pure Orb", components: [
        {compName: "Stardust", ip: 5},
        {compName: "Purity Powder", ip: 4},
        {compName: "Ethereal Glow", ip: 1},
        {compName: "Peridot", ip: 3},
    ]},
    "Spawn Shadow Orb": {ts: tsEnc, fl: "S", op: 1, display: "Shadow Orb", components: [
        {compName: "Lunar Dust", ip: 5},
        {compName: "Magma Powder", ip: 3},
        {compName: "Malachite", ip: 1},
        {compName: "Shadow Essence", ip: 3},
    ]},
    "Superior Arms Enchant": {ts: tsEnc, fl: "S", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Arcane Powder", ip: 2},
    ]},
    "Superior Feet Enchant": {ts: tsEnc, fl: "S", op: 1, components: [
        {compName: "Simple Dust", ip: 2},
        {compName: "Arcane Powder", ip: 1},
    ]},
    "Superior Hands Enchant": {ts: tsEnc, fl: "S", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Arcane Powder", ip: 2},
    ]},
    "Superior Shield Enchant": {ts: tsEnc, fl: "S", op: 1, components: [
        {compName: "Simple Dust", ip: 3},
        {compName: "Arcane Powder", ip: 3},
    ]},
    "Weapon Enchant (Epic+)": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Spawn Moonlight Orb", ip: 3},
    ]},
    "Weapon Enchant (Rare) {012}": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Spawn Shadow Orb", ip: 1},
    ]},
    "Weapon Enchant (Rare+) {005}": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Arcane Powder", ip: 4},
        {compName: "Mystic Essence", ip: 4},
    ]},
    "Weapon Enchant (Rare+) {030}": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Void Powder", ip: 2},
        {compName: "Chaos Essence", ip: 1},
        {compName: "Spawn Chaos Orb", ip: 1},
    ]},
    "Weapon Enchant (Rare+) {040}": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Spawn Insanity Orb", ip: 2},
    ]},
    "Weapon Enchant (Rare+) {051}": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Spawn Anarchy Orb", ip: 2},
    ]},
    "Weapon Enchant (Rare+) {071}": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Spawn Ethereal Orb", ip: 2},
    ]},
    "Weapon Enchant (Rare+) {101}": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Spawn Pure Orb", ip: 2},
    ]},
    "Weapon Enchant (Rare+) {131}": {ts: tsEnc, fl: "W", op: 1, components: [
        {compName: "Spawn Cloud Orb", ip: 2},
    ]},
    // fishing
    "Angelfish": {ts: tsFis, fl: "A", op: 1, components: []},
    "Arapaima": {ts: tsFis, fl: "A", op: 1, components: []},
    "Barracuda": {ts: tsFis, fl: "B", op: 1, components: []},
    "Bass": {ts: tsFis, fl: "B", op: 1, components: []},
    "Blackfish": {ts: tsFis, fl: "B", op: 1, components: []},
    "Blue Easter Egg": {ts: tsFis, fl: "B", op: 1, components: []},
    "Blue Party Seaweed": {ts: tsFis, fl: "B", op: 1, components: []},
    "Catfish": {ts: tsFis, fl: "C", op: 1, components: []},
    "Cinnamon Seaweed": {ts: tsFis, fl: "C", op: 1, components: []},
    "Cocoa Fish": {ts: tsFis, fl: "C", op: 1, components: []},
    "Cod": {ts: tsFis, fl: "C", op: 1, components: []},
    "Crawfish": {ts: tsFis, fl: "C", op: 1, components: []},
    "Cuttlefish": {ts: tsFis, fl: "C", op: 1, components: []},
    "Delvrak": {ts: tsFis, fl: "D", op: 1, components: []},
    "Dragonfish": {ts: tsFis, fl: "D", op: 1, components: []},
    "Eel": {ts: tsFis, fl: "E", op: 1, components: []},
    "Eelgrass": {ts: tsFis, fl: "E", op: 1, components: []},
    "Essense of Joy": {ts: tsFis, fl: "E", op: 1, components: []},
    "Fangtooth": {ts: tsFis, fl: "F", op: 1, components: []},
    "Featherback": {ts: tsFis, fl: "F", op: 1, components: []},
    "Fire Goby": {ts: tsFis, fl: "F", op: 1, components: []},
    "Fish Blood": {ts: tsFis, fl: "F", op: 1, components: []},
    "Fishing Tools": {ts: tsFis, fl: "F", op: 1, components: []},
    "Flathead": {ts: tsFis, fl: "F", op: 1, components: []},
    "Flounder": {ts: tsFis, fl: "F", op: 1, components: []},
    "Fresh Water": {ts: tsFis, fl: "F", op: 1, components: []},
    "Ghostfish": {ts: tsFis, fl: "G", op: 1, components: []},
    "Giant Trout": {ts: tsFis, fl: "G", op: 1, components: []},
    "Gilgrash": {ts: tsFis, fl: "G", op: 1, components: []},
    "Grey Salmon": {ts: tsFis, fl: "G", op: 1, components: []},
    "Haddock": {ts: tsFis, fl: "H", op: 1, components: []},
    "Halibut": {ts: tsFis, fl: "H", op: 1, components: []},
    "Hawkfin": {ts: tsFis, fl: "H", op: 1, components: []},
    "Heart Candy - Love Bug": {ts: tsFis, fl: "H", op: 1, components: []},
    "Herring": {ts: tsFis, fl: "H", op: 1, components: []},
    "Icefish": {ts: tsFis, fl: "I", op: 1, components: []},
    "Knifefish": {ts: tsFis, fl: "K", op: 1, components: []},
    "Lancetail": {ts: tsFis, fl: "L", op: 1, components: []},
    "Lightning Eel": {ts: tsFis, fl: "L", op: 1, components: []},
    "Lime Syrup": {ts: tsFis, fl: "L", op: 1, components: []},
    "Lionfish": {ts: tsFis, fl: "L", op: 1, components: []},
    "Mackerel": {ts: tsFis, fl: "M", op: 1, components: []},
    "Major Arcana Card": {ts: tsFis, fl: "M", op: 1, components: []},
    "Muskellunge": {ts: tsFis, fl: "M", op: 1, components: []},
    "Nettlefish": {ts: tsFis, fl: "N", op: 1, components: []},
    "Nile Tilapia": {ts: tsFis, fl: "N", op: 1, components: []},
    "Pearl Egg": {ts: tsFis, fl: "P", op: 1, components: []},
    "Perch": {ts: tsFis, fl: "P", op: 1, components: []},
    "Phantomfin": {ts: tsFis, fl: "P", op: 1, components: []},
    "Pickerel": {ts: tsFis, fl: "P", op: 1, components: []},
    "Pike": {ts: tsFis, fl: "P", op: 1, components: []},
    "Pile of Fish": {ts: tsFis, fl: "P", op: 1, components: []},
    "Pink Party Seaweed": {ts: tsFis, fl: "P", op: 1, components: []},
    "Pink Strawberry": {ts: tsFis, fl: "P", op: 1, components: []},
    "Pink Streamer": {ts: tsFis, fl: "P", op: 1, components: []},
    "Rockfish": {ts: tsFis, fl: "R", op: 1, components: []},
    "Salmon": {ts: tsFis, fl: "S", op: 1, components: []},
    "Salt": {ts: tsFis, fl: "S", op: 1, components: []},
    "Shadow Fish": {ts: tsFis, fl: "S", op: 1, components: []},
    "Shrimp": {ts: tsFis, fl: "S", op: 1, components: []},
    "Spiny Piranha": {ts: tsFis, fl: "S", op: 1, components: []},
    "Sponge": {ts: tsFis, fl: "S", op: 1, components: []},
    "Squid": {ts: tsFis, fl: "S", op: 1, components: []},
    "Sunray": {ts: tsFis, fl: "S", op: 1, components: []},
    "Terrorfish": {ts: tsFis, fl: "T", op: 1, components: []},
    "Tigerfish": {ts: tsFis, fl: "T", op: 1, components: []},
    "Trout": {ts: tsFis, fl: "T", op: 1, components: []},
    "Trunkfish": {ts: tsFis, fl: "T", op: 1, components: []},
    "Water Cherry": {ts: tsFis, fl: "W", op: 1, components: []},
    "Water": {ts: tsFis, fl: "W", op: 1, components: []},
    "Wels Catfish": {ts: tsFis, fl: "W", op: 1, components: []},
    "Whitefish": {ts: tsFis, fl: "W", op: 1, components: []},
    "Yellow Party Seaweed": {ts: tsFis, fl: "Y", op: 1, components: []},
    "Yellow Striped Easter Egg": {ts: tsFis, fl: "Y", op: 1, components: []},
    // gathering
    "A Touch of Jasmine": {ts: tsGat, fl: "A", op: 1, components: []},
    "Ancientbloom": {ts: tsGat, fl: "A", op: 1, components: []},
    "Angelflower": {ts: tsGat, fl: "A", op: 1, components: []},
    "Apple": {ts: tsGat, fl: "A", op: 1, components: []},
    "Apricot": {ts: tsGat, fl: "A", op: 1, components: []},
    "Arcaneleaf": {ts: tsGat, fl: "A", op: 1, components: []},
    "Banana": {ts: tsGat, fl: "B", op: 1, components: []},
    "Black Lotus": {ts: tsGat, fl: "B", op: 1, components: []},
    "Blood Berry": {ts: tsGat, fl: "B", op: 1, components: []},
    "Bone Remains": {ts: tsGat, fl: "B", op: 1, components: []},
    "Cacao Bean": {ts: tsGat, fl: "C", op: 1, components: []},
    "Cake Bites": {ts: tsGat, fl: "C", op: 1, components: []},
    "Cherry Syrup": {ts: tsGat, fl: "C", op: 1, components: []},
    "Cherry": {ts: tsGat, fl: "C", op: 1, components: []},
    "Chick Grain": {ts: tsGat, fl: "C", op: 1, components: []},
    "Corpsepoppy": {ts: tsGat, fl: "C", op: 1, components: []},
    "Cotton": {ts: tsGat, fl: "C", op: 1, components: []},
    "Crawlingroot": {ts: tsGat, fl: "C", op: 1, components: []},
    "Dark Cotton": {ts: tsGat, fl: "D", op: 1, components: []},
    "Dark Honey": {ts: tsGat, fl: "D", op: 1, components: []},
    "Devilpetal": {ts: tsGat, fl: "D", op: 1, components: []},
    "Devilweed": {ts: tsGat, fl: "D", op: 1, components: []},
    "Dogweed": {ts: tsGat, fl: "D", op: 1, components: []},
    "Dragon's Tongue": {ts: tsGat, fl: "D", op: 1, components: []},
    "Dragontear": {ts: tsGat, fl: "D", op: 1, components: []},
    "Eagletear": {ts: tsGat, fl: "E", op: 1, components: []},
    "Easter Grass": {ts: tsGat, fl: "E", op: 1, components: []},
    "Egg": {ts: tsGat, fl: "E", op: 1, components: []},
    "Emerald Egg": {ts: tsGat, fl: "E", op: 1, components: []},
    "Exploding Berries": {ts: tsGat, fl: "E", op: 1, components: []},
    "Fallen Leaves": {ts: tsGat, fl: "F", op: 1, components: []},
    "Flame Lotus": {ts: tsGat, fl: "F", op: 1, components: []},
    "Flax": {ts: tsGat, fl: "F", op: 1, components: []},
    "Foxflower": {ts: tsGat, fl: "F", op: 1, components: []},
    "Frostweed": {ts: tsGat, fl: "F", op: 1, components: []},
    "Ghostshroom": {ts: tsGat, fl: "G", op: 1, components: []},
    "Ghoulweed": {ts: tsGat, fl: "G", op: 1, components: []},
    "Giant Egg": {ts: tsGat, fl: "G", op: 1, components: []},
    "Goldenbush": {ts: tsGat, fl: "G", op: 1, components: []},
    "Golembloom": {ts: tsGat, fl: "G", op: 1, components: []},
    "Gorethistle": {ts: tsGat, fl: "G", op: 1, components: []},
    "Grape": {ts: tsGat, fl: "G", op: 1, components: []},
    "Grey Berries": {ts: tsGat, fl: "G", op: 1, components: []},
    "Heart Candy - Hug Me": {ts: tsGat, fl: "H", op: 1, components: []},
    "Holiday Spices": {ts: tsGat, fl: "H", op: 1, components: []},
    "Honey Mushroom": {ts: tsGat, fl: "H", op: 1, components: []},
    "Honey": {ts: tsGat, fl: "H", op: 1, components: []},
    "Horsetail": {ts: tsGat, fl: "H", op: 1, components: []},
    "Icebloom": {ts: tsGat, fl: "I", op: 1, components: []},
    "Iron Egg": {ts: tsGat, fl: "I", op: 1, components: []},
    "Kingsbloom": {ts: tsGat, fl: "K", op: 1, components: []},
    "Ladyslipper": {ts: tsGat, fl: "L", op: 1, components: []},
    "Large Egg": {ts: tsGat, fl: "L", op: 1, components: []},
    "Liontail": {ts: tsGat, fl: "L", op: 1, components: []},
    "Mango": {ts: tsGat, fl: "M", op: 1, components: []},
    "Maple Syrup": {ts: tsGat, fl: "M", op: 1, components: []},
    "Medusaroot": {ts: tsGat, fl: "M", op: 1, components: []},
    "Midnight Berry": {ts: tsGat, fl: "M", op: 1, components: []},
    "Milkweed": {ts: tsGat, fl: "M", op: 1, components: []},
    "Minor Arcana: Cup": {ts: tsGat, fl: "M", op: 1, components: []},
    "Moonbeard": {ts: tsGat, fl: "M", op: 1, components: []},
    "Nightshade": {ts: tsGat, fl: "N", op: 1, components: []},
    "Nightwolf": {ts: tsGat, fl: "N", op: 1, components: []},
    "Orange": {ts: tsGat, fl: "O", op: 1, components: []},
    "Orchid": {ts: tsGat, fl: "O", op: 1, components: []},
    "Peach": {ts: tsGat, fl: "P", op: 1, components: []},
    "Pear": {ts: tsGat, fl: "P", op: 1, components: []},
    "Peppermint": {ts: tsGat, fl: "P", op: 1, components: []},
    "Phantomlily": {ts: tsGat, fl: "P", op: 1, components: []},
    "Pink Striped Easter Egg": {ts: tsGat, fl: "P", op: 1, components: []},
    "Pixieroot": {ts: tsGat, fl: "P", op: 1, components: []},
    "Poinsettia": {ts: tsGat, fl: "P", op: 1, components: []},
    "Reindeer Grass": {ts: tsGat, fl: "R", op: 1, components: []},
    "Rosemary": {ts: tsGat, fl: "R", op: 1, components: []},
    "Sandcreeper": {ts: tsGat, fl: "S", op: 1, components: []},
    "Scallions": {ts: tsGat, fl: "S", op: 1, components: []},
    "Serpent Vine": {ts: tsGat, fl: "S", op: 1, components: []},
    "Silkweed": {ts: tsGat, fl: "S", op: 1, components: []},
    "Snow": {ts: tsGat, fl: "S", op: 1, components: []},
    "Speckled Egg": {ts: tsGat, fl: "S", op: 1, components: []},
    "Spiderfoot": {ts: tsGat, fl: "S", op: 1, components: []},
    "Spiritherb": {ts: tsGat, fl: "S", op: 1, components: []},
    "Sticky Mud": {ts: tsGat, fl: "S", op: 1, components: []},
    "Stonesilk Lily": {ts: tsGat, fl: "S", op: 1, components: []},
    "Sunlightbloom": {ts: tsGat, fl: "S", op: 1, components: []},
    "Tea Leaves": {ts: tsGat, fl: "T", op: 1, components: []},
    "Tiger Lily": {ts: tsGat, fl: "T", op: 1, components: []},
    "Trail Mix": {ts: tsGat, fl: "T", op: 1, components: []},
    "Venomweed": {ts: tsGat, fl: "V", op: 1, components: []},
    "Vilegourd": {ts: tsGat, fl: "V", op: 1, components: []},
    "Waterberry": {ts: tsGat, fl: "W", op: 1, components: []},
    "Wild Garlic": {ts: tsGat, fl: "W", op: 1, components: []},
    "Wolfsbane": {ts: tsGat, fl: "W", op: 1, components: []},
    "Wraith Thorns": {ts: tsGat, fl: "W", op: 1, components: []},
    "Yellow Easter Egg": {ts: tsGat, fl: "Y", op: 1, components: []},
    // inscription
    "Aged Book": {ts: tsIns, fl: "A", op: 1, components: [
        {compName: "Aged Chapter", ip: 6},
        {compName: "Waterberry Ink", ip: 6},
    ]},
    "Aged Chapter": {ts: tsIns, fl: "A", op: 1, components: [
        {compName: "Aged Writings", ip: 7},
        {compName: "Waterberry Ink", ip: 2},
    ]},
    "Aged Reference Book": {ts: tsIns, fl: "A", op: 1, components: [
        {compName: "Aged Book", ip: 1},
        {compName: "Aged Reference", ip: 6},
        {compName: "Waterberry Ink", ip: 3},
    ]},
    "Ancient Elven Parchment": {ts: tsIns, fl: "A", op: 1, components: [
        {compName: "Elven Parchment", ip: 3},
    ]},
    "Basic Book": {ts: tsIns, fl: "B", op: 1, components: [
        {compName: "Basic Chapter", ip: 5},
        {compName: "Midnight Ink", ip: 3},
    ]},
    "Basic Chapter": {ts: tsIns, fl: "B", op: 1, components: [
        {compName: "Basic Writings", ip: 7},
        {compName: "Midnight Ink", ip: 2},
    ]},
    "Basic Formation": {ts: tsIns, fl: "B", op: 1, components: [
        {compName: "Minor Etching", ip: 5},
        {compName: "Basic Rune", ip: 3},
    ]},
    "Basic Markings": {ts: tsIns, fl: "B", op: 1, components: [
        {compName: "Basic Formation", ip: 1},
        {compName: "Superb Etching", ip: 1},
    ]},
    "Basic Reference Book": {ts: tsIns, fl: "B", op: 1, components: [
        {compName: "Basic Book", ip: 1},
        {compName: "Basic Reference", ip: 6},
        {compName: "Midnight Ink", ip: 2},
    ]},
    "Bone Book": {ts: tsIns, fl: "B", op: 1, components: [
        {compName: "Bone Chapter", ip: 6},
        {compName: "Serpent Ink", ip: 6},
    ]},
    "Bone Chapter": {ts: tsIns, fl: "B", op: 1, components: [
        {compName: "Bone Writings", ip: 7},
        {compName: "Serpent Ink", ip: 3},
    ]},
    "Book of Serenades": {ts: tsIns, fl: "B", op: 1, components: [
        {compName: "Romantic Music Sheet", ip: 6},
    ]},
    "Celestial Book": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Celestial Chapter", ip: 6},
        {compName: "Lotus Ink", ip: 6},
    ]},
    "Celestial Chapter": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Celestial Writings", ip: 7},
        {compName: "Lotus Ink", ip: 3},
    ]},
    "Celestial Reference {Book}": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Celestial Book", ip: 1},
        {compName: "Celestial Reference", ip: 6},
        {compName: "Lotus Ink", ip: 3},
    ]},
    "Chicken Bedding": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Golden Hay", ip: 8},
    ]},
    "Coarse Parchment": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Primitive Parchment", ip: 3},
    ]},
    "Complex Markings": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Complex Symbol", ip: 2},
        {compName: "Ancient Etching", ip: 2},
    ]},
    "Complex Symbol": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Complex Etching", ip: 6},
        {compName: "Pristine Rune", ip: 2},
    ]},
    "Cryptic Book": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Cryptic Chapter", ip: 6},
        {compName: "Venom Ink", ip: 6},
    ]},
    "Cryptic Chapter": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Cryptic Writings", ip: 7},
        {compName: "Venom Ink", ip: 3},
    ]},
    "Cryptic Reference {Book}": {ts: tsIns, fl: "C", op: 1, components: [
        {compName: "Cryptic Book", ip: 1},
        {compName: "Cryptic Reference", ip: 6},
        {compName: "Venom Ink", ip: 3},
    ]},
    "Dark Arcane Ink": {ts: tsIns, fl: "D", op: 10, components: [
        {compName: "Arcaneleaf", ip: 7},
        {compName: "Dark Honey", ip: 3},
        {compName: "Spiny Piranha", ip: 2},
    ]},
    "Deflect Mysterious Force": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Mysterious Powder", ip: 5},
    ]},
    "Delicate Parchment": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Elemental Parchment", ip: 3},
    ]},
    "Durability Rune (Common+) {015}": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Elemental Markings", ip: 2},
        {compName: "Waterberry Ink", ip: 8},
    ]},
    "Durability Rune (Superior+) {026}": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Complex Markings", ip: 3},
        {compName: "Dark Arcane Ink", ip: 10},
    ]},
    "Durability Rune (Superior+) {040}": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Elaborate Markings", ip: 3},
        {compName: "Ghoul Ink", ip: 10},
    ]},
    "Durability Rune (Superior+) {051}": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Refined Markings", ip: 3},
        {compName: "Venom Ink", ip: 10},
    ]},
    "Durability Rune (Superior+) {071}": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Lotus Ink", ip: 10},
        {compName: "Rightous Markings", ip: 3},
    ]},
    "Durability Rune (Superior+) {101}": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Serpent Ink", ip: 10},
        {compName: "Wyvern Markings", ip: 3},
    ]},
    "Durability Rune (Superior+) {131}": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Flame Ink", ip: 10},
        {compName: "Witik Markings", ip: 3},
    ]},
    "Durability Rune (common+) {001} ": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Midnight Ink", ip: 2},
        {compName: "Basic Markings", ip: 2},
    ]},
    "Durability Rune (common+) {008}": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Pixie Ink", ip: 6},
        {compName: "Primitive Markings", ip: 2},
    ]},
    "Dwarven Symbol": {ts: tsIns, fl: "D", op: 1, components: [
        {compName: "Dwarven Etching", ip: 6},
        {compName: "Frozen Rune", ip: 2},
    ]},
    "Elaborate Markings": {ts: tsIns, fl: "E", op: 1, components: [
        {compName: "Elaborate Symbol", ip: 2},
        {compName: "Ancient Etching", ip: 2},
    ]},
    "Elaborate Symbol": {ts: tsIns, fl: "E", op: 1, components: [
        {compName: "Elaborate Etching", ip: 6},
        {compName: "Primeval Rune", ip: 2},
    ]},
    "Elder Book": {ts: tsIns, fl: "E", op: 1, components: [
        {compName: "Elder Chapter", ip: 6},
        {compName: "Flame Ink", ip: 6},
    ]},
    "Elder Chapter": {ts: tsIns, fl: "E", op: 1, components: [
        {compName: "Elder Writings", ip: 7},
        {compName: "Flame Ink", ip: 3},
    ]},
    "Elder Ritual Shrine": {ts: tsIns, fl: "E", op: 1, components: [
        {compName: "Witik Etching", ip: 25},
        {compName: "Old Rites", ip: 25},
        {compName: "Temple Translations", ip: 25},
        {compName: "Ancient Witik Etching", ip: 25},
        {compName: "Elder Reference", ip: 75},
        {compName: "Lylin Carving", ip: 75},
        {compName: "Wyvern Book", ip: 75},
    ]},
    "Elemental Markings": {ts: tsIns, fl: "E", op: 1, components: [
        {compName: "Elemental Symbol", ip: 2},
        {compName: "Superb Etching", ip: 2},
    ]},
    "Elemental Symbol": {ts: tsIns, fl: "E", op: 1, components: [
        {compName: "Intricate Etching", ip: 6},
        {compName: "Elemental Rune", ip: 2},
    ]},
    "Exceptional Parchment": {ts: tsIns, fl: "E", op: 1, components: [
        {compName: "Immaculate Parchment", ip: 3},
    ]},
    "Flame Ink": {ts: tsIns, fl: "F", op: 10, components: [
        {compName: "Cherry", ip: 3},
        {compName: "Crawfish", ip: 2},
        {compName: "Flame Lotus", ip: 2},
    ]},
    "Ghoul Ink": {ts: tsIns, fl: "G", op: 10, components: [
        {compName: "Medusaroot", ip: 3},
        {compName: "Fangtooth", ip: 2},
        {compName: "Ghoulweed", ip: 2},
    ]},
    "Inscribed Adamantite Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Adamantite Staff Base", ip: 1},
        {compName: "Ghoul Ink", ip: 8},
    ]},
    "Inscribed Bloodstone Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Bloodstone Staff Base", ip: 1},
        {compName: "Dark Arcane Ink", ip: 4},
    ]},
    "Inscribed Chromium Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Chromium Staff Base", ip: 1},
        {compName: "Waterberry Ink", ip: 6},
    ]},
    "Inscribed Cobalt Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Cobalt Staff Base", ip: 1},
        {compName: "Waterberry Ink", ip: 4},
    ]},
    "Inscribed Copper Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Copper Staff Base", ip: 1},
        {compName: "Midnight Ink", ip: 2},
    ]},
    "Inscribed Iridium Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Iridium Staff Base", ip: 1},
        {compName: "Dark Arcane Ink", ip: 8},
    ]},
    "Inscribed Iron Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Iron Staff Base", ip: 1},
        {compName: "Pixie Ink", ip: 4},
    ]},
    "Inscribed Manganese Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Manganese Staff Base", ip: 1},
        {compName: "Serpent Ink", ip: 8},
    ]},
    "Inscribed Mithril Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Mithril Staff Base", ip: 1},
        {compName: "Lotus Ink", ip: 8},
    ]},
    "Inscribed Tin Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Midnight Ink", ip: 6},
        {compName: "Tin Staff Base", ip: 1},
    ]},
    "Inscribed Titanium Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Titanium Staff Base", ip: 1},
        {compName: "Venom Ink", ip: 8},
    ]},
    "Inscribed Tourmaline Staff": {ts: tsIns, fl: "I", op: 1, components: [
        {compName: "Tourmaline Staff Base", ip: 1},
        {compName: "Flame Ink", ip: 8},
    ]},
    "Lotus Ink": {ts: tsIns, fl: "L", op: 10, components: [
        {compName: "Tiger Lily Oil", ip: 3},
        {compName: "Nettlefish", ip: 2},
        {compName: "Black Lotus", ip: 2},
    ]},
    "Medallion of Fred": {ts: tsIns, fl: "M", op: 1, components: []},
    "Medallion of the Mummy": {ts: tsIns, fl: "M", op: 1, components: []},
    "Medallion of the Vampire": {ts: tsIns, fl: "M", op: 1, components: []},
    "Medallion of the Werewolf": {ts: tsIns, fl: "M", op: 1, components: []},
    "Medallion of the Witch": {ts: tsIns, fl: "M", op: 1, components: []},
    "Midnight Ink": {ts: tsIns, fl: "M", op: 4, components: [
        {compName: "Midnight Berry", ip: 3},
        {compName: "Egg", ip: 2},
        {compName: "Honey", ip: 1},
        {compName: "Squid", ip: 1},
    ]},
    "Old Rites": {ts: tsIns, fl: "O", op: 1, components: [
        {compName: "Elder Book", ip: 1},
        {compName: "Old Rites", ip: 6},
        {compName: "Flame Ink", ip: 3},
    ]},
    "Perfected Parchment": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Firmament Parchment", ip: 3},
    ]},
    "Pixie Ink": {ts: tsIns, fl: "P", op: 6, components: [
        {compName: "Pixieroot", ip: 6},
        {compName: "Angelflower", ip: 2},
        {compName: "Honey", ip: 1},
        {compName: "Squid", ip: 1},
    ]},
    "Prayer Reference {Book}": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Bone Book", ip: 1},
        {compName: "Prayer Reference", ip: 6},
        {compName: "Serpent Ink", ip: 3},
    ]},
    "Premium Parchment": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Primeval Parchment", ip: 3},
    ]},
    "Primeval Book": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Primeval Chapter", ip: 6},
        {compName: "Ghoul Ink", ip: 6},
    ]},
    "Primeval Chapter": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Primeval Writings", ip: 7},
        {compName: "Ghoul Ink", ip: 3},
    ]},
    "Primeval Reference Book": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Primeval Book", ip: 1},
        {compName: "Primeval Reference", ip: 6},
        {compName: "Ghoul Ink", ip: 3},
    ]},
    "Primitive Book": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Primeval Chapter", ip: 6},
        {compName: "Ghoul Ink", ip: 6},
    ]},
    "Primitive Chapter": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Primitive Writings", ip: 7},
        {compName: "Pixie Ink", ip: 2},
    ]},
    "Primitive Formation": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Aged Etching", ip: 5},
        {compName: "Primitive Rune", ip: 3},
    ]},
    "Primitive Markings": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Primitive Formation", ip: 2},
        {compName: "Superb Etching", ip: 1},
    ]},
    "Primitive Reference Book": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Pixie Ink", ip: 2},
        {compName: "Primitive Book", ip: 1},
        {compName: "Primitive Reference", ip: 6},
    ]},
    "Pristine Book": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Pristine Chapter", ip: 6},
        {compName: "Dark Arcane Ink", ip: 6},
    ]},
    "Pristine Chapter": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Pristine Writings", ip: 7},
        {compName: "Dark Arcane Ink", ip: 3},
    ]},
    "Pristine Parchment": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Preserved Parchment", ip: 3},
    ]},
    "Pristine Reference Book": {ts: tsIns, fl: "P", op: 1, components: [
        {compName: "Pristine Book", ip: 1},
        {compName: "Pristine Reference", ip: 6},
        {compName: "Dark Arcane Ink", ip: 3},
    ]},
    "Refined Markings": {ts: tsIns, fl: "R", op: 1, components: [
        {compName: "Refined Symbol", ip: 2},
        {compName: "Treasured Etching", ip: 2},
    ]},
    "Refined Symbol": {ts: tsIns, fl: "R", op: 1, components: [
        {compName: "Arcane Etching", ip: 6},
        {compName: "Cryptic Rune", ip: 2},
    ]},
    "Reindeer Village Map": {ts: tsIns, fl: "R", op: 1, components: [
        {compName: "Reindeer Map Parchment", ip: 4},
    ]},
    "Rightous Markings": {ts: tsIns, fl: "R", op: 1, components: [
        {compName: "Rightous Symbol", ip: 2},
        {compName: "Nomadic Etching", ip: 2},
    ]},
    "Rightous Symbol": {ts: tsIns, fl: "R", op: 1, components: [
        {compName: "Rightous Etching", ip: 6},
        {compName: "Celestial Rune", ip: 2},
    ]},
    "Ritual Book": {ts: tsIns, fl: "R", op: 1, components: [
        {compName: "Basic Writings", ip: 50},
        {compName: "Craft Glue", ip: 5},
    ]},
    "Ritual Shrine": {ts: tsIns, fl: "R", op: 1, components: [
        {compName: "Arcane Etching", ip: 25},
        {compName: "Cryptic Reference", ip: 25},
        {compName: "Cryptic Rune", ip: 25},
        {compName: "Treasured Etching", ip: 25},
        {compName: "Precious Book", ip: 75},
        {compName: "Precious Reference", ip: 75},
        {compName: "Treasured Rune", ip: 75},
    ]},
    "Rough Parchment": {ts: tsIns, fl: "R", op: 1, components: [
        {compName: "Rough Parchment", ip: 3},
    ]},
    "Sacred Parchment": {ts: tsIns, fl: "S", op: 1, components: [
        {compName: "Sentinel Parchment", ip: 3},
    ]},
    "Sacred Ritual Shrine": {ts: tsIns, fl: "S", op: 1, components: [
        {compName: "Dwarven Etching", ip: 25},
        {compName: "Frozen Rune", ip: 25},
        {compName: "Prayer Reference", ip: 25},
        {compName: "Wyvern Etching", ip: 25},
        {compName: "Holy Rune", ip: 75},
        {compName: "Sacred Reference", ip: 75},
        {compName: "Wyvern Book", ip: 75},
    ]},
    "Serpent Ink": {ts: tsIns, fl: "S", op: 10, components: [
        {compName: "Orchid", ip: 3},
        {compName: "Arapaima", ip: 2},
        {compName: "Serpent Vine", ip: 2},
    ]},
    "Spring Clean Up Manual": {ts: tsIns, fl: "S", op: 1, components: [
        {compName: "Clean Up Instructions", ip: 5},
    ]},
    "Teleport : Danof": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Basic Book", ip: 2},
        {compName: "Rough Parchment", ip: 1},
    ]},
    "Teleport : Drimly": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Primeval Book", ip: 4},
        {compName: "Premium Parchment", ip: 1},
    ]},
    "Teleport : First Set Random": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Pristine Book", ip: 4},
        {compName: "Pristine Parchment", ip: 1},
    ]},
    "Teleport : Poi": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Aged Book", ip: 3},
        {compName: "Delicate Parchment", ip: 1},
    ]},
    "Teleport : Settlements": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Aged Book", ip: 3},
        {compName: "Delicate Parchment", ip: 1},
    ]},
    "Teleport : Small Mud Hut": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Primitive Book", ip: 3},
        {compName: "Coarse Parchment", ip: 1},
    ]},
    "Teleport: Direct Shrine": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Bone Book", ip: 4},
        {compName: "Sacred Parchment", ip: 1},
    ]},
    "Teleport: Quick Shrine": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Elder Book", ip: 4},
        {compName: "Ancient Elven Parchment", ip: 1},
    ]},
    "Teleport: Random Shrine": {ts: tsIns, fl: "T", op: 1, components: [
        {compName: "Cryptic Book", ip: 4},
        {compName: "Exceptional Parchment", ip: 1},
    ]},
    "Venom Ink": {ts: tsIns, fl: "V", op: 10, components: [
        {compName: "Stonesilk Lily", ip: 3},
        {compName: "Featherback", ip: 2},
        {compName: "Venomweed", ip: 2},
    ]},
    "Waterberry Ink": {ts: tsIns, fl: "W", op: 10, components: [
        {compName: "Waterberry", ip: 3},
        {compName: "Honey", ip: 3},
        {compName: "Delvrak", ip: 2},
    ]},
    "Witik Markings": {ts: tsIns, fl: "W", op: 1, components: [
        {compName: "Witik Symbol", ip: 2},
        {compName: "Ancient Witik Etching", ip: 2},
    ]},
    "Witik Symbol": {ts: tsIns, fl: "W", op: 1, components: [
        {compName: "Witik Etching", ip: 6},
        {compName: "Temple Translations", ip: 2},
    ]},
    "Wyvern Markings": {ts: tsIns, fl: "W", op: 1, components: [
        {compName: "Dwarven Symbol", ip: 2},
        {compName: "Wyvern Etching", ip: 2},
    ]},
    // jewelcrafting
    "Adamantite Band": {ts: tsJew, fl: "A", op: 1, components: [
        {compName: "Adamantite Powder", ip: 5},
        {compName: "Pure Adamantite", ip: 2},
    ]},
    "Adamantite Powder": {ts: tsJew, fl: "A", op: 3, components: [
        {compName: "Basalt", ip: 2},
        {compName: "Adamantite Ore", ip: 1},
    ]},
    "Adamantite Ring (Superior+)": {ts: tsJew, fl: "A", op: 1, components: [
        {compName: "Adamantite Powder", ip: 6},
        {compName: "Adamantite Band", ip: 1},
        {compName: "Process Huge Sun Opal", ip: 3},
    ]},
    "Agate Augment (Rare)": {ts: tsJew, fl: "A", op: 1, components: [
        {compName: "Copper Powder", ip: 2},
        {compName: "Tin Powder", ip: 2},
        {compName: "Polish Large Agate", ip: 3},
    ]},
    "Amber Augment (Common)": {ts: tsJew, fl: "A", op: 1, components: [
        {compName: "Chromium Powder", ip: 3},
        {compName: "Polish Amber", ip: 2},
    ]},
    "Amethyst Aug (Common+)": {ts: tsJew, fl: "A", op: 1, components: [
        {compName: "Iridium Powder", ip: 3},
        {compName: "Polish Amethyst", ip: 2},
    ]},
    "Aquamarine Aug (Common+)": {ts: tsJew, fl: "A", op: 1, components: [
        {compName: "Mithril Powder", ip: 3},
        {compName: "Polish Aquamarine", ip: 2},
    ]},
    "Basic Agate Aug (Common)": {ts: tsJew, fl: "B", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Polish Agate", ip: 1},
    ]},
    "Basic Malachite Aug (Common)": {ts: tsJew, fl: "B", op: 1, components: [
        {compName: "Slate", ip: 2},
        {compName: "Polish Malachite", ip: 1},
    ]},
    "Basic Small Gem Augment": {ts: tsJew, fl: "B", op: 1, components: [
        {compName: "Stone", ip: 1},
        {compName: "Polish Small Gem", ip: 1},
    ]},
    "Blood-Amber Aug (Epic+)": {ts: tsJew, fl: "B", op: 1, components: [
        {compName: "Bloodstone Powder", ip: 15},
        {compName: "Process Huge Amber", ip: 2},
    ]},
    "Bloodstone Band": {ts: tsJew, fl: "B", op: 1, components: [
        {compName: "Bloodstone Powder", ip: 4},
        {compName: "Pure Bloodstone", ip: 2},
    ]},
    "Bloodstone Powder": {ts: tsJew, fl: "B", op: 3, components: [
        {compName: "Lava Rock", ip: 1},
        {compName: "Bloodstone Ore", ip: 1},
    ]},
    "Bloodstone Ring (Common+)": {ts: tsJew, fl: "B", op: 1, components: [
        {compName: "Bloodstone Band", ip: 1},
        {compName: "Process Huge Amber", ip: 2},
    ]},
    "Chrome-Amber Aug (Epic+)": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Chromium Powder", ip: 10},
        {compName: "Process Huge Amber", ip: 2},
    ]},
    "Chromium Band": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Chromium Powder", ip: 4},
        {compName: "Pure Chromium", ip: 2},
    ]},
    "Chromium Powder": {ts: tsJew, fl: "C", op: 3, components: [
        {compName: "Marble", ip: 1},
        {compName: "Chromium Ore", ip: 1},
    ]},
    "Chromium Ring (Common+)": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Chromium Band", ip: 1},
        {compName: "Process Huge Malachite", ip: 2},
    ]},
    "Citrine Augment (Common)": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Shale Rock", ip: 1},
        {compName: "Polish Citrine", ip: 1},
    ]},
    "Citrine Augment (Superior)": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Shale Rock", ip: 4},
        {compName: "Process Large Citrine", ip: 1},
    ]},
    "Cobalt Band": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Cobalt Powder", ip: 4},
        {compName: "Pure Cobalt", ip: 2},
    ]},
    "Cobalt Powder": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 2},
        {compName: "Cobalt Ore", ip: 1},
    ]},
    "Cobalt Ring (Common+)": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Cobalt Band", ip: 1},
        {compName: "Process Huge Malachite", ip: 1},
    ]},
    "Copper Band": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Copper Powder", ip: 4},
        {compName: "Pure Copper", ip: 2},
    ]},
    "Copper Powder": {ts: tsJew, fl: "C", op: 3, components: [
        {compName: "Stone", ip: 1},
        {compName: "Copper Ore", ip: 1},
    ]},
    "Copper Ring (Common+)": {ts: tsJew, fl: "C", op: 1, components: [
        {compName: "Copper Band", ip: 1},
        {compName: "Process Huge Gem", ip: 1},
    ]},
    "Festive Ornament": {ts: tsJew, fl: "F", op: 1, components: [
        {compName: "Smelt Copper Bar", ip: 1},
        {compName: "Silver Glitter", ip: 5},
        {compName: "Small Gem", ip: 1},
    ]},
    "Gold Water Dish": {ts: tsJew, fl: "G", op: 1, components: [
        {compName: "Gold Dish", ip: 8},
    ]},
    "Huge Agate Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Tin Powder", ip: 6},
        {compName: "Process Huge Agate", ip: 2},
    ]},
    "Huge Amethyst Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Iridium Powder", ip: 18},
        {compName: "Process Huge Amethyst", ip: 2},
    ]},
    "Huge Amethyst Aug (Rare+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Iridium Powder", ip: 6},
        {compName: "Process Huge Amethyst", ip: 1},
    ]},
    "Huge Aquamarine Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Mithril Powder", ip: 18},
        {compName: "Process Huge Aquamarine", ip: 2},
    ]},
    "Huge Aquamarine Aug (Rare+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Mithril Powder", ip: 6},
        {compName: "Process Huge Aquamarine", ip: 1},
    ]},
    "Huge Citrine Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Tin Powder", ip: 12},
        {compName: "Process Huge Citrine", ip: 2},
    ]},
    "Huge Citrine Aug (Rare+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Shale Rock", ip: 3},
        {compName: "Process Huge Citrine", ip: 1},
    ]},
    "Huge Gem Augment": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Copper Powder", ip: 12},
        {compName: "Process Huge Gem", ip: 2},
    ]},
    "Huge Malachite Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Iron Powder", ip: 12},
        {compName: "Process Huge Malachite", ip: 2},
    ]},
    "Huge Malachite Aug (Rare+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Iron Powder", ip: 6},
        {compName: "Process Huge Malachite", ip: 1},
    ]},
    "Huge Melanite Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Iron Powder", ip: 12},
        {compName: "Process Huge Malachite", ip: 2},
    ]},
    "Huge Melanite Aug (Rare+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Iron Powder", ip: 1},
        {compName: "Process Huge Malachite", ip: 6},
    ]},
    "Huge Mercury Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Tourmaline Powder", ip: 18},
        {compName: "Process Huge Mercury", ip: 2},
    ]},
    "Huge Mercury Aug (Rare+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Tourmaline Powder", ip: 6},
        {compName: "Process Huge Mercury", ip: 1},
    ]},
    "Huge Peridot Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Manganese Powder", ip: 18},
        {compName: "Process Huge Peridot", ip: 2},
    ]},
    "Huge Peridot Aug (Rare+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Manganese Powder", ip: 6},
        {compName: "Process Huge Peridot", ip: 1},
    ]},
    "Huge Sun Opal Aug (Epic+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Adamantite Powder", ip: 18},
        {compName: "Process Huge Sun Opal", ip: 2},
    ]},
    "Huge Sun Opal Aug (Rare+)": {ts: tsJew, fl: "H", op: 1, components: [
        {compName: "Adamantite Powder", ip: 6},
        {compName: "Process Huge Sun Opal", ip: 1},
    ]},
    "Iridium Band": {ts: tsJew, fl: "I", op: 1, components: [
        {compName: "Iridium Powder", ip: 5},
        {compName: "Pure Iridium", ip: 2},
    ]},
    "Iridium Powder": {ts: tsJew, fl: "I", op: 3, components: [
        {compName: "Quartz", ip: 2},
        {compName: "Iridium Ore", ip: 1},
    ]},
    "Iridium Ring (Superior+)": {ts: tsJew, fl: "I", op: 1, components: [
        {compName: "Iridium Powder", ip: 6},
        {compName: "Iridium Band", ip: 1},
        {compName: "Process Huge Amethyst", ip: 3},
    ]},
    "Iron Band": {ts: tsJew, fl: "I", op: 1, components: [
        {compName: "Iron Powder", ip: 4},
        {compName: "Pure Iron", ip: 2},
    ]},
    "Iron Powder": {ts: tsJew, fl: "I", op: 3, components: [
        {compName: "Shale Rock", ip: 1},
        {compName: "Iron Ore", ip: 1},
    ]},
    "Iron Ring (Common+)": {ts: tsJew, fl: "I", op: 1, components: [
        {compName: "Iron Band", ip: 1},
        {compName: "Process Huge Citrine", ip: 1},
    ]},
    "Large Amber Aug (Rare+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Bloodstone Powder", ip: 9},
        {compName: "Polish Large Amber", ip: 5},
    ]},
    "Large Amber Aug (Superior+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Chromium Powder", ip: 3},
        {compName: "Polish Large Amber", ip: 3},
    ]},
    "Large Amethyst Aug (Sup+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Iridium Powder", ip: 6},
        {compName: "Polish Large Amethyst", ip: 2},
    ]},
    "Large Aquamarine Aug (Sup+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Mithril Powder", ip: 6},
        {compName: "Polish Large Aquamarine", ip: 2},
    ]},
    "Large Malachite Aug (Superior+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Slate", ip: 5},
        {compName: "Polish Large Malachite", ip: 3},
    ]},
    "Large Melanite Aug (Sup+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Titanium Powder", ip: 6},
        {compName: "Polish Large Melanite", ip: 2},
    ]},
    "Large Mercury Aug (Sup+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Tourmaline Powder", ip: 6},
        {compName: "Polish Large Mercury", ip: 2},
    ]},
    "Large Peridot Aug (Sup+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Manganese Powder", ip: 6},
        {compName: "Polish Large Peridot", ip: 2},
    ]},
    "Large Sun Opal Aug (Sup+)": {ts: tsJew, fl: "L", op: 1, components: [
        {compName: "Adamantite Powder", ip: 6},
        {compName: "Polish Large Sun Opal", ip: 2},
    ]},
    "Manganese Band": {ts: tsJew, fl: "M", op: 1, components: [
        {compName: "Manganese Powder", ip: 5},
        {compName: "Pure Manganese", ip: 2},
    ]},
    "Manganese Powder": {ts: tsJew, fl: "M", op: 3, components: [
        {compName: "Obsidian", ip: 2},
        {compName: "Manganese Ore", ip: 1},
    ]},
    "Manganese Ring (Superior+)": {ts: tsJew, fl: "M", op: 1, components: [
        {compName: "Manganese Powder", ip: 6},
        {compName: "Manganese Band", ip: 1},
        {compName: "Process Huge Peridot", ip: 3},
    ]},
    "Melanite Aug (Common+)": {ts: tsJew, fl: "M", op: 1, components: [
        {compName: "Titanium Powder", ip: 3},
        {compName: "Polish Melanite", ip: 2},
    ]},
    "Mithril Band": {ts: tsJew, fl: "M", op: 1, components: [
        {compName: "Mithril Powder", ip: 5},
        {compName: "Pure Mithril", ip: 2},
    ]},
    "Mithril Powder": {ts: tsJew, fl: "M", op: 3, components: [
        {compName: "Pyrestone", ip: 2},
        {compName: "Mithril Ore", ip: 1},
    ]},
    "Mithril Ring (Superior+)": {ts: tsJew, fl: "M", op: 1, components: [
        {compName: "Mithril Powder", ip: 7},
        {compName: "Mithril Band", ip: 1},
        {compName: "Process Huge Aquamarine", ip: 3},
    ]},
    "Mercury Aug (Common+)": {ts: tsJew, fl: "M", op: 1, components: [
        {compName: "Tourmaline Powder", ip: 3},
        {compName: "Polish Mercury", ip: 2},
    ]},
    "Peridot Aug (Common+)": {ts: tsJew, fl: "P", op: 1, components: [
        {compName: "Manganese Powder", ip: 3},
        {compName: "Polish Peridot", ip: 2},
    ]},
    "Polish Agate": {ts: tsJew, fl: "P", op: 1, display: "Polished Agate", components: [
        {compName: "Copper Powder", ip: 3},
        {compName: "Agate", ip: 1},
    ]},
    "Polish Amber": {ts: tsJew, fl: "P", op: 1, display: "Polished Amber", components: [
        {compName: "Chromium Powder", ip: 2},
        {compName: "Amber", ip: 1},
    ]},
    "Polish Amethyst": {ts: tsJew, fl: "P", op: 1, display: "Polished Amethyst", components: [
        {compName: "Iridium Powder", ip: 3},
        {compName: "Amethyst", ip: 1},
    ]},
    "Polish Aquamarine": {ts: tsJew, fl: "P", op: 1, display: "Polished Aquamarine", components: [
        {compName: "Mithril Powder", ip: 3},
        {compName: "Aquamarine", ip: 1},
    ]},
    "Polish Citrine": {ts: tsJew, fl: "P", op: 1, display: "Polished Citrine", components: [
        {compName: "Tin Powder", ip: 4},
        {compName: "Citrine", ip: 1},
    ]},
    "Polish Large Agate": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Agate", components: [
        {compName: "Copper Powder", ip: 6},
        {compName: "Process Large Agate", ip: 1},
    ]},
    "Polish Large Amber": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Amber", components: [
        {compName: "Chromium Powder", ip: 6},
        {compName: "Process Large Amber", ip: 1},
    ]},
    "Polish Large Amethyst": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Amethyst", components: [
        {compName: "Iridium Powder", ip: 6},
        {compName: "Process Large Amethyst", ip: 1},
    ]},
    "Polish Large Aquamarine": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Aquamarine", components: [
        {compName: "Mithril Powder", ip: 6},
        {compName: "Process Large Aquamarine", ip: 1},
    ]},
    "Polish Large Citrine": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Citrine", components: [
        {compName: "Tin Powder", ip: 6},
        {compName: "Process Large Citrine", ip: 1},
    ]},
    "Polish Large Gem": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Gem", components: [
        {compName: "Copper Powder", ip: 6},
        {compName: "Process Large Gem", ip: 1},
    ]},
    "Polish Large Malachite": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Malachite", components: [
        {compName: "Iron Powder", ip: 6},
        {compName: "Process Large Malachite", ip: 1},
    ]},
    "Polish Large Melanite": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Melanite", components: [
        {compName: "Titanium Powder", ip: 6},
        {compName: "Process Large Melanite", ip: 1},
    ]},
    "Polish Large Mercury": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Mercury", components: [
        {compName: "Tourmaline Powder", ip: 6},
        {compName: "Process Large Mercury", ip: 1},
    ]},
    "Polish Large Peridot": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Peridot", components: [
        {compName: "Manganese Powder", ip: 6},
        {compName: "Process Large Peridot", ip: 1},
    ]},
    "Polish Large Sun Opal": {ts: tsJew, fl: "P", op: 1, display: "Polished Large Sun Opal", components: [
        {compName: "Adamantite Powder", ip: 6},
        {compName: "Process Large Sun Opal", ip: 1},
    ]},
    "Polish Malachite": {ts: tsJew, fl: "P", op: 1, display: "Polished Malachite", components: [
        {compName: "Iron Powder", ip: 2},
        {compName: "Malachite", ip: 1},
    ]},
    "Polish Melanite": {ts: tsJew, fl: "P", op: 1, display: "Polished Melanite", components: [
        {compName: "Titanium Powder", ip: 3},
        {compName: "Melanite", ip: 1},
    ]},
    "Polish Mercury": {ts: tsJew, fl: "P", op: 1, display: "Polished Mercury", components: [
        {compName: "Tourmaline Powder", ip: 3},
        {compName: "Mercury", ip: 1},
    ]},
    "Polish Peridot": {ts: tsJew, fl: "P", op: 1, display: "Polished Peridot", components: [
        {compName: "Manganese Powder", ip: 3},
        {compName: "Peridot", ip: 1},
    ]},
    "Polish Small Gem": {ts: tsJew, fl: "P", op: 1, display: "Polished Small Gem", components: [
        {compName: "Copper Powder", ip: 2},
        {compName: "Small Gem", ip: 1},
    ]},
    "Polish Sun Opal": {ts: tsJew, fl: "P", op: 1, display: "Polished Sun Opal", components: [
        {compName: "Adamantite Powder", ip: 3},
        {compName: "Sun Opal", ip: 1},
    ]},
    "Polished Large Garnet": {ts: tsJew, fl: "P", op: 1, components: [
        {compName: "Slate", ip: 2},
        {compName: "Iron Powder", ip: 8},
        {compName: "Garnet", ip: 3},
    ]},
    "Process Huge Agate": {ts: tsJew, fl: "P", op: 1, display: "Huge Agate", components: [
        {compName: "Sandstone", ip: 6},
        {compName: "Process Large Agate", ip: 3},
    ]},
    "Process Huge Amber": {ts: tsJew, fl: "P", op: 1, display: "Huge Amber", components: [
        {compName: "Marble", ip: 5},
        {compName: "Process Large Amber", ip: 3},
    ]},
    "Process Huge Amethyst": {ts: tsJew, fl: "P", op: 1, display: "Huge Amethyst", components: [
        {compName: "Quartz", ip: 2},
        {compName: "Iridium Powder", ip: 2},
        {compName: "Process Large Amethyst", ip: 3},
    ]},
    "Process Huge Aquamarine": {ts: tsJew, fl: "P", op: 1, display: "Huge Aquamarine", components: [
        {compName: "Pyrestone", ip: 2},
        {compName: "Mithril Powder", ip: 2},
        {compName: "Process Large Aquamarine", ip: 4},
    ]},
    "Process Huge Citrine": {ts: tsJew, fl: "P", op: 1, display: "Huge Citrine", components: [
        {compName: "Shale Rock", ip: 6},
        {compName: "Process Large Citrine", ip: 3},
    ]},
    "Process Huge Gem": {ts: tsJew, fl: "P", op: 1, display: "Huge Gem", components: [
        {compName: "Stone", ip: 6},
        {compName: "Process Large Gem", ip: 3},
    ]},
    "Process Huge Malachite": {ts: tsJew, fl: "P", op: 1, display: "Huge Malachite", components: [
        {compName: "Slate", ip: 6},
        {compName: "Process Large Malachite", ip: 3},
    ]},
    "Process Huge Melanite": {ts: tsJew, fl: "P", op: 1, display: "Huge Melanite", components: [
        {compName: "Crystal", ip: 2},
        {compName: "Titanium Powder", ip: 2},
        {compName: "Process Large Melanite", ip: 4},
    ]},
    "Process Huge Mercury": {ts: tsJew, fl: "P", op: 1, display: "Huge Mercury", components: [
        {compName: "Dark Silver", ip: 2},
        {compName: "Tourmaline Powder", ip: 2},
        {compName: "Process Large Mercury", ip: 4},
    ]},
    "Process Huge Peridot": {ts: tsJew, fl: "P", op: 1, display: "Huge Peridot", components: [
        {compName: "Obsidian", ip: 2},
        {compName: "Manganese Powder", ip: 2},
        {compName: "Process Large Peridot", ip: 4},
    ]},
    "Process Huge Sun Opal": {ts: tsJew, fl: "P", op: 1, components: [
        {compName: "Basalt", ip: 2},
        {compName: "Adamantite Powder", ip: 2},
        {compName: "Process Large Sun Opal", ip: 4},
    ]},
    "Process Large Agate": {ts: tsJew, fl: "P", op: 1, display: "Large Agate", components: [
        {compName: "Stone", ip: 2},
        {compName: "Agate", ip: 3},
    ]},
    "Process Large Amber": {ts: tsJew, fl: "P", op: 1, display: "Large Amber", components: [
        {compName: "Marble", ip: 2},
        {compName: "Amber", ip: 3},
    ]},
    "Process Large Amethyst": {ts: tsJew, fl: "P", op: 1, display: "Large Amethyst", components: [
        {compName: "Quartz", ip: 2},
        {compName: "Amethyst", ip: 3},
    ]},
    "Process Large Aquamarine": {ts: tsJew, fl: "P", op: 1, display: "Large Aquamarine", components: [
        {compName: "Pyrestone", ip: 2},
        {compName: "Aquamarine", ip: 3},
    ]},
    "Process Large Citrine": {ts: tsJew, fl: "P", op: 1, display: "Large Citrine", components: [
        {compName: "Shale Rock", ip: 2},
        {compName: "Citrine", ip: 3},
    ]},
    "Process Large Gem": {ts: tsJew, fl: "P", op: 1, display: "Large Gem", components: [
        {compName: "Stone", ip: 1},
        {compName: "Small Gem", ip: 3},
    ]},
    "Process Large Malachite": {ts: tsJew, fl: "P", op: 1, display: "Large Malachite", components: [
        {compName: "Slate", ip: 2},
        {compName: "Malachite", ip: 3},
    ]},
    "Process Large Melanite": {ts: tsJew, fl: "P", op: 1, display: "Large Melanite", components: [
        {compName: "Crystal", ip: 2},
        {compName: "Melanite", ip: 3},
    ]},
    "Process Large Mercury": {ts: tsJew, fl: "P", op: 1, display: "Large Mercury", components: [
        {compName: "Dark Silver", ip: 2},
        {compName: "Mercury", ip: 3},
    ]},
    "Process Large Peridot": {ts: tsJew, fl: "P", op: 1, display: "Large Peridot", components: [
        {compName: "Obsidian", ip: 2},
        {compName: "Peridot", ip: 3},
    ]},
    "Process Large Sun Opal": {ts: tsJew, fl: "P", op: 1, display: "Large Sun Opal", components: [
        {compName: "Basalt", ip: 2},
        {compName: "Sun Opal", ip: 3},
    ]},
    "Rare Gem Augment": {ts: tsJew, fl: "R", op: 1, components: [
        {compName: "Stone", ip: 3},
        {compName: "Polish Large Gem", ip: 3},
    ]},
    "Ring of Power": {ts: tsJew, fl: "R", op: 1, components: [
        {compName: "Powerful Ore", ip: 5},
    ]},
    "Solitaire Ring": {ts: tsJew, fl: "S", op: 1, components: [
        {compName: "Sparkly Diamond", ip: 8},
    ]},
    "Spring Decor": {ts: tsJew, fl: "S", op: 1, components: [
        {compName: "Spring Jewels", ip: 5},
    ]},
    "Sun Opal Aug (Common+)": {ts: tsJew, fl: "S", op: 1, components: [
        {compName: "Adamantite Powder", ip: 3},
        {compName: "Polish Sun Opal", ip: 2},
    ]},
    "Superior Agate Augment": {ts: tsJew, fl: "S", op: 1, components: [
        {compName: "Copper Powder", ip: 2},
        {compName: "Polish Large Agate", ip: 1},
    ]},
    "Superior Small Gem Augment": {ts: tsJew, fl: "S", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Polish Small Gem", ip: 4},
    ]},
    "Tin Band": {ts: tsJew, fl: "T", op: 1, components: [
        {compName: "Tin Powder", ip: 4},
        {compName: "Pure Tin", ip: 2},
    ]},
    "Tin Powder": {ts: tsJew, fl: "T", op: 3, components: [
        {compName: "Sandstone", ip: 1},
        {compName: "Tin Ore", ip: 1},
    ]},
    "Tin Ring (Common+)": {ts: tsJew, fl: "T", op: 1, components: [
        {compName: "Tin Band", ip: 1},
        {compName: "Process Huge Agate", ip: 1},
    ]},
    "Titanium Band": {ts: tsJew, fl: "T", op: 1, components: [
        {compName: "Titanium Powder", ip: 5},
        {compName: "Pure Titanium", ip: 2},
    ]},
    "Titanium Powder": {ts: tsJew, fl: "T", op: 3, components: [
        {compName: "Crystal", ip: 2},
        {compName: "Titanium Ore", ip: 1},
    ]},
    "Titanium Ring (Superior+)": {ts: tsJew, fl: "T", op: 1, components: [
        {compName: "Titanium Powder", ip: 6},
        {compName: "Titanium Band", ip: 1},
        {compName: "Process Huge Melanite", ip: 3},
    ]},
    "Tourmaline Band": {ts: tsJew, fl: "T", op: 1, components: [
        {compName: "Tourmaline Powder", ip: 5},
        {compName: "Pure Tourmaline", ip: 2},
    ]},
    "Tourmaline Powder": {ts: tsJew, fl: "T", op: 3, components: [
        {compName: "Dark Silver", ip: 2},
        {compName: "Tourmaline Ore", ip: 1},
    ]},
    "Tourmaline Ring (Superior+)": {ts: tsJew, fl: "T", op: 1, components: [
        {compName: "Tourmaline Powder", ip: 6},
        {compName: "Tourmaline Band", ip: 1},
        {compName: "Process Huge Mercury", ip:13},
    ]},
    // logging
    "Acorn": {ts: tsLog, fl: "A", op: 1, components: []},
    "Alkali": {ts: tsLog, fl: "A", op: 1, components: []},
    "Ballista Wood": {ts: tsLog, fl: "B", op: 1, components: []},
    "Bigtooth Aspen": {ts: tsLog, fl: "B", op: 1, components: []},
    "Birdseye Maple": {ts: tsLog, fl: "B", op: 1, components: []},
    "Black Ash": {ts: tsLog, fl: "B", op: 1, components: []},
    "Black Spruce": {ts: tsLog, fl: "B", op: 1, components: []},
    "Blue Spruce": {ts: tsLog, fl: "B", op: 1, components: []},
    "Chestnut": {ts: tsLog, fl: "C", op: 1, components: []},
    "Coop Wood": {ts: tsLog, fl: "C", op: 1, components: []},
    "Dark Cherry": {ts: tsLog, fl: "D", op: 1, components: []},
    "Elder Birch": {ts: tsLog, fl: "E", op: 1, components: []},
    "Elven Walnut": {ts: tsLog, fl: "E", op: 1, components: []},
    "Evergreen": {ts: tsLog, fl: "E", op: 1, components: []},
    "Fern": {ts: tsLog, fl: "F", op: 1, components: []},
    "Firewood": {ts: tsLog, fl: "F", op: 1, components: []},
    "Green Party Balloon": {ts: tsLog, fl: "G", op: 1, components: []},
    "Grey Log Bed": {ts: tsLog, fl: "G", op: 1, components: []},
    "Heart Balloon": {ts: tsLog, fl: "H", op: 1, components: []},
    "Heart Candy - Be Mine": {ts: tsLog, fl: "H", op: 1, components: []},
    "Hemlock": {ts: tsLog, fl: "H", op: 1, components: []},
    "Hickory": {ts: tsLog, fl: "H", op: 1, components: []},
    "Jade Egg": {ts: tsLog, fl: "J", op: 1, components: []},
    "Kingswood": {ts: tsLog, fl: "K", op: 1, components: []},
    "Large Logs": {ts: tsLog, fl: "L", op: 1, components: []},
    "Light Birch": {ts: tsLog, fl: "L", op: 1, components: []},
    "Long Branch": {ts: tsLog, fl: "L", op: 1, components: []},
    "Mahogany": {ts: tsLog, fl: "M", op: 1, components: []},
    "Maidenhair": {ts: tsLog, fl: "M", op: 1, components: []},
    "Minor Arcana: Sword": {ts: tsLog, fl: "M", op: 1, components: []},
    "Mora": {ts: tsLog, fl: "M", op: 1, components: []},
    "Oak": {ts: tsLog, fl: "O", op: 1, components: []},
    "Pine Branches": {ts: tsLog, fl: "P", op: 1, components: []},
    "Pink Easter Egg": {ts: tsLog, fl: "P", op: 1, components: []},
    "Purple Party Balloon": {ts: tsLog, fl: "P", op: 1, components: []},
    "Purple Striped Easter Egg": {ts: tsLog, fl: "P", op: 1, components: []},
    "Red Cedar": {ts: tsLog, fl: "R", op: 1, components: []},
    "Red Party Balloon": {ts: tsLog, fl: "R", op: 1, components: []},
    "Rosewood": {ts: tsLog, fl: "R", op: 1, components: []},
    "Satinwood": {ts: tsLog, fl: "S", op: 1, components: []},
    "Sequoia": {ts: tsLog, fl: "S", op: 1, components: []},
    "Sharkwood": {ts: tsLog, fl: "S", op: 1, components: []},
    "Silver Birch": {ts: tsLog, fl: "S", op: 1, components: []},
    "Silver Maple": {ts: tsLog, fl: "S", op: 1, components: []},
    "Sourknot": {ts: tsLog, fl: "S", op: 1, components: []},
    "Strawberry Syrup": {ts: tsLog, fl: "S", op: 1, components: []},
    "Sugar Maple": {ts: tsLog, fl: "S", op: 1, components: []},
    "Tilia Cordata": {ts: tsLog, fl: "T", op: 1, components: []},
    "Walnut": {ts: tsLog, fl: "W", op: 1, components: []},
    "White Crystal": {ts: tsLog, fl: "W", op: 1, components: []},
    "White Pine": {ts: tsLog, fl: "W", op: 1, components: []},
    "White Willow": {ts: tsLog, fl: "W", op: 1, components: []},
    "Wild Cherry": {ts: tsLog, fl: "W", op: 1, components: []},
    // mining
    "Adamantite Ore": {ts: tsMin, fl: "A", op: 1, components: []},
    "Agate": {ts: tsMin, fl: "A", op: 1, components: []},
    "Amber": {ts: tsMin, fl: "A", op: 1, components: []},
    "Amethyst": {ts: tsMin, fl: "A", op: 1, components: []},
    "Aquamarine": {ts: tsMin, fl: "A", op: 1, components: []},
    "Basalt": {ts: tsMin, fl: "B", op: 1, components: []},
    "Black Diamond": {ts: tsMin, fl: "B", op: 1, components: []},
    "Blackened Clay": {ts: tsMin, fl: "B", op: 1, components: []},
    "Blackened Stone": {ts: tsMin, fl: "B", op: 1, components: []},
    "Bloodstone Ore": {ts: tsMin, fl: "B", op: 1, components: []},
    "Blue Striped Easter Egg": {ts: tsMin, fl: "B", op: 1, components: []},
    "Blueberry Syrup": {ts: tsMin, fl: "B", op: 1, components: []},
    "Carnelian": {ts: tsMin, fl: "C", op: 1, components: []},
    "Chromium Ore": {ts: tsMin, fl: "C", op: 1, components: []},
    "Citrine": {ts: tsMin, fl: "C", op: 1, components: []},
    "Clay": {ts: tsMin, fl: "C", op: 1, components: []},
    "Clear Opal": {ts: tsMin, fl: "C", op: 1, components: []},
    "Coal": {ts: tsMin, fl: "C", op: 1, components: []},
    "Cobalt Ore": {ts: tsMin, fl: "C", op: 1, components: []},
    "Copper Ore": {ts: tsMin, fl: "C", op: 1, components: []},
    "Crystal": {ts: tsMin, fl: "C", op: 1, components: []},
    "Dark Golemite Ore": {ts: tsMin, fl: "D", op: 1, components: []},
    "Dark Silver": {ts: tsMin, fl: "D", op: 1, components: []},
    "Diamond": {ts: tsMin, fl: "D", op: 1, components: []},
    "Dragonstone": {ts: tsMin, fl: "D", op: 1, components: []},
    "Earthen Clay": {ts: tsMin, fl: "E", op: 1, components: []},
    "Emberstone": {ts: tsMin, fl: "E", op: 1, components: []},
    "Emerald": {ts: tsMin, fl: "E", op: 1, components: []},
    "Fire Clay": {ts: tsMin, fl: "F", op: 1, components: []},
    "Flame Pearl": {ts: tsMin, fl: "F", op: 1, components: []},
    "Garnet": {ts: tsMin, fl: "G", op: 1, components: []},
    "Gem Fragment": {ts: tsMin, fl: "G", op: 1, components: []},
    "Gemstone": {ts: tsMin, fl: "G", op: 1, components: []},
    "Gold Dish": {ts: tsMin, fl: "G", op: 1, components: []},
    "Gold Link": {ts: tsMin, fl: "G", op: 1, components: []},
    "Golden Clay": {ts: tsMin, fl: "G", op: 1, components: []},
    "Golden Egg": {ts: tsMin, fl: "G", op: 1, components: []},
    "Golemite Ore": {ts: tsMin, fl: "G", op: 1, components: []},
    "Grey Stone Pie": {ts: tsMin, fl: "G", op: 1, components: []},
    "Heart Candy - I’m Yours": {ts: tsMin, fl: "H", op: 1, components: []},
    "Holiday Ore": {ts: tsMin, fl: "H", op: 1, components: []},
    "Iridium Ore": {ts: tsMin, fl: "I", op: 1, components: []},
    "Iron Ore": {ts: tsMin, fl: "I", op: 1, components: []},
    "Kaolinite": {ts: tsMin, fl: "K", op: 1, components: []},
    "Lava Rock": {ts: tsMin, fl: "L", op: 1, components: []},
    "Limestone": {ts: tsMin, fl: "L", op: 1, components: []},
    "Malachite": {ts: tsMin, fl: "M", op: 1, components: []},
    "Manganese Ore": {ts: tsMin, fl: "M", op: 1, components: []},
    "Marble": {ts: tsMin, fl: "M", op: 1, components: []},
    "Melanite": {ts: tsMin, fl: "M", op: 1, components: []},
    "Memory Powder": {ts: tsMin, fl: "M", op: 1, components: []},
    "Mercury": {ts: tsMin, fl: "M", op: 1, components: []},
    "Metal Fragment": {ts: tsMin, fl: "M", op: 1, components: []},
    "Mineral Chunk": {ts: tsMin, fl: "M", op: 1, components: []},
    "Minor Arcana: Coin": {ts: tsMin, fl: "M", op: 1, components: []},
    "Mithril Ore": {ts: tsMin, fl: "M", op: 1, components: []},
    "Molten Ash": {ts: tsMin, fl: "M", op: 1, components: []},
    "Molten Clay": {ts: tsMin, fl: "M", op: 1, components: []},
    "Molten Fossil": {ts: tsMin, fl: "M", op: 1, components: []},
    "Moonstone": {ts: tsMin, fl: "M", op: 1, components: []},
    "Obsidian": {ts: tsMin, fl: "O", op: 1, components: []},
    "Peridot": {ts: tsMin, fl: "P", op: 1, components: []},
    "Pile of Laterite": {ts: tsMin, fl: "P", op: 1, components: []},
    "Powerful Ore": {ts: tsMin, fl: "P", op: 1, components: []},
    "Pyrestone": {ts: tsMin, fl: "P", op: 1, components: []},
    "Quartz": {ts: tsMin, fl: "Q", op: 1, components: []},
    "Raw Arrow Head": {ts: tsMin, fl: "R", op: 1, components: []},
    "Red Easter Egg": {ts: tsMin, fl: "R", op: 1, components: []},
    "Red Golemite Ore": {ts: tsMin, fl: "R", op: 1, components: []},
    "Ruby": {ts: tsMin, fl: "R", op: 1, components: []},
    "Sandstone": {ts: tsMin, fl: "S", op: 1, components: []},
    "Sapphire": {ts: tsMin, fl: "S", op: 1, components: []},
    "Scrap Metal": {ts: tsMin, fl: "S", op: 1, components: []},
    "Shackle Key": {ts: tsMin, fl: "S", op: 1, components: []},
    "Shale Rock": {ts: tsMin, fl: "S", op: 1, components: []},
    "Silver Ore": {ts: tsMin, fl: "S", op: 1, components: []},
    "Slate": {ts: tsMin, fl: "S", op: 1, components: []},
    "Small Gem": {ts: tsMin, fl: "S", op: 1, components: []},
    "Sparkly Diamond": {ts: tsMin, fl: "S", op: 1, components: []},
    "Stone": {ts: tsMin, fl: "S", op: 1, components: []},
    "Sun Opal": {ts: tsMin, fl: "S", op: 1, components: []},
    "Tin Ore": {ts: tsMin, fl: "T", op: 1, components: []},
    "Titanium Ore": {ts: tsMin, fl: "T", op: 1, components: []},
    "Tourmaline Ore": {ts: tsMin, fl: "T", op: 1, components: []},
    "Truffles": {ts: tsMin, fl: "T", op: 1, components: []},
    "Wolf Ash Clay": {ts: tsMin, fl: "W", op: 1, components: []},
    "Wulfenite Ore": {ts: tsMin, fl: "W", op: 1, components: []},
    // smithing
    "Acid-Etched Adamantite Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Adamantite Hammer Head", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
        {compName: "Devilweed Acid", ip: 3},
    ]},
    "Acid-Etched Bloodstone Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Bloodstone Hammer Head", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
        {compName: "Dragontear Acid", ip: 3},
    ]},
    "Acid-Etched Chromium Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Chromium Hammer Head", ip: 1},
        {compName: "Dragontear Acid", ip: 3},
    ]},
    "Acid-Etched Cobalt Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Cobalt Hammer Head", ip: 1},
        {compName: "Dragontear Acid", ip: 2},
    ]},
    "Acid-Etched Copper Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Copper Hammer Head", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Dragontear Acid", ip: 1},
    ]},
    "Acid-Etched Iridium Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Iridium Hammer Head", ip: 1},
        {compName: "Silver Maple Handle", ip: 1},
        {compName: "Devilweed Acid", ip: 3},
    ]},
    "Acid-Etched Iron Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Iron Hammer Head", ip: 1},
        {compName: "Dragontear Acid", ip: 2},
    ]},
    "Acid-Etched Manganese Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Manganese Hammer Head", ip: 1},
        {compName: "Maidenhair Handle", ip: 1},
        {compName: "Serpent Vine Acid", ip: 3},
    ]},
    "Acid-Etched Mithril Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Mithril Hammer Head", ip: 1},
        {compName: "Black Lotus Acid", ip: 3},
        {compName: "Mahogany Handle", ip: 1},
    ]},
    "Acid-Etched Tin Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Tin Hammer Head", ip: 1},
        {compName: "Dragontear Acid", ip: 1},
    ]},
    "Acid-Etched Titanium Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Titanium Hammer Head", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
        {compName: "Venomweed Acid", ip: 3},
    ]},
    "Acid-Etched Tourmaline Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Tourmaline Hammer Head", ip: 1},
        {compName: "Elven Walnut Handle", ip: 1},
        {compName: "Flame Lotus Acid", ip: 3},
    ]},
    "Adamantite Armplates": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Thick Adamantite Bar", ip: 1},
    ]},
    "Adamantite Axe Blade": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Basalt", ip: 4},
        {compName: "Clear Opal", ip: 3},
        {compName: "Pure Adamantite", ip: 2},
    ]},
    "Adamantite Boots (Rare+)": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Basalt", ip: 1},
        {compName: "Thick Adamantite Bar", ip: 3},
    ]},
    "Adamantite Chestguard (Superior+)": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Thick Adamantite Bar", ip: 1},
    ]},
    "Adamantite Crafting Blade": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Basalt", ip: 4},
        {compName: "Pure Adamantite", ip: 1},
        {compName: "Devilweed Acid", ip: 4},
    ]},
    "Adamantite Fitting": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Thick Adamantite Bar", ip: 1},
    ]},
    "Adamantite Flame Sword": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Clear Opal", ip: 12},
        {compName: "Thick Adamantite Bar", ip: 8},
    ]},
    "Adamantite Gauntlets (Superior+)": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Thick Adamantite Bar", ip: 1},
    ]},
    "Adamantite Hammer Head": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Basalt", ip: 4},
        {compName: "Clear Opal", ip: 4},
        {compName: "Thick Adamantite Bar", ip: 2},
    ]},
    "Adamantite Helm (Superior+)": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Thick Adamantite Bar", ip: 1},
    ]},
    "Adamantite Ice Gloves": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Clear Opal", ip: 7},
        {compName: "Thick Adamantite Bar", ip: 6},
    ]},
    "Adamantite Key": {ts: tsSmi, fl: "A", op: 5, components: [
        {compName: "Smelt Adamantite Bar", ip: 3},
    ]},
    "Adamantite Legplates (Superior+)": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Thick Adamantite Bar", ip: 1},
    ]},
    "Adamantite Logging Axe": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Smelt Adamantite Bar", ip: 2},
        {compName: "Adamantite Axe Blade", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Adamantite Nails": {ts: tsSmi, fl: "A", op: 15, display: "Adamantite Nail", components: [
        {compName: "Smelt Adamantite Bar", ip: 1},
    ]},
    "Adamantite Pick Blade": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Basalt", ip: 4},
        {compName: "Smelt Adamantite Bar", ip: 4},
    ]},
    "Adamantite Shield (Rare+)": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Basalt", ip: 3},
        {compName: "Thick Adamantite Bar", ip: 3},
    ]},
    "Adamantite Shovel Blade": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Basalt", ip: 2},
        {compName: "Thick Adamantite Bar", ip: 2},
        {compName: "Devilweed Acid", ip: 4},
    ]},
    "Adamantite Smithing Hammer": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Smelt Adamantite Bar", ip: 2},
        {compName: "Adamantite Hammer Head", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Adamantite Staff Base": {ts: tsSmi, fl: "A", op: 1, components: [
        {compName: "Thick Adamantite Bar", ip: 1},
        {compName: "Rough Sharkwood Rod", ip: 1},
    ]},
    "Basic Cobalt Feet": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Slate", ip: 1},
        {compName: "Smelt Cobalt Bar", ip: 1},
        {compName: "Smelt Iron Bar", ip: 1},
    ]},
    "Basic Cobalt Legs": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Slate", ip: 1},
        {compName: "Smelt Cobalt Bar", ip: 1},
        {compName: "Smelt Iron Bar", ip: 1},
    ]},
    "Basic Copper Arms": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Smelt Copper Bar", ip: 2},
    ]},
    "Basic Copper Chest": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Smelt Copper Bar", ip: 2},
    ]},
    "Basic Copper Feet": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Smelt Copper Bar", ip: 1},
    ]},
    "Basic Copper Hands": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Smelt Copper Bar", ip: 1},
    ]},
    "Basic Copper Legs": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Stone", ip: 1},
        {compName: "Smelt Copper Bar", ip: 2},
    ]},
    "Basic Iron Armplates": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Shale Rock", ip: 2},
        {compName: "Smelt Iron Bar", ip: 2},
    ]},
    "Basic Tin Arms": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Sandstone", ip: 1},
        {compName: "Smelt Tin Bar", ip: 1},
    ]},
    "Basic Tin Hands": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Sandstone", ip: 1},
        {compName: "Smelt Tin Bar", ip: 1},
    ]},
    "Basic Tin Helm": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Sandstone", ip: 1},
        {compName: "Smelt Tin Bar", ip: 2},
    ]},
    "Basic Tin Legs": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Sandstone", ip: 1},
        {compName: "Smelt Tin Bar", ip: 1},
    ]},
    "Basic Tin Shield": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Sandstone", ip: 1},
        {compName: "Smelt Tin Bar", ip: 2},
    ]},
    "Blood Chestguard (Superior+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Smelt Bloodstone Bar", ip: 4},
    ]},
    "Blood Gauntlets (Common+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Smelt Bloodstone Bar", ip: 3},
    ]},
    "Blood Golemite Arms (Epic+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 5},
        {compName: "Smelt Blood Golemite Bar", ip: 3},
    ]},
    "Blood Golemite Helm (Epic+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 5},
        {compName: "Smelt Blood Golemite Bar", ip: 3},
    ]},
    "Bloodstone Arms (Superior+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Smelt Bloodstone Bar", ip: 4},
    ]},
    "Bloodstone Axe Blade": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Pure Bloodstone", ip: 2},
        {compName: "Moonstone", ip: 1},
    ]},
    "Bloodstone Boots (Rare+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Thick Bloodstone Bar", ip: 3},
    ]},
    "Bloodstone Cap (Common+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 2},
        {compName: "Smelt Bloodstone Bar", ip: 3},
    ]},
    "Bloodstone Crafting Blade": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Pure Bloodstone", ip: 1},
        {compName: "Dragontear Acid", ip: 4},
    ]},
    "Bloodstone Fitting": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Thick Bloodstone Bar", ip: 1},
    ]},
    "Bloodstone Hammer Head": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Thick Bloodstone Bar", ip: 2},
        {compName: "Moonstone", ip: 1},
    ]},
    "Bloodstone Key": {ts: tsSmi, fl: "B", op: 5, components: [
        {compName: "Smelt Bloodstone Bar", ip: 3},
    ]},
    "Bloodstone Logging Axe": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Bloodstone Ore", ip: 2},
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Bloodstone Axe Blade", ip: 1},
    ]},
    "Bloodstone Pick Blade": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 4},
        {compName: "Smelt Bloodstone Bar", ip: 4},
    ]},
    "Bloodstone Shovel Blade": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Thick Bloodstone Bar", ip: 2},
        {compName: "Dragontear Acid", ip: 4},
    ]},
    "Bloodstone Smithing Hammer": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Bloodstone Ore", ip: 2},
        {compName: "Bloodstone Hammer Head", ip: 1},
        {compName: "Wild Cherry Handle", ip: 1},
    ]},
    "Bloodstone Staff Base": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Rough Aspen Rod", ip: 1},
        {compName: "Thick Bloodstone Bar", ip: 1},
    ]},
    "Bolt Mould": {ts: tsSmi, fl: "B", op: 1, components: []},
    "Bronze-Iron Hands (Rare+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Shale Rock", ip: 3},
        {compName: "Smelt Thick Bronze Bar", ip: 2},
        {compName: "Smelt Thick Iron Bar", ip: 3},
    ]},
    "Bronze-Iron Shield (Rare+)": {ts: tsSmi, fl: "B", op: 1, components: [
        {compName: "Shale Rock", ip: 2},
        {compName: "Smelt Thick Bronze Bar", ip: 2},
        {compName: "Smelt Thick Iron Bar", ip: 3},
    ]},
    "Candlestick": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Holiday Ore", ip: 7},
    ]},
    "Cast Iron Pot": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Smelt Thick Iron Bar", ip: 10},
        {compName: "Smelt Golemite Ore", ip: 2},
    ]},
    "Chrome Chainmail (Superior+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 2},
        {compName: "Smelt Chromium Bar", ip: 4},
    ]},
    "Chromebalt Fitting": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Smelt Chromium Bar", ip: 3},
        {compName: "Smelt Cobalt Bar", ip: 3},
    ]},
    "Chromium Axe Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 3},
        {compName: "Blackened Stone", ip: 1},
        {compName: "Pure Chromium", ip: 2},
    ]},
    "Chromium Boots (Common+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 3},
        {compName: "Smelt Chromium Bar", ip: 3},
    ]},
    "Chromium Chain-Coif (Rare+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 2},
        {compName: "Smelt Thick Chromium Bar", ip: 4},
    ]},
    "Chromium Crafting Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 3},
        {compName: "Pure Chromium", ip: 1},
        {compName: "Dragontear Acid", ip: 3},
    ]},
    "Chromium Fitting": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Smelt Thick Chromium Bar", ip: 1},
    ]},
    "Chromium Gauntlets (Rare+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 3},
        {compName: "Smelt Thick Chromium Bar", ip: 3},
    ]},
    "Chromium Hammer Head": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 3},
        {compName: "Blackened Stone", ip: 1},
        {compName: "Smelt Thick Chromium Bar", ip: 2},
    ]},
    "Chromium Key": {ts: tsSmi, fl: "C", op: 5, components: [
        {compName: "Smelt Chromium Bar", ip: 3},
    ]},
    "Chromium Legs (Common+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 2},
        {compName: "Smelt Chromium Bar", ip: 3},
    ]},
    "Chromium Logging Axe": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Chromium Ore", ip: 2},
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Chromium Axe Blade", ip: 1},
    ]},
    "Chromium Pick Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 4},
        {compName: "Smelt Chromium Bar", ip: 4},
    ]},
    "Chromium Shield (Superior+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 3},
        {compName: "Smelt Chromium Bar", ip: 4},
    ]},
    "Chromium Shovel Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Marble", ip: 3},
        {compName: "Smelt Thick Chromium Bar", ip: 2},
        {compName: "Dragontear Acid", ip: 3},
    ]},
    "Chromium Smithing Hammer": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Chromium Ore", ip: 2},
        {compName: "Chromium Hammer Head", ip: 1},
        {compName: "Wild Cherry Handle", ip: 1},
    ]},
    "Chromium Staff Base": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Rough Aspen Rod", ip: 1},
        {compName: "Smelt Thick Chromium Bar", ip: 1},
    ]},
    "Cobalt Axe Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 2},
        {compName: "Malachite", ip: 1},
        {compName: "Pure Cobalt", ip: 2},
    ]},
    "Cobalt Chestguard (Superior+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 1},
        {compName: "Smelt Thick Cobalt Bar", ip: 2},
        {compName: "Smelt Thick Iron Bar", ip: 1},
    ]},
    "Cobalt Crafting Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 3},
        {compName: "Pure Cobalt", ip: 1},
        {compName: "Dragontear Acid", ip: 3},
    ]},
    "Cobalt Fitting": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Smelt Thick Cobalt Bar", ip: 1},
    ]},
    "Cobalt Greathelm (Superior+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 1},
        {compName: "Smelt Thick Cobalt Bar", ip: 2},
        {compName: "Smelt Thick Iron Bar", ip: 1},
    ]},
    "Cobalt Hammer Head": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 2},
        {compName: "Malachite", ip: 1},
        {compName: "Smelt Thick Cobalt Bar", ip: 2},
    ]},
    "Cobalt Key": {ts: tsSmi, fl: "C", op: 5, components: [
        {compName: "Smelt Cobalt Bar", ip: 3},
    ]},
    "Cobalt Logging Axe": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Cobalt Ore", ip: 2},
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Cobalt Axe Blade", ip: 1},
    ]},
    "Cobalt Pick Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 4},
        {compName: "Smelt Cobalt Bar", ip: 4},
    ]},
    "Cobalt Shovel Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 3},
        {compName: "Smelt Thick Cobalt Bar", ip: 2},
        {compName: "Dragontear Acid", ip: 3},
    ]},
    "Cobalt Smithing Hammer": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Cobalt Ore", ip: 2},
        {compName: "Cobalt Hammer Head", ip: 1},
        {compName: "Wild Cherry Handle", ip: 1},
    ]},
    "Cobalt Staff Base": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Rough Aspen Rod", ip: 1},
        {compName: "Smelt Thick Cobalt Bar", ip: 1},
    ]},
    "Cobalt Wristguards (Rare+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Slate", ip: 1},
        {compName: "Malachite", ip: 3},
        {compName: "Smelt Thick Cobalt Bar", ip: 1},
        {compName: "Smelt Thick Iron Bar", ip: 2},
    ]},
    "Common Copper Dagger": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Smelt Copper Bar", ip: 3},
    ]},
    "Common Iron Gauntlets": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Shale Rock", ip: 2},
        {compName: "Smelt Iron Bar", ip: 2},
    ]},
    "Copper Armplates (Rare+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Small Gem", ip: 1},
        {compName: "Smelt Thick Copper Bar", ip: 2},
    ]},
    "Copper Axe Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Pure Copper", ip: 2},
        {compName: "Small Gem", ip: 1},
    ]},
    "Copper Crafting Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Stone", ip: 3},
        {compName: "Pure Copper", ip: 1},
        {compName: "Dragontear Acid", ip: 1},
    ]},
    "Copper Fasteners": {ts: tsSmi, fl: "C", op: 4, components: [
        {compName: "Pure Copper", ip: 2},
    ]},
    "Copper Feet (Rare+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Small Gem", ip: 1},
        {compName: "Smelt Thick Copper Bar", ip: 2},
    ]},
    "Copper Fitting": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Smelt Thick Copper Bar", ip: 1},
    ]},
    "Copper Hammer Head": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Small Gem", ip: 1},
        {compName: "Smelt Thick Copper Bar", ip: 2},
    ]},
    "Copper Hands (Rare+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Small Gem", ip: 1},
        {compName: "Smelt Thick Copper Bar", ip: 3},
    ]},
    "Copper Key": {ts: tsSmi, fl: "C", op: 4, components: [
        {compName: "Smelt Copper Bar", ip: 2},
    ]},
    "Copper Logging Axe": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Copper Ore", ip: 2},
        {compName: "Copper Axe Blade", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
    ]},
    "Copper Pick Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Stone", ip: 4},
        {compName: "Smelt Copper Bar", ip: 4},
    ]},
    "Copper Shovel Blade": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Smelt Thick Copper Bar", ip: 2},
        {compName: "Dragontear Acid", ip: 1},
    ]},
    "Copper Smithing Hammer": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Copper Ore", ip: 2},
        {compName: "Copper Hammer Head", ip: 1},
        {compName: "Fine Spruce Handle", ip: 1},
    ]},
    "Copper Staff Base": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Rough Birch Rod", ip: 1},
        {compName: "Smelt Thick Copper Bar", ip: 1},
    ]},
    "Copper-Tin Helm (Epic+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Smelt Thick Copper Bar", ip: 1},
        {compName: "Smelt Thick Tin Bar", ip: 1},
        {compName: "Smelt Golemite Ore", ip: 2},
    ]},
    "Copper-Tin Shield (Epic+)": {ts: tsSmi, fl: "C", op: 1, components: [
        {compName: "Smelt Thick Copper Bar", ip: 1},
        {compName: "Smelt Thick Tin Bar", ip: 2},
        {compName: "Smelt Golemite Ore", ip: 2},
    ]},
    "Crystal Key": {ts: tsSmi, fl: "C", op: 5, components: [
        {compName: "Crystal", ip: 12},
    ]},
    "Dark Golemite Key": {ts: tsSmi, fl: "D", op: 5, components: [
        {compName: "Smelt Dark Golemite", ip: 3},
    ]},
    "First Pumpkin Token": {ts: tsSmi, fl: "F", op: 1, components: []},
    "Form Chromium Nails": {ts: tsSmi, fl: "F", op: 15, display: "Chromium Nail", components: [
        {compName: "Smelt Chromium Bar", ip: 1},
    ]},
    "Form Copper Nails": {ts: tsSmi, fl: "F", op: 15, display: "Copper Nail", components: [
        {compName: "Smelt Copper Bar", ip: 1},
    ]},
    "Form Iron Nails": {ts: tsSmi, fl: "F", op: 15, display: "Iron Nail", components: [
        {compName: "Smelt Iron Bar", ip: 1},
    ]},
    "Form Tin Nails": {ts: tsSmi, fl: "F", op: 15, display: "Tin Nail", components: [
        {compName: "Smelt Tin Bar", ip: 1},
    ]},
    "Gold Fencing": {ts: tsSmi, fl: "G", op: 1, components: [
        {compName: "Gold Wire", ip: 4},
    ]},
    "Gold Necklace": {ts: tsSmi, fl: "G", op: 1, components: [
        {compName: "Gold Link", ip: 6},
    ]},
    "Golemite Key": {ts: tsSmi, fl: "G", op: 5, components: [
        {compName: "Smelt Golemite Ore", ip: 3},
    ]},
    "Golemite Shortsword (Epic+)": {ts: tsSmi, fl: "G", op: 1, components: [
        {compName: "Smelt Golemite Ore", ip: 5},
    ]},
    "Hard Chrome Legs (Epic+)": {ts: tsSmi, fl: "H", op: 1, components: [
        {compName: "Lava Rock", ip: 4},
        {compName: "Smelt Chrome Golemite Bar", ip: 3},
    ]},
    "Hard Chrome Mace (Epic+)": {ts: tsSmi, fl: "H", op: 1, components: [
        {compName: "Lava Rock", ip: 4},
        {compName: "Smelt Chrome Golemite Bar", ip: 3},
    ]},
    "Hard Cobalt Legs (Epic+)": {ts: tsSmi, fl: "H", op: 1, components: [
        {compName: "Smelt Thick Cobalt Bar", ip: 4},
        {compName: "Smelt Golemite Ore", ip: 3},
    ]},
    "Hard Cobalt Shield (Epic+)": {ts: tsSmi, fl: "H", op: 1, components: [
        {compName: "Slate", ip: 4},
        {compName: "Smelt Thick Cobalt Bar", ip: 2},
        {compName: "Smelt Thick Iron Bar", ip: 2},
        {compName: "Smelt Golemite Ore", ip: 3},
    ]},
    "Holiday Elk Token": {ts: tsSmi, fl: "H", op: 1, components: []},
    "Iridium Armplates": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 1},
    ]},
    "Iridium Axe Blade": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Quartz", ip: 4},
        {compName: "Pure Iridium", ip: 2},
        {compName: "Flame Pearl", ip: 1},
    ]},
    "Iridium Boots (Rare+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Quartz", ip: 1},
        {compName: "Thick Iridium Bar", ip: 3},
    ]},
    "Iridium Chestplate (Superior+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 1},
    ]},
    "Iridium Crafting Blade": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Quartz", ip: 3},
        {compName: "Pure Iridium", ip: 1},
        {compName: "Devilweed Acid", ip: 4},
    ]},
    "Iridium Fitting": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 1},
    ]},
    "Iridium Flame Helm": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 6},
        {compName: "Flame Pearl", ip: 4},
    ]},
    "Iridium Flame Sword": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 8},
        {compName: "Flame Pearl", ip: 5},
    ]},
    "Iridium Gauntlets (Superior+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 1},
    ]},
    "Iridium Hammer Head": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Quartz", ip: 4},
        {compName: "Thick Iridium Bar", ip: 2},
        {compName: "Flame Pearl", ip: 1},
    ]},
    "Iridium Helm (Superior+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 1},
    ]},
    "Iridium Key": {ts: tsSmi, fl: "I", op: 5, components: [
        {compName: "Smelt Iridium Bar", ip: 3},
    ]},
    "Iridium Legplates (Superior+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 1},
    ]},
    "Iridium Logging Axe": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Fine Aspen Handle", ip: 1},
        {compName: "Iridium Ore", ip: 2},
        {compName: "Iridium Axe Blade", ip: 1},
    ]},
    "Iridium Nails": {ts: tsSmi, fl: "I", op: 15, display: "Iridium Nail", components: [
        {compName: "Smelt Iridium Bar", ip: 1},
    ]},
    "Iridium Pick Blade": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Quartz", ip: 4},
        {compName: "Smelt Iridium Bar", ip: 4},
    ]},
    "Iridium Shield (Rare+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Quartz", ip: 3},
        {compName: "Thick Iridium Bar", ip: 3},
    ]},
    "Iridium Shovel Blade": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Quartz", ip: 3},
        {compName: "Thick Iridium Bar", ip: 2},
        {compName: "Dragontear Acid", ip: 4},
    ]},
    "Iridium Smithing Hammer": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Iridium Ore", ip: 1},
        {compName: "Iridium Hammer Head", ip: 1},
        {compName: "Wild Cherry Handle", ip: 1},
    ]},
    "Iridium Staff Base": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Thick Iridium Bar", ip: 1},
        {compName: "Rough Sharkwood Rod", ip: 1},
    ]},
    "Iron Axe Blade": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Shale Rock", ip: 2},
        {compName: "Citrine", ip: 1},
        {compName: "Pure Iron", ip: 2},
    ]},
    "Iron Broadsword (Rare+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Shale Rock", ip: 5},
        {compName: "Malachite", ip: 2},
        {compName: "Smelt Thick Iron Bar", ip: 5},
    ]},
    "Iron Chest-guard (Epic+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Smelt Thick Iron Bar", ip: 3},
        {compName: "Smelt Golemite Ore", ip: 3},
    ]},
    "Iron Crafting Blade": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Shale Rock", ip: 3},
        {compName: "Pure Iron", ip: 1},
        {compName: "Dragontear Acid", ip: 2},
    ]},
    "Iron Fitting": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Smelt Thick Iron Bar", ip: 1},
    ]},
    "Iron Hammer Head": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Shale Rock", ip: 2},
        {compName: "Citrine", ip: 1},
        {compName: "Smelt Thick Iron Bar", ip: 2},
    ]},
    "Iron Key": {ts: tsSmi, fl: "I", op: 5, components: [
        {compName: "Smelt Iron Bar", ip: 3},
    ]},
    "Iron Logging Axe": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Iron Ore", ip: 2},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Iron Axe Blade", ip: 1},
    ]},
    "Iron Pick Blade": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Shale Rock", ip: 4},
        {compName: "Smelt Iron Bar", ip: 4},
    ]},
    "Iron Shovel Blade": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Shale Rock", ip: 3},
        {compName: "Smelt Thick Iron Bar", ip: 2},
        {compName: "Dragontear Acid", ip: 2},
    ]},
    "Iron Smithing Hammer": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Iron Ore", ip: 2},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Iron Hammer Head", ip: 1},
    ]},
    "Iron Staff Base": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Rough Cedar Rod", ip: 1},
        {compName: "Smelt Thick Iron Bar", ip: 1},
    ]},
    "Iron War Helm (Rare+)": {ts: tsSmi, fl: "I", op: 1, components: [
        {compName: "Shale Rock", ip: 3},
        {compName: "Malachite", ip: 1},
        {compName: "Smelt Thick Iron Bar", ip: 3},
    ]},
    "Large Arrow Head": {ts: tsSmi, fl: "L", op: 1, components: [
        {compName: "Raw Arrow Head", ip: 5},
    ]},
    "Manganese Armplates": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Smelt Manganese Bar", ip: 1},
    ]},
    "Manganese Axe Blade": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Obsidian", ip: 4},
        {compName: "Pure Manganese", ip: 2},
        {compName: "Ruby", ip: 3},
    ]},
    "Manganese Boots (Rare+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Obsidian", ip: 1},
        {compName: "Thick Manganese Bar", ip: 3},
    ]},
    "Manganese Chestplate (Superior+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Manganese Bar", ip: 1},
    ]},
    "Manganese Crafting Blade": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Obsidian", ip: 4},
        {compName: "Pure Manganese", ip: 1},
        {compName: "Serpent Vine Acid", ip: 4},
    ]},
    "Manganese Fitting": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Manganese Bar", ip: 1},
    ]},
    "Manganese Gauntlets (Superior+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Manganese Bar", ip: 1},
    ]},
    "Manganese Hammer Head": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Obsidian", ip: 4},
        {compName: "Ruby", ip: 4},
        {compName: "Thick Manganese Bar", ip: 2},
    ]},
    "Manganese Helm (Superior+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Manganese Bar", ip: 1},
    ]},
    "Manganese Key": {ts: tsSmi, fl: "M", op: 5, components: [
        {compName: "Smelt Manganese Bar", ip: 3},
    ]},
    "Manganese Legplates (Superior+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Manganese Bar", ip: 1},
    ]},
    "Manganese Logging Axe": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Smelt Manganese Bar", ip: 2},
        {compName: "Manganese Axe Blade", ip: 1},
        {compName: "Maidenhair Handle", ip: 1},
    ]},
    "Manganese Nails": {ts: tsSmi, fl: "M", op: 15, display: "Manganese Nail", components: [
        {compName: "Smelt Manganese Bar", ip: 1},
    ]},
    "Manganese Pick Blade": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Obsidian", ip: 4},
        {compName: "Smelt Manganese Bar", ip: 4},
    ]},
    "Manganese Ruby Gloves": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Ruby", ip: 7},
        {compName: "Thick Manganese Bar", ip: 6},
    ]},
    "Manganese Ruby Sword": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Ruby", ip: 12},
        {compName: "Thick Manganese Bar", ip: 8},
    ]},
    "Manganese Shield (Rare+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Obsidian", ip: 3},
        {compName: "Thick Manganese Bar", ip: 3},
    ]},
    "Manganese Shovel Blade": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Obsidian", ip: 4},
        {compName: "Thick Manganese Bar", ip: 2},
        {compName: "Serpent Vine Acid", ip: 2},
    ]},
    "Manganese Smithing Hammer": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Smelt Manganese Bar", ip: 2},
        {compName: "Manganese Hammer Head", ip: 1},
        {compName: "Maidenhair Handle", ip: 1},
    ]},
    "Manganese Staff Base": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Manganese Bar", ip: 1},
        {compName: "Rough White Willow Rod", ip: 1},
    ]},
    "Mithril Armplates": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Mithril Bar", ip: 1},
    ]},
    "Mithril Axe Blade": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Pyrestone", ip: 4},
        {compName: "Pure Mithril", ip: 2},
        {compName: "Sapphire", ip: 3},
    ]},
    "Mithril Boots (Rare+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Pyrestone", ip: 1},
        {compName: "Thick Mithril Bar", ip: 3},
    ]},
    "Mithril Chestguard (Superior+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Mithril Bar", ip: 1},
    ]},
    "Mithril Crafting Blade": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Pyrestone", ip: 4},
        {compName: "Pure Mithril", ip: 1},
        {compName: "Black Lotus Acid", ip: 4},
    ]},
    "Mithril Fitting": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Mithril Bar", ip: 1},
    ]},
    "Mithril Gauntlets (Superior+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Mithril Bar", ip: 1},
    ]},
    "Mithril Hammer Head": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Pyrestone", ip: 4},
        {compName: "Sapphire", ip: 4},
        {compName: "Thick Mithril Bar", ip: 2},
    ]},
    "Mithril Helm (Superior+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Mithril Bar", ip: 1},
    ]},
    "Mithril Key": {ts: tsSmi, fl: "M", op: 5, components: [
        {compName: "Smelt Mithril Bar", ip: 3},
    ]},
    "Mithril Legplates (Superior+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Mithril Bar", ip: 1},
    ]},
    "Mithril Logging Axe": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Smelt Mithril Bar", ip: 2},
        {compName: "Mithril Axe Blade", ip: 1},
        {compName: "Mahogany Handle", ip: 1},
    ]},
    "Mithril Nails": {ts: tsSmi, fl: "M", op: 15, display: "Mithril Nail", components: [
        {compName: "Smelt Mithril Bar", ip: 1},
    ]},
    "Mithril Pick Blade": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Pyrestone", ip: 4},
        {compName: "Smelt Mithril Bar", ip: 4},
    ]},
    "Mithril Sapphire Gloves": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Sapphire", ip: 7},
        {compName: "Thick Mithril Bar", ip: 6},
    ]},
    "Mithril Sapphire Sword": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Sapphire", ip: 12},
        {compName: "Thick Mithril Bar", ip: 8},
    ]},
    "Mithril Shield (Rare+)": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Pyrestone", ip: 3},
        {compName: "Thick Mithril Bar", ip: 3},
    ]},
    "Mithril Shovel Blade": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Pyrestone", ip: 2},
        {compName: "Thick Mithril Bar", ip: 2},
        {compName: "Black Lotus Acid", ip: 4},
    ]},
    "Mithril Smithing Hammer": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Smelt Mithril Bar", ip: 2},
        {compName: "Mithril Hammer Head", ip: 1},
        {compName: "Mahogany Handle", ip: 1},
    ]},
    "Mithril Staff Base": {ts: tsSmi, fl: "M", op: 1, components: [
        {compName: "Thick Mithril Bar", ip: 1},
        {compName: "Rough Rosewood Rod", ip: 1},
    ]},
    "Neck Bolts": {ts: tsSmi, fl: "N", op: 1, components: [
        {compName: "Bolt Mould", ip: 2},
        {compName: "Silver Ore", ip: 8},
    ]},
    "Obsidian Key": {ts: tsSmi, fl: "O", op: 5, components: [
        {compName: "Obsidian", ip: 12},
    ]},
    "Peppermint Bark Token": {ts: tsSmi, fl: "P", op: 1, components: []},
    "Pure Adamantite": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Adamantite Bar", ip: 8},
    ]},
    "Pure Bloodstone": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Bloodstone Bar", ip: 8},
    ]},
    "Pure Chromium": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Chromium Bar", ip: 8},
    ]},
    "Pure Cobalt": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Cobalt Bar", ip: 8},
    ]},
    "Pure Copper": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Copper Bar", ip: 8},
    ]},
    "Pure Iridium": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Iridium Bar", ip: 8},
    ]},
    "Pure Iron": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Iron Bar", ip: 8},
    ]},
    "Pure Manganese": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Manganese Bar", ip: 8},
    ]},
    "Pure Mithril": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Mithril Bar", ip: 8},
    ]},
    "Pure Tin": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Tin Bar", ip: 8},
    ]},
    "Pure Titanium": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Titanium Bar", ip: 8},
    ]},
    "Pure Tourmaline": {ts: tsSmi, fl: "P", op: 1, components: [
        {compName: "Smelt Tourmaline Bar", ip: 8},
    ]},
    "Pyrestone Key": {ts: tsSmi, fl: "P", op: 5, components: [
        {compName: "Pyrestone", ip: 12},
    ]},
    "Red Golemite Key": {ts: tsSmi, fl: "R", op: 5, components: [
        {compName: "Smelt Red Golemite", ip: 3},
    ]},
    "Second Pumpkin Token": {ts: tsSmi, fl: "S", op: 1, components: []},
    "Sharpening Stone": {ts: tsSmi, fl: "S", op: 1, components: []},
    "Shovel Scoop": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Scrap Metal", ip: 8},
    ]},
    "Silver Key": {ts: tsSmi, fl: "S", op: 3, components: [
        {compName: "Dark Silver", ip: 12},
    ]},
    "Small Iron Gate": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Horsetail Sandpaper", ip: 3},
        {compName: "Smelt Thick Iron Bar", ip: 2},
    ]},
    "Smelt Adamantite Bar": {ts: tsSmi, fl: "S", op: 1, display: "Adamantite Bar", components: [
        {compName: "Basalt", ip: 2},
        {compName: "Adamantite Ore", ip: 1},
    ]},
    "Smelt Blood Golemite Bar": {ts: tsSmi, fl: "S", op: 1, display: "Blood Golemite Bar", components: [
        {compName: "Thick Bloodstone Bar", ip: 3},
        {compName: "Smelt Golemite Ore", ip: 1},
    ]},
    "Smelt Bloodstone Bar": {ts: tsSmi, fl: "S", op: 1, display: "Bloodstone Bar", components: [
        {compName: "Lava Rock", ip: 2},
        {compName: "Bloodstone Ore", ip: 1},
    ]},
    "Smelt Chrome Golemite Bar": {ts: tsSmi, fl: "S", op: 1, display: "Chrome Golemite Bar", components: [
        {compName: "Smelt Thick Chromium Bar", ip: 3},
        {compName: "Smelt Golemite Ore", ip: 1},
    ]},
    "Smelt Chromium Bar": {ts: tsSmi, fl: "S", op: 1, display: "Chromium Bar", components: [
        {compName: "Marble", ip: 2},
        {compName: "Chromium Ore", ip: 1},
    ]},
    "Smelt Cobalt Bar": {ts: tsSmi, fl: "S", op: 1, display: "Cobalt Bar", components: [
        {compName: "Slate", ip: 2},
        {compName: "Cobalt Ore", ip: 1},
    ]},
    "Smelt Copper Bar": {ts: tsSmi, fl: "S", op: 1, display: "Copper Bar", components: [
        {compName: "Stone", ip: 1},
        {compName: "Copper Ore", ip: 1},
    ]},
    "Smelt Dark Golemite": {ts: tsSmi, fl: "S", op: 1, display: "Dark Golemite Bar", components: [
        {compName: "Smelt Mithril Bar", ip: 4},
        {compName: "Smelt Titanium Bar", ip: 4},
        {compName: "Dark Golemite Ore", ip: 3},
    ]},
    "Smelt Golemite Ore": {ts: tsSmi, fl: "S", op: 1, display: "Golemite Bar", components: [
        {compName: "Smelt Copper Bar", ip: 2},
        {compName: "Smelt Iron Bar", ip: 2},
        {compName: "Smelt Tin Bar", ip: 2},
        {compName: "Golemite Ore", ip: 3},
    ]},
    "Smelt Iridium Bar": {ts: tsSmi, fl: "S", op: 1, display: "Iridium Bar", components: [
        {compName: "Quartz", ip: 2},
        {compName: "Iridium Ore", ip: 1},
    ]},
    "Smelt Iron Bar": {ts: tsSmi, fl: "S", op: 1, display: "Iron Bar", components: [
        {compName: "Shale Rock", ip: 1},
        {compName: "Iron Ore", ip: 1},
    ]},
    "Smelt Manganese Bar": {ts: tsSmi, fl: "S", op: 1, display: "Manganese Bar", components: [
        {compName: "Obsidian", ip: 2},
        {compName: "Manganese Ore", ip: 1},
    ]},
    "Smelt Mithril Bar": {ts: tsSmi, fl: "S", op: 1, display: "Mithril Bar", components: [
        {compName: "Pyrestone", ip: 2},
        {compName: "Mithril Ore", ip: 1},
    ]},
    "Smelt Red Golemite": {ts: tsSmi, fl: "S", op: 1, display: "Red Golemite Bar", components: [
        {compName: "Smelt Manganese Bar", ip: 4},
        {compName: "Dragonstone", ip: 1},
        {compName: "Red Golemite Ore", ip: 3},
    ]},
    "Smelt Steel Bar": {ts: tsSmi, fl: "S", op: 1, display: "Steel Bar", components: [
        {compName: "Iron Ore", ip: 2},
        {compName: "Silver Ore", ip: 2},
        {compName: "Molten Fossil", ip: 4},
    ]},
    "Smelt Thick Bronze Bar": {ts: tsSmi, fl: "S", op: 1, display: "Thick Bronze Bar", components: [
        {compName: "Smelt Copper Bar", ip: 5},
        {compName: "Smelt Tin Bar", ip: 5},
    ]},
    "Smelt Thick Chromium Bar": {ts: tsSmi, fl: "S", op: 1, display: "Thick Chromium Bar", components: [
        {compName: "Marble", ip: 5},
        {compName: "Smelt Chromium Bar", ip: 5},
    ]},
    "Smelt Thick Cobalt Bar": {ts: tsSmi, fl: "S", op: 1, display: "Thick Cobalt Bar", components: [
        {compName: "Slate", ip: 5},
        {compName: "Smelt Cobalt Bar", ip: 5},
    ]},
    "Smelt Thick Copper Bar": {ts: tsSmi, fl: "S", op: 1, display: "Thick Copper Bar", components: [
        {compName: "Stone", ip: 5},
        {compName: "Smelt Copper Bar", ip: 5},
    ]},
    "Smelt Thick Iron Bar": {ts: tsSmi, fl: "S", op: 1, display: "Thick Iron Bar", components: [
        {compName: "Shale Rock", ip: 5},
        {compName: "Smelt Iron Bar", ip: 5},
    ]},
    "Smelt Thick Tin Bar": {ts: tsSmi, fl: "S", op: 1, display: "Thick Tin Bar", components: [
        {compName: "Sandstone", ip: 5},
        {compName: "Smelt Tin Bar", ip: 5},
    ]},
    "Smelt Tin Bar": {ts: tsSmi, fl: "S", op: 1, display: "Tin Bar", components: [
        {compName: "Sandstone", ip: 1},
        {compName: "Tin Ore", ip: 1},
    ]},
    "Smelt Titanium Bar": {ts: tsSmi, fl: "S", op: 1, display: "Titanium Bar", components: [
        {compName: "Crystal", ip: 2},
        {compName: "Titanium Ore", ip: 1},
    ]},
    "Smelt Tourmaline Bar": {ts: tsSmi, fl: "S", op: 1, display: "Tourmaline Bar", components: [
        {compName: "Dark Silver", ip: 2},
        {compName: "Tourmaline Ore", ip: 1},
    ]},
    "Smelt Wulfenite": {ts: tsSmi, fl: "S", op: 1, display: "Wulfenite Bar", components: [
        {compName: "Smelt Tourmaline Bar", ip: 4},
        {compName: "Emberstone", ip: 1},
        {compName: "Wulfenite Ore", ip: 3},
    ]},
    "Steel Razor Blade": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Sharpening Stone", ip: 2},
        {compName: "Smelt Steel Bar", ip: 1},
    ]},
    "Superior Cobalt Hands": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Slate", ip: 2},
        {compName: "Smelt Iron Bar", ip: 3},
        {compName: "Smelt Thick Cobalt Bar", ip: 1},
    ]},
    "Superior Copper Arms": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Smelt Copper Bar", ip: 3},
    ]},
    "Superior Copper Chest": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Stone", ip: 3},
        {compName: "Smelt Copper Bar", ip: 3},
    ]},
    "Superior Copper Helm": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Stone", ip: 3},
        {compName: "Smelt Copper Bar", ip: 2},
    ]},
    "Superior Copper Legs": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Stone", ip: 2},
        {compName: "Smelt Copper Bar", ip: 4},
    ]},
    "Superior Iron Chestguard": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Shale Rock", ip: 2},
        {compName: "Smelt Thick Iron Bar", ip: 1},
    ]},
    "Superior Iron Legplate": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Shale Rock", ip: 2},
        {compName: "Smelt Thick Iron Bar", ip: 1},
    ]},
    "Superior Tin Chest": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Sandstone", ip: 2},
        {compName: "Smelt Tin Bar", ip: 4},
    ]},
    "Superior Tin Feet": {ts: tsSmi, fl: "S", op: 1, components: [
        {compName: "Sandstone", ip: 2},
        {compName: "Smelt Tin Bar", ip: 3},
    ]},
    "Thick Adamantite Bar": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Basalt", ip: 5},
        {compName: "Smelt Adamantite Bar", ip: 5},
    ]},
    "Thick Blood Shield (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Lava Rock", ip: 3},
        {compName: "Thick Bloodstone Bar", ip: 3},
    ]},
    "Thick Bloodstone Bar": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Lava Rock", ip: 4},
        {compName: "Smelt Bloodstone Bar", ip: 5},
    ]},
    "Thick Chrome Arms (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Marble", ip: 3},
        {compName: "Smelt Thick Chromium Bar", ip: 3},
    ]},
    "Thick Iridium Bar": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Quartz", ip: 5},
        {compName: "Smelt Iridium Bar", ip: 5},
    ]},
    "Thick Manganese Bar": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Obsidian", ip: 5},
        {compName: "Smelt Manganese Bar", ip: 5},
    ]},
    "Thick Mithril Bar": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Pyrestone", ip: 5},
        {compName: "Smelt Mithril Bar", ip: 5},
    ]},
    "Thick Titanium Bar": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Crystal", ip: 5},
        {compName: "Smelt Titanium Bar", ip: 5},
    ]},
    "Thick Tourmaline Bar": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Dark Silver", ip: 5},
        {compName: "Smelt Tourmaline Bar", ip: 5},
    ]},
    "Third Pumpkin Token": {ts: tsSmi, fl: "T", op: 1, components: []},
    "Tin Axe Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Sandstone", ip: 2},
        {compName: "Agate", ip: 1},
        {compName: "Pure Tin", ip: 2},
    ]},
    "Tin Battle Axe": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Sandstone", ip: 2},
        {compName: "Smelt Thick Tin Bar", ip: 1},
    ]},
    "Tin Battle Helm (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Agate", ip: 2},
        {compName: "Citrine", ip: 2},
        {compName: "Smelt Thick Tin Bar", ip: 3},
    ]},
    "Tin Battleplate (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Agate", ip: 2},
        {compName: "Smelt Thick Tin Bar", ip: 3},
    ]},
    "Tin Crafting Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Sandstone", ip: 3},
        {compName: "Pure Tin", ip: 1},
        {compName: "Dragontear Acid", ip: 2},
    ]},
    "Tin Fitting": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Smelt Thick Tin Bar", ip: 1},
    ]},
    "Tin Hammer Head": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Sandstone", ip: 2},
        {compName: "Agate", ip: 1},
        {compName: "Smelt Thick Tin Bar", ip: 2},
    ]},
    "Tin Key": {ts: tsSmi, fl: "T", op: 4, components: [
        {compName: "Smelt Tin Bar", ip: 2},
    ]},
    "Tin Leggings (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Agate", ip: 1},
        {compName: "Smelt Thick Tin Bar", ip: 3},
    ]},
    "Tin Logging Axe": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Tin Ore", ip: 2},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Tin Axe Blade", ip: 1},
    ]},
    "Tin Longsword (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Sandstone", ip: 4},
        {compName: "Agate", ip: 4},
        {compName: "Smelt Thick Tin Bar", ip: 3},
    ]},
    "Tin Pick Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Sandstone", ip: 4},
        {compName: "Smelt Tin Bar", ip: 4},
    ]},
    "Tin Shovel Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Sandstone", ip: 2},
        {compName: "Smelt Thick Tin Bar", ip: 2},
        {compName: "Dragontear Acid", ip: 2},
    ]},
    "Tin Smithing Hammer": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Tin Ore", ip: 2},
        {compName: "Fine Spruce Handle", ip: 1},
        {compName: "Tin Hammer Head", ip: 1},
    ]},
    "Tin Staff Base": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Rough Birch Rod", ip: 1},
        {compName: "Smelt Thick Tin Bar", ip: 1},
    ]},
    "Titanium Armplates": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Titanium Bar", ip: 1},
    ]},
    "Titanium Axe Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Crystal", ip: 4},
        {compName: "Emerald", ip: 3},
        {compName: "Pure Titanium", ip: 2},
    ]},
    "Titanium Boots (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Crystal", ip: 1},
        {compName: "Thick Titanium Bar", ip: 3},
    ]},
    "Titanium Chestplate (Superior+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Titanium Bar", ip: 1},
    ]},
    "Titanium Crafting Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Crystal", ip: 4},
        {compName: "Pure Titanium", ip: 1},
        {compName: "Venomweed Acid", ip: 4},
    ]},
    "Titanium Emerald Gloves": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Emerald", ip: 7},
        {compName: "Thick Titanium Bar", ip: 6},
    ]},
    "Titanium Emerald Sword": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Emerald", ip: 12},
        {compName: "Thick Titanium Bar", ip: 8},
    ]},
    "Titanium Fitting": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Titanium Bar", ip: 1},
    ]},
    "Titanium Gauntlets (Superior+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Titanium Bar", ip: 1},
    ]},
    "Titanium Hammer Head": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Crystal", ip: 4},
        {compName: "Emerald", ip: 4},
        {compName: "Thick Titanium Bar", ip: 2},
    ]},
    "Titanium Helm (Superior+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Titanium Bar", ip: 1},
    ]},
    "Titanium Key": {ts: tsSmi, fl: "T", op: 5, components: [
        {compName: "Smelt Titanium Bar", ip: 3},
    ]},
    "Titanium Legplates (Superior+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Titanium Bar", ip: 1},
    ]},
    "Titanium Logging Axe": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Smelt Titanium Bar", ip: 2},
        {compName: "Titanium Axe Blade", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Titanium Nails": {ts: tsSmi, fl: "T", op: 1, display: "Titanium Nail", components: [
        {compName: "Smelt Titanium Bar", ip: 1},
    ]},
    "Titanium Pick Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Crystal", ip: 4},
        {compName: "Smelt Titanium Bar", ip: 4},
    ]},
    "Titanium Shield (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Crystal", ip: 3},
        {compName: "Thick Titanium Bar", ip: 3},
    ]},
    "Titanium Shovel Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Crystal", ip: 2},
        {compName: "Thick Titanium Bar", ip: 2},
        {compName: "Venomweed Acid", ip: 4},
    ]},
    "Titanium Smithing Hammer": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Smelt Titanium Bar", ip: 2},
        {compName: "Titanium Hammer Head", ip: 1},
        {compName: "Dark Cherry Handle", ip: 1},
    ]},
    "Titanium Staff Base": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Titanium Bar", ip: 1},
        {compName: "Rough Satinwood Rod", ip: 1},
    ]},
    "Tourmaline Armplates": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Tourmaline Bar", ip: 1},
    ]},
    "Tourmaline Axe Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Dark Silver", ip: 4},
        {compName: "Carnelian", ip: 3},
        {compName: "Pure Tourmaline", ip: 2},
    ]},
    "Tourmaline Boots (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Dark Silver", ip: 1},
        {compName: "Thick Tourmaline Bar", ip: 3},
    ]},
    "Tourmaline Carnelian Gloves": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Carnelian", ip: 7},
        {compName: "Thick Tourmaline Bar", ip: 6},
    ]},
    "Tourmaline Carnelian Sword": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Carnelian", ip: 12},
        {compName: "Thick Tourmaline Bar", ip: 8},
    ]},
    "Tourmaline Crafting Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Dark Silver", ip: 4},
        {compName: "Pure Tourmaline", ip: 1},
        {compName: "Flame Lotus Acid", ip: 4},
    ]},
    "Tourmaline Fitting": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Tourmaline Bar", ip: 1},
    ]},
    "Tourmaline Gauntlets (Superior+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Tourmaline Bar", ip: 1},
    ]},
    "Tourmaline Hammer Head": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Dark Silver", ip: 4},
        {compName: "Carnelian", ip: 4},
        {compName: "Thick Tourmaline Bar", ip: 2},
    ]},
    "Tourmaline Helm (Superior+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Tourmaline Bar", ip: 1},
    ]},
    "Tourmaline Key": {ts: tsSmi, fl: "T", op: 3, components: [
        {compName: "Smelt Tourmaline Bar", ip: 3},
    ]},
    "Tourmaline Legplates (Superior+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Tourmaline Bar", ip: 1},
    ]},
    "Tourmaline Logging Axe": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Smelt Tourmaline Bar", ip: 2},
        {compName: "Tourmaline Axe Blade", ip: 1},
        {compName: "Elven Walnut Handle", ip: 1},
    ]},
    "Tourmaline Nails": {ts: tsSmi, fl: "T", op: 15, display: "Tournaline Nail", components: [
        {compName: "Smelt Tourmaline Bar", ip: 1},
    ]},
    "Tourmaline Pick Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Dark Silver", ip: 4},
        {compName: "Smelt Tourmaline Bar", ip: 4},
    ]},
    "Tourmaline Shield (Rare+)": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Dark Silver", ip: 1},
        {compName: "Thick Tourmaline Bar", ip: 3},
    ]},
    "Tourmaline Shovel Blade": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Dark Silver", ip: 2},
        {compName: "Thick Tourmaline Bar", ip: 2},
        {compName: "Flame Lotus Acid", ip: 4},
    ]},
    "Tourmaline Smithing Hammer": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Smelt Tourmaline Bar", ip: 2},
        {compName: "Tourmaline Hammer Head", ip: 1},
        {compName: "Elven Walnut Handle", ip: 1},
    ]},
    "Tourmaline Staff Base": {ts: tsSmi, fl: "T", op: 1, components: [
        {compName: "Thick Tourmaline Bar", ip: 1},
        {compName: "Rough Tilia Cordata Rod", ip: 1},
    ]},
    "Wulfenite Key": {ts: tsSmi, fl: "W", op: 3, components: [
        {compName: "Smelt Wulfenite", ip: 4},
    ]},
    // treasure hunting
    "Ancient Chest": {ts: tsTre, fl: "A", op: 1, components: []},
    "Barnicle Chest": {ts: tsTre, fl: "B", op: 1, components: []},
    "Battered Chest": {ts: tsTre, fl: "B", op: 1, components: []},
    "Blackened Chest": {ts: tsTre, fl: "B", op: 1, components: []},
    "Candle": {ts: tsTre, fl: "C", op: 1, components: []},
    "Candy Cane": {ts: tsTre, fl: "C", op: 1, components: []},
    "Carniverous Chest": {ts: tsTre, fl: "C", op: 1, components: []},
    "Champion's Chest": {ts: tsTre, fl: "C", op: 1, components: []},
    "Chest of Cruelty": {ts: tsTre, fl: "C", op: 1, components: []},
    "Chest of Mourning": {ts: tsTre, fl: "C", op: 1, components: []},
    "Chest of the Damned": {ts: tsTre, fl: "C", op: 1, components: []},
    "Chest of the Golem": {ts: tsTre, fl: "C", op: 1, components: []},
    "Clawtooth Chest": {ts: tsTre, fl: "C", op: 1, components: []},
    "Diamond Arrow Shaft": {ts: tsTre, fl: "D", op: 1, components: []},
    "Diamond Egg": {ts: tsTre, fl: "D", op: 1, components: []},
    "Dragonscale Chest": {ts: tsTre, fl: "D", op: 1, components: []},
    "Enchanted Chest": {ts: tsTre, fl: "E", op: 1, components: []},
    "Gem Encrusted Chest": {ts: tsTre, fl: "G", op: 1, components: []},
    "Gold Wire": {ts: tsTre, fl: "G", op: 1, components: []},
    "Golden Bound Chest": {ts: tsTre, fl: "G", op: 1, components: []},
    "Grey Secure Tent": {ts: tsTre, fl: "G", op: 1, components: []},
    "Heart Candy - Pookie Bear": {ts: tsTre, fl: "H", op: 1, components: []},
    "Howling Chest": {ts: tsTre, fl: "H", op: 1, components: []},
    "Lava Etched Chest": {ts: tsTre, fl: "L", op: 1, components: []},
    "Living Chest": {ts: tsTre, fl: "L", op: 1, components: []},
    "Marble Chest": {ts: tsTre, fl: "M", op: 1, components: []},
    "Marble Ornament": {ts: tsTre, fl: "M", op: 1, components: []},
    "Mysterious Chest": {ts: tsTre, fl: "M", op: 1, components: []},
    "Mystical Chest": {ts: tsTre, fl: "M", op: 1, components: []},
    "Obsidian Chest": {ts: tsTre, fl: "O", op: 1, components: []},
    "Oozing Chest": {ts: tsTre, fl: "O", op: 1, components: []},
    "Red Rose": {ts: tsTre, fl: "R", op: 1, components: []},
    "Resplendent Chest": {ts: tsTre, fl: "R", op: 1, components: []},
    "Rusty Chest": {ts: tsTre, fl: "R", op: 1, components: []},
    "Screaming Chest": {ts: tsTre, fl: "S", op: 1, components: []},
    "Siren Chest": {ts: tsTre, fl: "S", op: 1, components: []},
    "Slime Covered Chest": {ts: tsTre, fl: "S", op: 1, components: []},
    "Small Chest": {ts: tsTre, fl: "S", op: 1, components: []},
    "Solid Chest": {ts: tsTre, fl: "S", op: 1, components: []},
    "Spring Jewels": {ts: tsTre, fl: "S", op: 1, components: []},
    "Stinky Chest": {ts: tsTre, fl: "S", op: 1, components: []},
    "Stone Chest": {ts: tsTre, fl: "S", op: 1, components: []},
    "Unholy Chest": {ts: tsTre, fl: "U", op: 1, components: []},
    "Untraceable Jewels": {ts: tsTre, fl: "U", op: 1, components: []},
    "Wolf Fur Chest": {ts: tsTre, fl: "W", op: 1, components: []},
    "Wraithfall Chest": {ts: tsTre, fl: "W", op: 1, components: []},
};

function getTradeskill() {
    return document.getElementById("tradeskill-select");
}
function getAlphabet() {
    return document.getElementById("alphabet-select")
}
function getPattern() {
    return document.getElementById("pattern-select");
}
function getQuantity() {
    return document.getElementById("quantity");
}
function getIncludeTradeskill() {
    return document.getElementById("include-tradeskill");
}
function getIncludeLinks() {
    return document.getElementById("include-links");
}

//preliminary processing; triggered by onLoad
function prelimProc() {
    var keys = Object.keys(dataForward).sort(); 

    keys.forEach(buildData);

    readUrl();
}
function buildData(name) {
    var compName;
    var compTrade;
    var i;
    
    if (dataTradeskill.hasOwnProperty(dataForward[name].ts)){
        dataTradeskill[dataForward[name].ts].push(name);
    } else {
        dataTradeskill[dataForward[name].ts] = [name];
    }

    if (dataAlphabet.hasOwnProperty(dataForward[name].fl)) {
        dataAlphabet[dataForward[name].fl].push(name);
    } else {
        dataAlphabet[dataForward[name].fl] = [name];
    }

    if (dataReverse.hasOwnProperty(name)){
        //it's already got a top-level entry
    } else {
        dataReverse[name] = {ts: dataForward[name].ts, targets: []};
    }

    for (i=0;i<dataForward[name].components.length;i++){
        compName = dataForward[name].components[i].compName;
        // console.log(compName); // FIXME for debugging new mats
        compTrade = dataForward[compName].ts;

        if (dataReverse.hasOwnProperty(compName)){
            dataReverse[compName].targets.push(name);
        } else {
            dataReverse[compName] = {ts: compTrade, targets: [name]};
        }
    }
}

//build tradeskill menus
function displayTradeskills() {
    var tradeskill = getTradeskill();
    var keys = Object.keys(dataTradeskill).sort();

    displayMenus(tradeskill, keys);
}
function displayAlphabets() {
    var alphabet = getAlphabet();
    var keys = Object.keys(dataAlphabet).sort();

    displayMenus(alphabet, keys);
}
function displayPatterns(source) {
    var tradeskill = getTradeskill();
    var alphabet = getAlphabet();
    var pattern = getPattern();
    var option;
    var keys;

    if (source === "tradeskill") {
        keys = dataTradeskill[tradeskill.value];
        alphabet.value = "";
    } else {
        keys = dataAlphabet[alphabet.value];
        tradeskill.value = "";
    }

    //clean out previous menu
    while (pattern.firstChild) {
        pattern.removeChild(pattern.firstChild);
    }
    option = document.createElement("OPTION");
    pattern.appendChild(option);

    displayMenus(pattern, keys);
    pattern.parentNode.style.display = "block";
}
function displayMenus(display, keys) {
    var option;
    var text;
    var i;

    for (i=0;i<keys.length;i++) {
        option = document.createElement("OPTION");
        text = document.createTextNode(keys[i]);
        option.appendChild(text);
        display.appendChild(option);
    }
}

//link sharing facilitation
function getQueryVariable(variable) {
    var query = decodeURI(window.location.search.substring(1));
    var vars = query.split("&");
    var i;
    var pair;
    var result = false;

    for (i=0;i<vars.length;i++) {
        pair = vars[i].split("=");
        if(pair[0] === variable) {
            result = pair[1];
        }
    }
    return(result);
}
function getQueryBooleanVariable(variable) {
    var raw = getQueryVariable(variable);
    var result = false;

    if (raw === "true") {
        result = true;
    }

    return(result);
}
function setUrl() {
    var url;
    var tradeskill = getTradeskill();
    var alphabet = getAlphabet();
    var pattern = getPattern();
    var quantity = getQuantity();
    var includeTradeskill = getIncludeTradeskill();
    var includeLinks = getIncludeLinks();

    if (window.location.protocol === "file:"){
        url = window.location.protocol;
        url += window.location.hostname;
        url += window.location.pathname;
    } else {
        url = "https://drakor.rose-labyrinth.com/pattern/";
    }

    url += "?t=" + tradeskill.value;
    url += "&a=" + alphabet.value;
    url += "&p=" + pattern.value;
    url += "&q=" + quantity.value;
    url += "&it=" + includeTradeskill.checked;
    url += "&il=" + includeLinks.checked;
    url = encodeURI(url);

    window.history.pushState({},null,url);
}
function readUrl() {
    var tradeskill = getTradeskill();
    var alphabet = getAlphabet();
    var pattern;
    var includeTradeskill;
    var includeLinks;
    var quantity;
    var source;

    displayTradeskills();
    tradeskill.value = getQueryVariable("t");

    displayAlphabets();
    alphabet.value = getQueryVariable("a");

    if ((tradeskill.value !== "") || (alphabet.value !== "")) {
        if (tradeskill.value !== ""){
            source = "tradeskill"
        }
        includeTradeskill = getIncludeTradeskill();
        includeTradeskill.checked = getQueryBooleanVariable("it");

        includeLinks = getIncludeLinks();
        includeLinks.checked = getQueryBooleanVariable("il");

        displayPatterns(source);
        pattern = getPattern();
        pattern.value = getQueryVariable("p");

        if (pattern.value !== "") {
            quantity = getQuantity();
            quantity.value = getQueryVariable("q");

            if (getQueryVariable("q") === "") {
                quantity.value = 1;
            }
            fetchPattern();
        }
    }
}

//building the pattern tree
function fetchPattern() {
    var pattern = getPattern().value;
    var quantity = getQuantity().value;
    var links = getIncludeLinks().checked;
    var element;
    var indent = 0;
    var display = document.getElementById("tree-display");

    cleanUpDisplay();

    traverseTree(pattern,quantity,indent);
    if (links) {
        element = displayLinkedTree("forward");
    } else {
        element = displayTree("forward");
    }
    element.id = "forward-tree";
    display.insertBefore(element, document.getElementById("tree-display-b"));
    patternTree = [];

    traverseRvTree(pattern,indent);
    if (links) {
        element = displayLinkedTree("reverse");
    } else {
        element = displayTree("reverse");
    }
    element.id = "reverse-tree";
    display.appendChild(element);
    patternTree = [];

    display.style.display = "block";
    setUrl();
}
function traverseTree(pattern,quantity,indent) {
    var tradeskill = dataForward[pattern].ts;
    var output = dataForward[pattern].op;
    var components = dataForward[pattern].components;
    var i;
    var compName;
    var compInput;
    var compQuan;

    buildTree(pattern,tradeskill,indent,quantity);

    for (i=0;i<components.length;i++) {
        compName = components[i].compName;
        compInput = components[i].ip;

        compQuan = calcQuan(output,compInput,quantity);
        
        traverseTree(compName,compQuan,indent+1);
    }
}
function traverseRvTree(pattern,indent) {
    var tradeskill = dataReverse[pattern].ts;
    var targets = dataReverse[pattern].targets;
    var i;

    buildRvTree(pattern,tradeskill,indent);

    for (i=0;i<targets.length;i++) {
        traverseRvTree(targets[i],indent+1);
    }

}
function buildTree(pattern, skill, indent, quantity) {
    var displayName = pattern;

    if (dataForward[pattern].hasOwnProperty("display")){
        displayName = dataForward[pattern].display;
    }

    patternTree.push({"pattern": displayName, "skill": skill, "quantity": quantity, "indent": indent});
}
function buildRvTree(pattern, skill, indent) {
    var defaultQuantity = 1;
    buildTree(pattern, skill, indent, defaultQuantity);
}
function calcQuan(output,input,quantity) {
    var result;

    if (output == 1) {
        result = input * quantity;
    } else {
        result = Math.ceil(quantity*input/output);

        if (result % input !== 0){
            result = (Math.floor(result/input) + 1) * input;
        }
    }

    return result;
}
function displayTree(direction) {
    var includeTradeskill = getIncludeTradeskill();
    var indentChar = "\t";
    var tree = "";
    var display = document.createElement("PRE");
    var i, j;
    var indent;

    for (i=0;i<patternTree.length;i++){
        indent = "";

        for (j=0;j<patternTree[i]["indent"];j++){
            indent += indentChar;
        }
        tree += indent + "[" + patternTree[i]["pattern"] + "]";

        if (direction === "forward"){
            tree += " x" + patternTree[i]["quantity"].toLocaleString("en");
        }

        if (includeTradeskill.checked){
            tree += " (" + patternTree[i]["skill"] + ")";
        }

        tree += "\n";
    }

    display.appendChild(document.createTextNode(tree));

    return display;
}
function displayLinkedTree(direction) {
    var includeTradeskill = getIncludeTradeskill();
    var includeLinks = getIncludeLinks();
    var tree = document.createElement("DIV");
    var text;
    var line;
    var link;
    var source;
    var i;
    var indent = 40;

    for (i=0;i<patternTree.length;i++){
        text = "";
        line = "";
        link = "";

        line = document.createElement("DIV");
        line.style.textIndent = (patternTree[i]["indent"] * indent) + "px";

        line.appendChild(document.createTextNode("["));

        link = document.createElement("A");

        source = "../pattern/index.html";
        source += "?t=" + patternTree[i]["skill"];
        source += "&p=" + patternTree[i]["pattern"];
        source += "&q=" + 1;
        source += "&it=" + includeTradeskill.checked;
        source += "&il=" + includeLinks.checked;
        link.href = encodeURI(source);

        text = patternTree[i]["pattern"];
        link.appendChild(document.createTextNode(text));

        text = "]";

        if (direction === "forward"){
            text += " x" + patternTree[i]["quantity"].toLocaleString("en");
        }

        if (includeTradeskill.checked){
            text += " (" + patternTree[i]["skill"] + ")";
        }

        line.appendChild(link);
        line.appendChild(document.createTextNode(text));

        tree.appendChild(line);
    }

    return tree;
}

//misc.
function fetchSearch() {
    var display = document.getElementById("results-display");
    var element;
    var keys = Object.keys(dataForward).sort(); 

    cleanUpDisplay();

    keys.forEach(buildSearch);

    element = displayLinkedTree("reverse");
    element.id = "search-results";

    display.appendChild(element);
    display.style.display = "block";

    patternTree = [];

}
function buildSearch(name){
    var searchTerm = document.getElementById("search").value;
    var quantity = 1;
    var indent = 0;
    var re;

    searchTerm = searchTerm.replace(/\W/g,'.');
    re = new RegExp(searchTerm, "i");

    if (re.test(name)){
        patternTree.push({"pattern": name, "skill": dataForward[name].ts, "quantity": quantity, "indent": indent});
    }
}
function cleanUpDisplay(){
    if (document.getElementById("tree-display").style.display == "block") {
        document.getElementById("forward-tree").remove();
        document.getElementById("reverse-tree").remove();
        document.getElementById("tree-display").style.display = "none";
    }

    if (document.getElementById("results-display").style.display == "block") {
        document.getElementById("search-results").remove();
        document.getElementById("results-display").style.display = "none";
    }
}