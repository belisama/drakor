![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

# Persica's Drakor Utilities

This is a collection of fan-made utilities for the online game [Drakor](https://drakor.com). Their publication is permitted by [eFireball Media](http://www.efireball.com/) (AKA the Great and Powerful Goz) and is in no way a challenge to the ownership thereby of Drakor's trademarks, copyrights, trade dress, or other relevant intellectual property.

## Public

The public facing webpages include the Searchable Map, Pattern Tree, and Directions and are described in detail [here](https://code.rose-labyrinth.com/drakor).

## *Monkey Scripts

These are scripts for use with browser extensions such as Greasemonkey and Tampermonkey. Currently, there are two finished ones, both of which add new colors to chat: pet-quotes and chat-mentions.

## Wiki

These are articles posted to the [Drakor Wiki](https://sites.google.com/site/drakorpedia/), stashed here merely as a convenient back-up and storage location. 